<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>
<%@ page import="javax.servlet.http.*"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<link href="resources/css/mystyle.css" rel="stylesheet">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>

<style>
.itemCodeleble {
	position: absolute;
	top: 0%;
	/* right: 5%; */
	width: auto;
	height: 26px;
	background: #3c43a4;
	z-index: 100;
	/* border-radius: 60px; */
	-webkit-border-radius: 15px;
	color: white;
	text-align: center;
	padding: 2px;
}

.itemCodeleble h6 {
	font-size: 1em;
	color: #fff;
	line-height: 3.5em;
}

i {
	font-style: normal;
}

.eyeimg {
	position: absolute;
	top: 63%;
	right: 15%;
	width: 30px;
	height: 30px;
	text-align: center;
}

.img-cover {
	height: 70%;
	text-align: -webkit-center;
}

._1Nyybr {
	max-width: 100%;
	max-height: 100%;
	margin: auto;
	bottom: 0;
	left: 0;
	right: 0;
	top: 0;
	vertical-align: middle;
}

.shopping-btn {
	background-image: url(../images/items/shopping-cart.png) no-repeat;
	cursor: pointer;
	border: none;
}
</style>

<script type="text/javascript">
	$(document).ready(
			function() {
				$("#myInput").on(
						"keyup",
						function() {
							var value = $(this).val().toLowerCase(); // find only chareter
							var value = $(this).val();
							$("#myTable ul").filter(
									function() {
										$(this).toggle(
												$(this).text().toLowerCase()
														.indexOf(value) > -1)
									});
						});
			});
</script>
<title>product details</title>
</head>
<body class="container-fluid alert-success">




	<c:set var="display" value="none"></c:set>
	<c:set var="btnDisplay" value="block"></c:set>
	<c:set var="btnlogin" value="none"></c:set>
	<c:choose>
		<c:when test="${role =='SUPER ADMIN'}">
			<c:set var="display" value="block"></c:set>
			<c:set var="btnDisplay" value="none"></c:set>
		</c:when>
		<c:when test="${role =='ADMIN'}">
			<c:set var="display" value="block"></c:set>
			<c:set var="btnDisplay" value="none"></c:set>
		</c:when>
		<c:when test="${role == 'USER'}">
			<c:set var="display" value="none"></c:set>
			<c:set var="btnDisplay" value="block"></c:set>
		</c:when>
		<c:otherwise>
			<c:set var="display" value="none"></c:set>
			<c:set var="btnDisplay" value="none"></c:set>
			<c:set var="btnlogin" value="block"></c:set>
		</c:otherwise>

	</c:choose>




	<div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html"%>
	</div>

	<div class="row" style="display:${display}">
		<%@include file="menupage.html"%>
	</div>

	${session.carts}

	<c:set var="tableTitle" value="NY Products"></c:set>


	<!--  table pagination  -->
	<c:set var="list" scope="session" value="${itemList}"></c:set>
	<c:set var="totalCount" scope="session" value="${list.size()}"></c:set>
	<c:set var="perPage" scope="session" value="${8}"></c:set>
	<c:set var="pageStart" value="${param.start}"></c:set>

	<c:if test="${empty pageStart or pageStart < 0 }">
		<c:set var="pageStart" value="0"></c:set>
	</c:if>
	<c:if test="${totalCount < pageStart}">
		<c:set var="pageStart" value="${pageStart - perPage}"></c:set>
	</c:if>

	<!--  table header and search and prev and next button  -->

	<div class="row filter-header">
		<div class="col-sm-4">
			<h4>${tableTitle}</h4>
		</div>
		<div class="col-sm-5">
			<input class="form-control form-control-sm" id="myInput" type="text"
				placeholder="Search..">
		</div>
		<div class="col-sm-2" hidden>

			<a href="?start=${startPage - perPage}"
				class="btn btn-primary btn-sm"
				style="margin-right: 10px; margin-bottom: 10xp;">Prev</a> <span>
				${pageStart} - ${pageStart + perPage} / ${totalCount}</span> <a
				href="?start=${startPage + perPage}" class="btn btn-primary btn-sm"
				style="margin-left: 10px; margin-bottom: 10xp;">Next</a>

		</div>

		<c:set var="addItem" value="${cartItemList.size()}" />

		<c:choose>
			<c:when test="${addItem >= 0 }">
				<c:set var="disabled" value=""></c:set>
			</c:when>
			<c:otherwise>
				<c:set var="disabled" value="disabled"></c:set>
			</c:otherwise>




		</c:choose>

		<div class="col-sm-1" style="text-align: right;">
			<button class="btn btn-sm shopping-btn" data-toggle="modal"
				data-target="#itemCartDetails" ${disabled}>
				<img src="resources/images/items/shopping-cart.png"
					style="width: 50px; height: 100%;"></img> ${cartItemList.size()}
			</button>
		</div>
		<div class="col-sm-1" style="display:${btnlogin}">
			<a href="signin"><button class="btn btn-danger btn-sm">Sign
					in</button></a>
		</div>
		<div class="col-sm-1" style="display:${btnDisplay}">
			<a href="logout"><button class="btn btn-danger btn-sm">Logout</button></a>
		</div>

	</div>





	<div class="row">
		<div class="col-sm-2" style="background-color: #efedee;">

			<div class="row">
				<div class="col-sm-10 offset-sm-2">
					<h4>Categories</h4>
				</div>

			</div>
			<div class="row">
				<div class="col-sm-12">


					<nav class="navbar bg-light">
						<ul class="navbar-nav">
							<li class="nav-item"><a class="nav-link"
								href="/productslist?cat=&subcat="> ALL </a></li>
							<c:forEach var="cat" items="${category}">
								<li class="nav-item dropdown"><a href="#"
									class="nav-link dropdown-toggle" id="navbarDropdown"
									role="button" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false"> ${cat.key} </a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<c:forEach var="scat" items="${cat.value}">
											<a class="dropdown-item"
												href="/productslist?cat=${cat.key}&subcat=${scat}"
												onClick="getProductList('${cat.key}','${scat}')">${scat}</a>
										</c:forEach>

									</div></li>

							</c:forEach>

						</ul>
					</nav>



					<%-- 		 <div class="list-group">
					
					<c:forEach var="cat" items="${category}">
						 <a href="#" class="list-group-item list-group-item-action ">${cat.category}</a>
					<a href="#" class="list-group-item dropdown-toggle" id="invoiceDropdown"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						${cat.key}</a>
							<c:set var="item" value="${cat.value}"></c:set>
							<div>
								<c:forEach var="subCat" items="${item}">
						 		 <a class="dropdown-item" href="companyinfo"> ${subCat}</a> 		
						 	  </c:forEach>
							
							
							</div>
						 	</c:forEach>
					
					
					</div>
			 --%>

				</div>
			</div>


		</div>
		<div class="col-sm-10">


			<!--  begin="${pageStart}" end="${pageStart + perPage -1}" -->
			<div id="myTable" class="row justify-content-cnter"
				style="padding: 10px; margin: 10px;">
				<c:forEach var="item" items="${list}" varStatus="loop">
					<ul class="col-sm-4"
						style="padding: 10px; height: 450px; border: 5px solid #d4edda; background-color: #efedee;">
						<div class="img-cover" style="height: 70%;">


							<img src="resources/images/items/${item.imageUrl}"
								class="_1Nyybr"></img>
							<div class="itemCodeleble">
								<p style="font-size: 15px;">${item.itemCode}</p>

							</div>
							<%-- 
						eye code 
				<div class="eyeimg">
					<img class="btn" src="resources/images/items/eye.png" style="height:100%" data-toggle="modal" data-target="#itemDetails" onClick="showDetails('${item.id}','${item.itemName}','${item.itemDescription}','${item.itemOSP}','${item.itemTP}','${item.imageUrl}')"></img>
				</div> --%>
						</div>
						<div
							style="height: 12%; width: 100%; text-align: center; color: red;">
							<h6>${item.itemName}</h6>
						</div>
						<div
							style="height: 8%; width: 100%; text-align: center; color: black;">

							<img src="resources/images/items/NewRupeeSign_thumb.jpg"
								style="height: 15px; margin: auto"></img> <span class="h6">
								${item.itemOSP} </span>
							<!-- for user -->
							<span class="h6" hidden> ${item.itemTP} </span> &nbsp &nbsp
							<!-- for resaler -->
							<span class="h6" style="color: #abc"><strike>
									${item.itemMRP} </strike></span>
						</div>

						<div style="height: 10%; width: 100%;">
							<button class="btn btn-default btn-sm btn-block"
								data-toggle="modal" data-target="#itemDetails"
								onClick="showDetails('${item.id}','${item.itemName}','${item.itemDescription}','${item.itemOSP}','${item.itemTP}','${item.imageUrl}')">
								View</button>
							<%-- 	<form action="addcart" method="post" modelAttribute="item">
									<input type="hidden" name="itemId" value="${item.id}" />
									<input type="hidden" name="itemPrice" value="${item.itemOSP}" /> 
									<input type="hidden" name="itemPrice1" value="${item.itemMRP}" /> 
									<input type="hidden" name="quantity" value="${1}" /> 
									<button type="submit" class="btn btn-default btn-sm btn-block" >Add to cart</button>
					</form> --%>
						</div>

					</ul>

				</c:forEach>





			</div>

		</div>
	</div>



	<script>
		function showDetails(id, name, description, ospPrice, tpPrice, imgUrl) {
			debugger;
			//	var image='<img alt="defuser" src="resources/images/items/'+itemCode+'.jpg" style="width:100%;height:100%;">';
			var image = '<img alt="defuser" src="resources/images/items/'+imgUrl+'"  class="_1Nyybr"/>';

			document.getElementById('PitemImage').innerHTML = image;
			document.getElementById('itemTitle').innerHTML = name;
			document.getElementById('priceId').innerHTML = ospPrice;
			//document.getElementById('priceId').innerHTML='200px':
			document.forms['purchaseForm']['itemId'].value = id;
			document.forms['purchaseForm']['itemName'].value = name;
			//document.forms['purchaseForm']['quantity'].value = quantity;
			document.forms['purchaseForm']['itemDescription'].value = description;
			document.getElementById('itemDesc').innerHTML = description;
			document.forms['purchaseForm']['itemPrice'].value = ospPrice;

			var quantity = 0;
			document.forms['purchaseForm']['totalPrice'].value = ((Number(quantity)) * (Number(ospPrice)));

		}

		function calculatePrice() {
			var quantity = document.forms['purchaseForm']['quantity'].value;
			var price = document.forms['purchaseForm']['itemPrice'].value;
			document.getElementById("totalAmount").innerHTML = ((Number(quantity)) * (Number(price)));
			document.forms['purchaseForm']['totalPrice'].value = ((Number(quantity)) * (Number(price)));
		}
	</script>



	<div class="row">
		<!-- Button to Opne The modal -->

		<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> open modal</button> -->

		<!-- the  modal -->
		<div class="modal" id="itemDetails">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="addcart" method="POST" name="purchaseForm"
						modelAttribute="item">
						<div class="modal-header">

							<h4 class="modal-title" id="itemTitle">${itemName}</h4>
							<button type="button" class="close" data-dismiss="modal">
								&times;</button>
						</div>

						<div class="modal-body">
							<div class="row">
								<div class="col-sm-6" id="PitemImage" style="height: 400px;">
									<!-- here showing image..  don't remove it -->
								</div>
								<div class="col-sm-6">
									<div class="form-group" hidden>
										<label>item Id</label> <input type="text" class="form-control"
											name="itemId" id="itemId">
									</div>

									<div class="form-group">
										<label hidden>item Description</label> <input type="text"
											class="form-control" name="itemDescription"
											id="itemDescription" hidden />
										<h6 id="itemDesc"></h6>
									</div>

									<div class="form-group" hidden>
										<label>Supplier Name</label> <input type="text"
											class="form-control" name="vendorName" id="venderName" />
									</div>

									<div class="form-group" hidden>
										<label>Item Name </label> <input type="text"
											class="form-control" name="itemName" id="itemName" />
									</div>
									<div class="form-group">
										<label>Quantity</label> <input type="number"
											class="form-control" name="quantity" id="quantity"
											onKeyUp="calculatePrice()" />
									</div>
									<div class="form-group">
										<label>Price : </label> <img
											src="resources/images/items/NewRupeeSign_thumb.jpg"
											style="height: 15px;"></img> <span id="priceId"></span> <input
											type="text" class="form-control" name="itemPrice"
											id="itemPriec" hidden />

									</div>
									<div class="form-group">
										<label>Total</label> : <label id="totalAmount"> </label> <input
											type="text" class="form-control" name="totalPrice"
											id="totalPrice" hidden />
									</div>



								</div>

							</div>


						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-success" value="Add To Cart" />


							<button type="button" class="btn btn-danger" data-dismiss="modal">
								close</button>
						</div>
					</form>

				</div>
			</div>

		</div>
	</div>


	<div class="row">
		<!-- Button to Opne The modal -->

		<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> open modal</button> -->

		<!-- the  modal -->
		<div class="modal" id="itemCartDetails">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<div class="modal-header">

						<h4 class="modal-title" id="itemTitle">Cart Items List</h4>
						<button type="button" class="close" data-dismiss="modal">
							&times;</button>
					</div>

					<div class="modal-body">
						<table class="table table-style table-sm table-bordered">
							<thead>
								<tr style="background-color: #abc;">
									<th>S.N.</th>
									<th>Item Name</th>
									<th>Quantity</th>
									<th>Price</th>
									<th>Total Price</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="totalItem" value="0"></c:set>
								<c:set var="totalAmount" value="0"></c:set>
								<c:forEach var="citem" items="${cartItemList}" varStatus="loop">
									<tr>
										<c:set var="totalItem" value="${totalItem + citem.quantity}"></c:set>
										<c:set var="totalAmount"
											value="${totalAmount + citem.totalPrice}"></c:set>

										<td>${loop.count}</td>
										<td>${citem.itemCode.itemName}</td>
										<td>${citem.quantity}</td>
										<td>${citem.itemPrice}</td>
										<td>${citem.totalPrice}</td>
										<td><a href="removecart/${loop.count}"><button
													class="btn btn-danger btn-sm">Remove</button></a></td>
									</tr>
								</c:forEach>
								<tr style="border: solid 2px red; background-color: #abc;">

									<th colspan="2">Total Number Of Item</th>
									<th>${totalItem}</th>
									<th>Total Amount</th>
									<th colspan="2">${totalAmount}</th>


								</tr>
							</tbody>

						</table>
					</div>
					<div class="modal-footer">
						<!-- <a href="/cartinvoice"><button class="btn btn-success">Buy</button></a> -->

						<a href="addorderaddress"><button class="btn btn-success">Buy</button></a>

						<button type="button" class="btn btn-danger" data-dismiss="modal">
							close</button>
					</div>


				</div>
			</div>

		</div>
	</div>

	<script type="text/javascript">
		function getProductList(cat, subcat) {
			console.log(" cat " + cat);
			console.log(" subcat " + subcat);
			var url = "productlist?cat=Computer&subcat=Kitchen";
			$.get(url, function(data, status) {
				console.log(' list of produt ' + data);

			})

		}
	</script>



</body>
</html>