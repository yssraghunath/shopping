<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>sales details</title>
</head>
<body class="container-fluid alert-success">
<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>

<c:set var="list" scope="session" value="${itemsList}"></c:set>
<c:set var="tableTitle"  value="Sale Details"></c:set> 
<c:set var="list" scope="session" value="${itemsList}"></c:set>
<%-- <%@include file="table_filter.jsp" %> --%>
<%-- <c:set var="totalCount" scope="session" value="${list.size()}"></c:set>
<c:set var="perPage" scope="session" value="${10}"></c:set>
<c:set var="startPage"  value="${param.start}"></c:set>
<c:if test="${empty startPage or startPage < 0 }">
	<c:set var="startPage"  value="0"></c:set>
</c:if>

<c:if test="${totalCount < startPage }">
	<c:set var="startPage"  value="${startPage - perPage}"></c:set>
</c:if>
 --%>

 <div class="row">
 	<div class="col-sm-12">
 		<div class="card">
 			<div class="card-header">
 			<%@include file="table_filter.jsp" %>
 			</div>
 			<div class="card-body">
 	<table class="table table-style table-sm table-bordered table-striped">
	<thead class="table-header-style">
		<tr>
			<th>ID </th>
			<th>Item Name</th>
			<th>Quantity </th>
			<th> Item Price </th>
			<th>Total Price </th>
			<th>Buyer Name</th>
			<th>Date Time </th>
			
			
		</tr>
	</thead>
	<tbody id="myTable">
	<c:forEach var="item" items="${list}" begin="${startPage}" end="${startPage + perPage -1}" varStatus="loop">
	<tr>
			<td>${item.id}</td>
			<td>${item.itemName}</td>
			<td>${item.quantity}</td>
			<td>${item.itemPrice}</td>
			<td>${item.totalPrice}</td>
			<td>${item.buyerName}</td>
			<td>${item.dateAndTime}</td>
		</tr>
	
	</c:forEach>
		
	</tbody>

</table>
			
 			</div>
 		
 		</div>
 	
 	</div>
 
 
 </div>

</body>
</html>