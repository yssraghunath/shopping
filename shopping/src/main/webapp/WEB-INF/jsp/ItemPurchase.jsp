<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<link rel='stylesheet'	href='https://use.fontawesome.com/releases/v5.4.2/css/all.css'
	integrity='sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns'
	crossorigin='anonymous'>
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<title>Item Purchase</title>
</head>
<body class="container-fluid alert-success">
	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>

<div class="row">



	<form>
		<div class="table-responsive">

			<table class="table table-bordered table-striped table-highlight">

				<thead>
					<tr>
					
						<th>Item Name</th>
						<th>Taxable Price</th>
						<th>CGST</th>
						<th>SGST</th>
						<th>Total Tax</th>
						<th>Total Price</th>
					</tr>
				</thead>

				<tbody id="my_id">
					

				</tbody>

			</table>
		</div>
	</form>
	

	<div id="my_id">
	
	</div>
	<button onClick="add()"> + </button>
	<button onClick="remove()"> -  </button>
	<script type="text/javascript">
	
	
	var id=[];
	var i=0;
	 var v1='<tr>'+
		'<td><input id="id'+i+'" type="text" class="form-control form-control-sm"/></td>'+
		'<td><input type="text" class="txt"  size="12" /></td>'+
		'<td><input type="text" class="txt"  size="10" /></td>'+
		'<td><input type="text" class="txt"  size="10" /></td>'+
		'<td><input type="text" class="txt"  size="12" /></td>'+
		'<td><span id="sum">0</span></td></tr>'; 
	
		var str=[];
		str.push(v1);
		document.getElementById("my_id").innerHTML=str;
		function add(){
			
			
			
				var idvalue=document.getElementById("id"+i+"").value;
				id.push(idvalue);
				//var x = document.getElementById("myText").value;
				console.log(idvalue)
			
			
			i++;
			var v='<tr>'+
			'<td><input id="id'+i+'" type="text" class="form-control form-control-sm"/></td>'+
			'<td><input type="text" class="txt"  size="12" /></td>'+
			'<td><input type="text" class="txt"  size="10" /></td>'+
			'<td><input type="text" class="txt"  size="10" /></td>'+
			'<td><input type="text" class="txt"  size="12" /></td>'+
			'<td><span id="sum">0</span></td></tr>';
			console.log(i);
			str.push(v);
			document.getElementById("my_id").innerHTML=str;
			for(var l=0;l<i;l++){
				document.getElementById("id"+l+"").value=id[l];
			}
			
		}
		function remove(){
			
			var len=str.length;
			if(len==1){
				
			}else{
				i--;
				str.splice(len-1);	
			}
			console.log(len);
			
			
			document.getElementById("my_id").innerHTML=str;
			for(var l=0;l<len-1;l++){
				document.getElementById("id"+l+"").value=id[l];
			}
			
		}
		$(document).ready(function(){

			//iterate through each textboxes and add keyup
			//handler to trigger sum event
			$(".txt").each(function() {

				$(this).keyup(function(){
					calculateSum();
				});
			});

		});

		function calculateSum() {

			var sum = 0;
			//iterate through each textboxes and add the values
			$(".txt").each(function() {

				//add only if the value is number
				if(!isNaN(this.value) && this.value.length!=0) {
					sum += parseFloat(this.value);
				}

			});
			//.toFixed() method will roundoff the final sum to 2 decimal places
			$("#sum").html(sum.toFixed(2));
		}
	</script>
</body>
</html>





</script>