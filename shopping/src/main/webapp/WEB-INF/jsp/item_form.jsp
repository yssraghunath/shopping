<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css"  rel="stylesheet">
<script>
	function calculatePrice(){
		
		
		var i_CGST=document.getElementById("cgst").value;
		var i_SGST=document.getElementById("sgst").value;
		/* CGST=CGST*i_quantity;
		SGST=SGST*i_quantity; */
		var iGst=Number(i_CGST)+Number(i_SGST);
		document.getElementById("igst").value=iGst;
	}
	
	
	</script>

<title>item form</title>
</head>
<body class="container-fluid alert-success">
	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>
		
	<div class="row" style="background-color:white;padding:10px;margin:10px;">
			
				<p class="col-sm-12 h4" >Item Form</p>				
				<form action="saveitem" method="POST"  modelAttribute="item" class="form form-inline col-sm-12 alert-success" style="padding:10px;" > 
				<div class="form-group col-sm-2" >
					<label>Item Name<span class="req" style="color: Red">*</span></label>
					<input type="text" class="form-control col-sm-12" name="itemName" id="itemName" placeholder="" required>
		<!-- 			<select class="form-control col-sm-12" name="itemName">
					
						<option value="MOUSE">Mouse</option>
						<option value="MONITOR">Monitor</option>
						<option value="KEYBOARD">Keyboard</option>
						<option value="HARD DISK">Hard Disk</option>
						
	
						
					</select>
					
		-->
				</div>
				<!-- <div class="form-group col-sm-2">
					<label> Company Name </label>
					<input type="text" class="form-control col-sm-12" name="companyName" id="companyName" placeholder="">
				</div> -->
				
				<div class="form-group col-sm-2">
					<label> HSN/SAC<span class="req" style="color: Red">*</span> </label>
					<input type="number" class="form-control col-sm-12" name="hnsCode" id="hnsCode" placeholder="" required>
				</div>
				
				
				
				
				<div class="form-group col-sm-2">
					<label>SGST (%) <span class="req" style="color: Red">*</span></label>
					<input type="number" class="form-control col-sm-12"  name="sgst" onKeyUp="calculatePrice()" id="sgst"  placeholder="" required>
				</div>
				<div class="form-group col-sm-2">
					<label>CGST (%) <span class="req" style="color: Red">*</span></label>
					<input type="number" class="form-control col-sm-12"  name="cgst" onKeyUp="calculatePrice()" id="cgst"  placeholder="" required>
				</div>
				
				<div class="form-group col-sm-2">
					<label>IGST (%) <span class="req" style="color: Red">*</span></label>
					<input type="numbers" class="form-control col-sm-12"  name="igst" onKeyUp="calculatePrice()" id="igst"  placeholder="" required>
				</div>
				
					
				<div class="form-group col-sm-2" style="padding-top:10px;">
					
					<input type="submit" class="btn btn-success btn-block" value="Add" style="margin-top:10px;" name="" placeholder="">
				</div>
				
			</form>
			
			
	</div>
	
	

<c:set var="tableTitle"  value="Invoice Details"></c:set>
	<c:set var="list" scope="session" value="${itemList}"></c:set>
	

	
	
	<div class="row" >
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<%@include file="table_filter.jsp" %>
				</div>
				<div class="card-body">
						<table class="table table-style table-sm table-bordered table-striped">
					
						<thead class="table-header-style">
						<tr>
							<th>Id</th>
							<th>Name</th>
							<!-- <th>Comapany</th> -->
							<th>HSN/SAC Code </th>
							<th>CGST</th>
							<th>SGST</th>
							<th>CGST</th>
							
					</tr>
						</thead>
					
					
						<tbody id="myTable">
						<c:forEach  var="item" items="${list}" begin="${pageStart}" end="${pageStart + perPage -1}"  varStatus="loop">
						<tr  >
							<td>${item.id}</td>
							<td>${item.itemName}</td>
							<%-- <td>${purchaseInfo.companyName}</td> --%>
							<td>${item.hnsCode}</td>
							<td>${item.cgst}</td>
							<td>${item.sgst}</td>
							<td>${item.igst}</td>
														
						</tr>
						</c:forEach>
						</tbody>
					
				</table>
						
				
				</div>
			</div>
		</div>
				</div>
	
	

</body>
</html>