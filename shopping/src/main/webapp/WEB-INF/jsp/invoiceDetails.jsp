<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css"  rel="stylesheet">


 <script type="text/javascript">
      function myFunction() {
    		document.getElementById("print").style.display = "none";
    window.print();
}
    </script>
<style>
/* You can add global styles to this file, and also import other style files */
.container{
    padding-top:5%;
    padding-right: 12%;
    padding-left:12%;
}
.header{
    border-style: none;
    background: #dcd9d9;
    font-weight: 600;
    text-align: center;
    font-family: sans-serif;
    font-size: large;
}
.company{
    font-weight: bold;
    font-size:larger;
    font-family: sans-serif;
}
 .gst{
    font-weight: 700;
    font-size:small;
    font-family: sans-serif;
}
.pan{
    font-weight: 700;
    font-size:small;
    font-family: sans-serif;
}
.cin{
    font-weight: 700;
    font-size:small;
    font-family: sans-serif;
}

.GSTIN_NO{
    font-weight: 800;
    font-size:small;
}
.PAN_NO{
    font-weight: 800;
    font-size:small;
}
.CIN_NO{
    font-weight: 800;
    font-size:small;
}
.border{
    border-style: solid;
    border-width: 1px;
}
.bill_border
{
    border-right:1px solid black
}

.Buyer
{
    background: #dcd9d9;
    font-weight: 700;
    font-size: medium;
    margin-top:1px;
    font-family: sans-serif;
}

.Buyer_name{
    font-size: small;
}

.invoice_border{
    border-right:1px solid black;
}
.invoice{
    height:50px; 
    border-bottom: 1px solid black;
}
.invoice_no
{
    font-size: small;
    font-family: sans-serif;
}
.quote
{
    font-size: small;
    font-family: sans-serif;
}
.date_border
{
    height:50px; 
    border-bottom: 1px solid black;
}
.date{
    font-size: small;
    font-family: sans-serif;
}
.due_date{
    font-size: small;
    font-family: sans-serif;
}
.shipped_border
{
    border-right:1px solid black;
    border-left:1px solid black;
}
.shipped_to
{
    border-right:1px solid black;
}
.Shipped_to_site
{
    background: #dcd9d9;
    font-weight: 700;
    font-size: medium;
    border-width: 1px;
    margin-top:1px;
    font-family: sans-serif;
}
.site_name{
    font-size: small;
}
.client_border
{
    border-bottom: 1px solid black 
}
.client
{
    height:50px; 
    border-right: 1px solid black;
}
.client_po
{ 
    font-size: small;
    font-family: sans-serif;
}
.p{
    height:50px;
}
.po_so{
    font-size: small;
}
.amc_duration{
    font-size: small;
    font-family: sans-serif;
}
.project_ref{
    border:solid black 1px;
    background-color:#eefbda;
    margin-top:1px;
}
.project{
    text-align:center;
    font-family: sans-serif;
}
.table{
    margin-top:2px;
}
.table .table-sm .table-bordered
{
    margin:0px;
}
.th{
    font-size:small;
    font-family: sans-serif;
    font-weight:700;
}
.td{
    font-size:small;
    font-family: sans-serif;
    font-weight:700;
}


.ammount
{
    font-size:small;
    font-family: sans-serif;
}
.total_ammount{
    font-size:small;
}
.payable{
    font-size:small;
    font-family: sans-serif;
}
.INR{
    font-size:small;
    font-family: sans-serif;
}
.ammount_word{
font-size:small;
font-weight:600;
}
.tax{
    font-size:small;
    font-weight: 600;
}
.tax_apply
{
    font-size:small;
    font-weight: 600;
}
.company_name
{
    font-size:small
}
.signature
{
    font-size:small;
}
.address_border
{
    border-top:1px solid black;
}
.registered_address
{
    border-right:1px solid black;
    border-left:1px solid black;
    background:#dcd9d9;
    text-align:center;
    font-weight:700;
    font-size:small;
    font-family:sans-serif;
}
.corporate_address
{
    border-right:1px solid black;
    background:#dcd9d9;
    text-align:center;
    font-weight:700;
    font-size:small;
    font-family:sans-serif;
}
.r_address{
    border-top:1px solid black;
}
.reg_address{
    border-right:1px solid black;
    border-left:1px solid black;
    font-size:small;
    font-weight:500;
}
.corp_address{
    border-right:1px solid black;
    font-size:small;
    font-weight:500;
}
.declaration
{
    border-top:solid 1px black;
}
.description{
font-size:x-small;
font-weight: 500;
border-left:solid 1px black;
border-right:solid 1px black;
}
.bank_border
{
    border:solid 1px black;
}
.bank_detail
{
    font-size:small;
    background: #dcd9d9;
    font-weight: 800;
    margin-top:1px;
}
    @media print {

        .container{
            padding-top:5%;
            padding-right: 12%;
            padding-left:12%;
        }
        .header{
            border-style: none;
            background: #dcd9d9;
            font-weight: 600;
            text-align: center;
            font-family: sans-serif;
            font-size: large;
        }
        .company{
            font-weight: bold;
            font-size:larger;
            font-family: sans-serif;
        }
         .gst{
            font-weight: 700;
            font-size:small;
            font-family: sans-serif;
        }
        .pan{
            font-weight: 700;
            font-size:small;
            font-family: sans-serif;
        }
        .cin{
            font-weight: 700;
            font-size:small;
            font-family: sans-serif;
        }
        
        .GSTIN_NO{
            font-weight: 800;
            font-size:small;
        }
        .PAN_NO{
            font-weight: 800;
            font-size:small;
        }
        .CIN_NO{
            font-weight: 800;
            font-size:small;
        }
        .border{
            border-style: solid;
            border-width: 1px;
        }
        .bill_border
        {
            border-right:1px solid black
        }
        
        .Buyer
        {
            background: #dcd9d9;
            font-weight: 700;
            font-size: medium;
            margin-top:1px;
            font-family: sans-serif;
        }
        
        .Buyer_name{
            font-size: small;
        }
        
        .invoice_border{
            border-right:1px solid black;
        }
        .invoice{
            height:50px; 
            border-bottom: 1px solid black;
        }
        .invoice_no
        {
            font-size: small;
            font-family: sans-serif;
        }
        .quote
        {
            font-size: small;
            font-family: sans-serif;
        }
        .date_border
        {
            height:50px; 
            border-bottom: 1px solid black;
        }
        .date{
            font-size: small;
            font-family: sans-serif;
        }
        .due_date{
            font-size: small;
            font-family: sans-serif;
        }
        .shipped_border
        {
            border-right:1px solid black;
            border-left:1px solid black;
        }
        .shipped_to
        {
            border-right:1px solid black;
        }
        .Shipped_to_site
        {
            background: #dcd9d9;
            font-weight: 700;
            font-size: medium;
            border-width: 1px;
            margin-top:1px;
            font-family: sans-serif;
        }
        .site_name{
            font-size: small;
        }
        .client_border
        {
            border-bottom: 1px solid black 
        }
        .client
        {
            height:50px; 
            border-right: 1px solid black;
        }
        .client_po
        { 
            font-size: small;
            font-family: sans-serif;
        }
        .p{
            height:50px;
        }
        .po_so{
            font-size: small;
        }
        .amc_duration{
            font-size: small;
            font-family: sans-serif;
        }
        .project_ref{
            border:solid black 1px;
            background-color:#eefbda;
            margin-top:1px;
        }
        .project{
            text-align:center;
            font-family: sans-serif;
        }
        .table{
            margin-top:2px;
        }
        .table .table-sm .table-bordered
        {
            margin:0px;
        }
        .th{
            font-size:small;
            font-family: sans-serif;
            font-weight:700;
        }
        .td{
            font-size:small;
            font-family: sans-serif;
            font-weight:700;
        }
        .ammount
        {
            font-size:small;
            font-family: sans-serif;
        }
        .total_ammount{
            font-size:small;
        }
        .payable{
            font-size:small;
            font-family: sans-serif;
        }
        .INR{
            font-size:small;
            font-family: sans-serif;
        }
        .ammount_word{
        font-size:small;
        font-weight:600;
        }
        .tax{
            font-size:small;
            font-weight: 600;
        }
        .tax_apply
        {
            font-size:small;
            font-weight: 600;
        }
        .company_name
        {
            font-size:small
        }
        .signature
        {
            font-size:small;
        }
        .address_border
        {
            border-top:1px solid black;
        }
        .registered_address
        {
            border-right:1px solid black;
            border-left:1px solid black;
            background:#dcd9d9;
            text-align:center;
            font-weight:700;
            font-size:small;
            font-family:sans-serif;
        }
        .corporate_address
        {
            border-right:1px solid black;
            background:#dcd9d9;
            text-align:center;
            font-weight:700;
            font-size:small;
            font-family:sans-serif;
        }
        .r_address{
            border-top:1px solid black;
        }
        .reg_address{
            border-right:1px solid black;
            border-left:1px solid black;
            font-size:small;
            font-weight:500;
        }
        .corp_address{
            border-right:1px solid black;
            font-size:small;
            font-weight:500;
        }
        .declaration
        {
            border-top:solid 1px black;
        }
        .description{
        font-size:x-small;
        font-weight: 500;
        border-left:solid 1px black;
        border-right:solid 1px black;
        }
        .bank_border
        {
            border:solid 1px black;
        }
        .bank_detail
        {
            font-size:small;
            background: #dcd9d9;
            font-weight: 800;
            margin-top:1px;
        }

    #print {
      display: none;
    }
  }
</style>    
</head>
<body>

<c:set var = "theString" value = "${previewData[0].invoiceDetails.buyerDetails}"/>
	
	
	
	<c:choose>
  		<c:when test="${fn:contains(theString, 'Delhi')}">
  			<c:set var="delhiYes" value="block"></c:set>
			<c:set var="delhiNo" value="none"></c:set>
			
		
  		 </c:when>
  		<c:when test="${fn:contains(theString, 'DELHI')}">
			<c:set var="delhiYes1" value="block"></c:set>
			<c:set var="delhiYes2" value="block"></c:set>
			<c:set var="delhiNo" value="none"></c:set>
		
		</c:when>
  		<c:when test="${fn:contains(theString, 'delhi')}">
  				<c:set var="delhiYes1" value="block"></c:set>
			<c:set var="delhiYes2" value="block"></c:set>
			<c:set var="delhiNo" value="none"></c:set>
			
  		</c:when>
  		<c:otherwise> 
				<c:set var="delhiYes1" value="none"></c:set>
				<c:set var="delhiYes2" value="none"></c:set>
				<c:set var="delhiNo" value="block"></c:set>
				
		 </c:otherwise>
	</c:choose>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
           <p class="header">Tax Invoice</p>
             
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 offset-sm-8">
            <span class="company">${selfDetails.companyName}</span><br>
            <span class="gst">GSTIN No:-</span><span class="GSTIN_NO">${selfDetails.gstNo}</span><br>
            <span class="pan">PAN No:-</span><span class="PAN_NO">${selfDetails.panNo}</span><br>
            <span class="cin">CIN No:-</span><span class="CIN_NO">${selfDetails.cinNO}</span><br>
            
        </div>
    </div>

    <div class="row" style=" border-style: solid;border-width: 1px;">
        <div class="col-sm-6 bill_border" >

            <p class="Buyer">Bill To(Buyer)</p>
            <p class="Buyer_name"><b>${previewData[0].invoiceDetails.buyerDetails}
                </b></p>
        </div>
        <div class="col-sm-3 invoice_border">
            <div class="row">
                <div class="col-sm-12 invoice">
                    <p class="invoice_no">Invoice No: - <br><b><i>${previewData[0].invoiceDetails.invoiceNo}${previewData[0].invoiceDetails.id}</i></b></p>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="quote">Quote/AAC/Ref No:-<br><b>${previewData[0].invoiceDetails.quoteRefNo}</b></p>
                </div>

            </div>

        </div>
        <div class="col-sm-3">
            <div class="row">
                <div class="col-sm-12 date_border">
                    <p class="date">Date:-<i>${previewData[0].invoiceDetails.invoiceDate}</i></p>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-12 ">
                    <p class="due_date">Due Date:-<br><b><i>${previewData[0].invoiceDetails.dueDate}</i></b></p>
                </div>
            </div>

        </div>
    </div>

    <div class="row shipped_border">
        <div class="col-sm-6 shipped_to">
            <p class="Shipped_to_site">Shipped To(Site)</p>
            <p class="site_name"><b>${previewData[0].invoiceDetails.siteDetails}
                </b></p>
        </div>
        <div class="col-sm-6">
            <div class="row client_border">
                <div class="col-sm-6 client" >
                    <p class="client_po">Client's/PO/Ref No:-<br><b><i>${previewData[0].invoiceDetails.clientPoNo}</i></b></p>
                </div>
                <div class="col-sm-6 po">
                    <p class="po_so">PO/SO/Date<br><b><i>${previewData[0].invoiceDetails.poDate}</i></b></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="amc_duration">AMC Duration:<b>${previewData[0].invoiceDetails.amcDateFrom} to ${previewData[0].invoiceDetails.amcDateTo}</b><br><b>Payment
                            Mode:${previewData[0].invoiceDetails.paymentMode}</b></p>
                </div>
            </div>
        </div>

    </div>
    <div class="row project_ref">
        <div class="col-sm-12 project">
            <b>Project Ref:-AMC-IVRS</b>
         
      </div>
      </div>

      <div class="row table" >
          <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr  class="th">
                <th>HNS/SAC Code</th>
                <th>Item</th>
                <th>Taxable price</th>
                <th style="display:${delhiYes1}">CGST (%)</th>
                
                <th style="display:${delhiYes1}" >SGST (%)</th>
                
                <th style="display:${delhiNo}">IGST (%)</th>
                <th>Total Tax Amount</th>
                <th>Total Price </th>
              
              </tr>
            </thead>
            <tbody>
            
                <c:set var="totalPrice" value="0" scope="page"/> 
                <c:forEach var="item" items="${previewData}" varStatus="loopCounter">
                 
              <tr class="td">
                <td>${item.hnsCode}</td>
                <td>${item.itemName}</td>
                <td>${item.taxAblePrice}</td>
                <td style="display:${delhiYes1}" >${item.cgst }</td>
                <td style="display:${delhiYes1}" >${item.sgst }</td>
                <td style="display:${delhiNo}">${item.igst }</td>
                <td >${item.totalTaxAmount}</td>
              	<c:set var="mmm" value="${item.totalprice}" scope="request"/>
            		
            		<c:set var="xx" value="${totalPrice+mmm}" scope="request"/>
            		<c:set var="totalPrice" value="${xx}" scope="page"/>	
            		<td>${item.totalprice }</td>
                           
              </tr>
            </c:forEach>
            </tbody>
          
          </table>
            
            </div>
          
          </div>

          <div class="row">
              <div class="col-sm-3"></div>
              <div class="col-sm-3"></div>
              <div class="col-sm-2"></div>
              <div class="col-sm-2">
                  <p class="ammount"><b>Total Amount</b> <br>    	
                      <span class="payable">Payable In</span>
                  </p>
              </div>
              <div class="col-sm-2">
                  <p class="total_ammount" ><b><c:out value="${totalPrice}"></c:out></b><br>
                      <span class="INR">INR</span>
                  </p>
              </div>

          </div>

          <div class="row">
              <div class="col-sm-12">
                  <span class="ammount_word">In Word:- One Lac Fifty Four Thousand One Hundred
                      Thirty Six Rupees Only</span>
              </div>

          </div>

          <div class="row">
              <div class="col-sm-5">
                  <p class="tax">Whether Tax is Payable on Reverse Charges</p>
              </div>
              <div class="col-sm-7">
                  <p class="tax_apply"> <b>${previewData[0].invoiceDetails.taxPayable}</b></p>
              </div>
          </div>
          <div class="row">
              <div class="col-md-4 offset-md-8">
                  <div class="col-sm-10">
                      <span class="company_name"><b>For YSS SoftTech Pvt.Ltd.</b></span>
                  </div>
              </div>
          </div><br>
          <div class="row">
              <div class="col-md-4 offset-md-8">
                  <div class="col-sm-10">
                      <span class="signature"><b>Authorized Signatory</b></span>
                  </div>
              </div>
          </div>


          <div class="row address_border">
              <div class="col-sm-6 registered_address">
              Registered Address
              </div>
              <div class="col-sm-6 corporate_address">
             <span>Corporate Address</span>
              </div>
              </div>
              
              <div class="row r_address">
              
              <div class="col-sm-6 reg_address">
              
                  ${selfDetails.registerAddress}
              </div>
              
              <div class="col-sm-6 corp_address">
                  ${selfDetails.corporateAddress}
              </div>
              
              </div>
                
                  <div class="row declaration">
                      <div class="col-sm-12 description">
                        *  Declaration-- We declare that this invoice shows the actual price of the goods/Services described and that all particulars are True and correct.<br> 
                        * This Invoice is recognized subject to payment realization.<br>
                        * All Disputes are subject to Delhi Jurisdiction only.<br>
                        
                      </div>
                  </div>
                  <div class="row bank_border">
                      <div class="col-sm-12">
                          <p class="bank_detail">Bank Details for Payment</p>
                             Name of the Bank:-${selfDetails.bank.bankName}<br>
                             Account No:-<b>${selfDetails.bank.accountNo}</b><br>
                             IFSC/NEFT Code:-<b>${selfDetails.bank.ifscCode}</b><br>
                             Branch:-${selfDetails.bank.bankAddress}
                          
                      </div>
                  </div>
                  <br>
                  <div class="row justify-content-center">
                      
                      <button type="button" class="btn btn-dark" onclick="myFunction()" id="print" style="display:block">Print this page</button>
                  </div>
</div>
    
    
</body>
</html>