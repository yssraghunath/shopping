<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body class="container alert-success">

<div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
</div>
<div class="row"  style="margin-top:15%;">
	<div class="col-sm-4 offset-sm-4" style="border:solid white 2px;background-color:#aabbcc">
		<h3>Forget Password</h3>
		<form action="forgetpassword" method="POST">
		<div class="form-group">
			<input type="email" class="form-control" name="emailId" placeholder="Enter your email id">
		</div>
		<div class="form-group">
			<input type="mobile" class="form-control" name="mobile" placeholder="Enter your mobile number">
		</div>
		<div class="form-group">
			<p style="color:red">${message}</p>
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-default btn-block" value="Submit"/>
		</div>
	</form>
	
	</div>
	

</div>

</body>
</html>