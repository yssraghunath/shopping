<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>delivery details</title>
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"	rel="stylesheet">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
<script src="webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
</head>
<body class="container alert-success">


 <c:set var="display" value="block"></c:set>
<c:choose>
	<c:when test="${role =='DELIVERY MAN'}">
	<c:set var="display" value="none"></c:set>
	</c:when>
</c:choose>
 
	<div class="row h1  justify-content-center" style="text-align: center;" >
		<%@include file="headerpage.html" %>
	</div>

	 <div class="row" style="display:${display}">
		<%@include file="menupage.html" %>
	</div>
	<hr>
	<div class="row  form-control-sm">
		<div class="col-sm-6 offset-sm-3" style="margin-top:1%; border:solid 2px white;padding-top:20px;border-radius:10px;background-color:#e2d09b">
			<form action="delivery" method="GET"  >
			<div class="form-group " style="background-color:#d4edda;">
			<div class="row justify-content-center" style="padding-left:5px;padding-right:5px;padding-top:5px;">
				<div class="col-sm-10"><h4>Delivery Form</h4></div>
				<div class="col-sm-2"><a href="logout" class="btn btn-danger btn-sm">Logout</a></div> 
			</div>
			
		</div>
		<div class="form-group " >
			<div class="row">
				<label class="col-sm-3">Order Number</label> 
				<input type="number" class="form-control col-sm-6" name="id">	
			</div>
			
		</div>
		<!-- <div class="form-group">
			<div class="row">
				<label class="col-sm-3"> Delivery Date  </label> 
				<input type="date" class="form-control col-sm-6" name="id">	
			</div>
			
		</div> -->
		
		<!-- <div class="form-group">
			<div class="row justify-content-center">
				<h4>Success full delivered</h4>	
			</div>
			
		</div> -->
		
		<div class="form-group">
			<div class="row justify-content-center">
				<input type="submit" class="btn btn-success " value="submit">	
			</div>
			
		</div>
	</form>	
		
		
		</div>
	</div>
	
	
	
		<div class="row h6" style="background-color:white;padding:10px;">
		<div class="col-sm-12" >
		<div class="row">
		<div class="col-sm-3">
			Customer Name : ${orderList[0].userId.name}
    					
    					
		</div>
		<div class="col-sm-3">
		Mobile No : ${orderList[0].userId.mobile}
		</div>
		<div class="col-sm-3">
			Address : ${orderList[0].userId.address}
		</div>
		
		<div class="col-sm-3">
		 Order Id : ${orderList[0].orderId.id} 
		</div> 
		</div>
	</div>	
	</div>
	<div class="row" style="background-color:white;font-size:13px;">
		<div class="col-sm-8 offset-sm-2">
		
		<c:forEach var="item" items="${orderList}">
			
			<div class="row alert-success" style="border:solid 2px white ;height:380px;">
				<div class="col-sm-6" style="height:380px;text-align:center;padding-top:10px;padding-bottom:10px;">
					<img src="resources/images/items/${item.itemCode.imageUrl}" style="max-width:100%;max-height:100%;">
				</div>
				<div class="col-sm-6" style="height:380px;">
					<table class="table table-bordered" >
						<tr>
							<th> id </th>
							<td> ${item.id }</td>
						</tr>
						<tr>
							<th> Item Code </th>
							<td> ${item.itemCode.itemCode }</td>
						</tr>
						<tr>
							<th> Price  </th>
							<td> ${item.itemPrice } </td>
						</tr>
						<tr>
							<th> Quantity </th>
							<td>${item.quantity} </td>
						</tr>
						<tr>
							<th> Total </th>
							<td>${item.totalPrice} </td>
						</tr>
						<tr>
							<th> OrderDate </th>
							<td>${item.orderDate} </td>
						</tr>
						<tr>
							<th> Delivery Date </th>
							<td>${item.deliveryDate} </td>
						</tr>
						<c:set var="message" value="block"/>
							<c:set var="button" value="none"/>
							<c:if test="${item.deliveryStatus==null}">
								<c:set var="message" value="none"/>
								<c:set var="button" value="block"/>
							</c:if>
							<c:if test="${item.deliveryStatus!=null}">
								<c:set var="message" value="block"/>
								<c:set var="button" value="none"/>
							</c:if>
							
						
						<tr>
							<th>
							<a class="btn btn-danger btn-sm"  href="/setdeliverydate/?id=${item.id}" style="display:${button};"  > Accept </a>
							<span style="display:${message};"> Delivery Status</span>
							</th>
							<td>
							 <span style="color:red;display:${message}">${item.deliveryStatus}</span>
							</td>
							
						</tr>
					</table>
				
				</div>
			</div>
					
			</c:forEach>
		</div>
	
	
	</div>

</body>
</html>