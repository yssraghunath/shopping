<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"	rel="stylesheet">
	

<title>customer order details</title>

<style type="text/css">
	.lbtn{
		color: black;
    	padding-left: 0px;
    	padding-right: 0px;
    	
   	}
   	.accept-btn{
   	padding-top: 0px;
    padding-bottom: 0px;
   
   	}
   	
   	.h{
   	 text-decoration:none;
   	 margin:2px;
   	 padding:0px;
   	 padding-left: 2px;
     padding-right: 2px;
     font-size:12px;
     border:solid 2px ;
    
     
    
   	}
   	.bg-red{
   		background-color:red;
   		color:white;
   		
   	}
   	.bg-green{
   		background-color:green;
   		color:white;
   	}
 
</style>

</head>
<body class="container-fluid alert-success">

	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>


	<c:set var="tableTitle"  value="Order Details"></c:set>
	<c:set var="list" scope="session" value="${orderList}"></c:set><br>
	<%-- <%@include file="table_filter.jsp" %> --%>
	<div class="row h6" style="margin:10px;background-color:white;padding:10px;">
		<div class="col-sm-3">
			Customer Name : ${orderList[0].userId.name}
    					
    					
		</div>
		<div class="col-sm-3">
		Mobile No : ${orderList[0].userId.mobile}
		</div>
		<div class="col-sm-3">
			Address : ${orderList[0].userId.address}
		</div>
		
		<div class="col-sm-3">
			<a href="customerinvoice?id=${list[0].orderId.id}"><button class="btn btn-sm"> Invoice  ${list[0].orderId.id} </button></a>
		</div>
		
	</div>
	
	<div class="row" style="margin:10px;">
		<table   class="table  table-sm table-bordered table-striped">
			<thead style="background-color:#abc">
			<tr>
				<th>S.N. </th>
				<th> Order No. </th>
				<th> Customer Id </th>
				<th> ItemCode </th>
				<th> Quantity </th>
				<th> price </th>
				<th> Total price </th>
				<th> Order Date </th>
				<th style="width:100px;padding:3px;"> Order status </th>
				<th> Delivery Date </th>
				<th style="width:120px;padding:2px;"> Delivery status </th>
				
				</tr>
			</thead>
			<tbody id="myTable" style="background-color:#cba">
				<c:forEach var="order" items="${list}" begin="${pageStart}" end="${pageStart + perPage -1}"  varStatus="loop" >
				<tr>
					<td>${order.id}</td>
				 	<td>${order.orderId.id}</td>
					<td>${order.userId.id}</td>
					<td> ${order.itemCode.itemCode}</td>
					<td>${order.quantity }</td>
					<td>${order.itemPrice }</td>
					<td>${order.totalPrice }</td>
					<td>${order.orderDate }</td>
					<td style="width:100px;padding:3px;">
						<c:set var="display1" value="block"></c:set>
						<c:set var="display2" value="none"></c:set>
						<c:set var="color" value="green"></c:set>	
						<c:choose>
							<c:when test="${order.orderStatus == 'ACCEPTED'}">
								<c:set var="display1" value="none"></c:set>
								<c:set var="display2" value="block"></c:set>
								<c:set var="color" value="green"></c:set>
							</c:when>
							<c:when test="${order.orderStatus == 'REJECTED'}">
								<c:set var="display1" value="none"></c:set>
								<c:set var="display2" value="block"></c:set>
								<c:set var="color" value="red"></c:set>
							</c:when>
							<c:otherwise>
								<c:set var="display1" value="block"></c:set>
								<c:set var="display2" value="none"></c:set>
				
							</c:otherwise>
						</c:choose>
				
						<div class="row" style="height:30px;padding-left:17px; display:${display1}"">
						<a class="h bg-green"  href="orderstatus?id=${order.id}&status=ACCEPTED">Accept</a>
						<a class="h bg-red"  href="orderstatus?id=${order.id}&status=REJECTED">Reject</a>
						</div>
						<div class="row" style="height:30px;padding-left:17px;display:${display2}"">
							<label style="color:${color}">${order.orderStatus}</label>
						</div>
					
					
					</td>
					<td>
				
						${order.deliveryDate }
						
					</td>
					<td  style="width:120px;padding:2px;">
					<c:set var="display3" value="block"></c:set>
					<c:set var="display4" value="none"></c:set>
					<c:set var="color" value="green"></c:set>
						<c:choose>
							<c:when test="${order.deliveryStatus == 'ACCEPTED'}">
								<c:set var="display3" value="none"></c:set>
								<c:set var="display4" value="block"></c:set>
								<c:set var="color" value="green"></c:set>
							</c:when>
							<c:when test="${order.deliveryStatus == 'REJECTED'}">
								<c:set var="display3" value="none"></c:set>
								<c:set var="display4" value="block"></c:set>
								<c:set var="color" value="red"></c:set>
							</c:when>
							<c:otherwise>
									<c:set var="display3" value="block"></c:set>
									<c:set var="display4" value="none"></c:set>
							
							</c:otherwise>
						</c:choose>
						
					<div class="row" style="height:30px; padding-left:20px;display:${display3}">
						<a class="h bg-green"  href="deliverystatus?id=${order.id}&status=ACCEPTED ">Accept</a>
						<a class="h bg-red"  href="deliverystatus?id=${order.id}&status=REJECTED">Reject</a>
					</div>
					<div class="row" style="height:30px; padding-left:20px;display:${display4}">
					<label style="color:${color}">	${order.deliveryStatus}</label>
					</div>	
					
					</td>
					
					
				</tr>
				
				</c:forEach>
				
			</tbody>
			
		</table>
	
	</div>
	<div class="row  justify-content-center">
		<a href="orders"><button class="btn btn-danger btn-sm" style="">Go Back </button></a>
		
	</div>
	

 
</body>
</html>