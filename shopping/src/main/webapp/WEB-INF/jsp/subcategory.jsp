<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>sub category</title>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css"  rel="stylesheet">


<style>
	.form-group{
		margin-top:10px;
	}
</style>

<script type="text/javascript">

function Validation(){
	
	
}

function autoFillFieldEdit(id,subCategory,category){
	debugger;
	document.forms['updateform']['id'].value=id;
	document.forms['updateform']['subCate'].value=subCategory;
	document.forms['updateform']['category'].value=category;
	
}
</script>


</head>
<body class="container-fluid alert-success">
	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>


	<div class="row" style="margin-top:10px;">
		<div class="col-sm-6 offset-sm-3">
		<div class="card">
			<div class="card-header">
			<h4>Add New SubCategory</h4>
			</div>
			<div class="card-body">
			
			<form name="myForm1" action="addsubcategory" modelAttribute="subCategoryInfo"	method="POST" class="form form-inline col-sm-12 "
			 onsubmit="return Validation()">

			<div class="form-group col-sm-9">
					<label class="col-sm-5"> Category </label> &nbsp; &nbsp;
					<!-- <input type="text" class="form-control col-sm-12" name="itemCategory" id="itemCategory" placeholder="" required> -->
				 <select name="cid" id="cid" class="form-control col-sm-6">
						<c:forEach var="cat" items="${catList}">
							<option value="${cat.id}">${cat.category}</option>
						</c:forEach>
						
					</select>
				
				
			</div>
			
			<div class="form-group col-sm-9" >
				<label class="col-sm-5">Sub Category <span class="req" style="color: Red">*</span></label> &nbsp; &nbsp;
				<input type="text" name="subCategory"	placeholder="Enter sub category Name " class="form-control col-sm-6" required>
			</div>
			

			<div class="form-group col-sm-3">
				<input type="submit" value="submit"
					class="btn btn-success btn-block">
			</div>


		</form>
			
			</div>
		</div>
		</div>
		</div>


<c:set var="tableTitle"  value="Category Details"></c:set>
	<c:set var="list" scope="session" value="${subCatList}"></c:set>
	
	<div class="row" style="font-size:14px;">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
				 	 	<%@include file="table_filter.jsp" %>  
				
					</div>
					<div class="card-body">
						<table class="table table-style table-sm table-bordered table-striped" >
					
						<thead class="table-header-style">
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Category</th>
						<th style="width:85px;">Action</th>
						</tr>
						</thead>
					
					
						<tbody id="myTable">
						<%-- begin="${pageStart}" end="${pageStart + perPage -1}" --%>
					<c:forEach  var="cat" items="${list}"  begin="${pageStart}" end="${pageStart + perPage -1}" varStatus="loop" > 
						<tr>
						
							<td>${cat.id}</td>
							 <td>${cat.subCategory}</td>
							<td>
							<%-- ${cat.category.category} --%>
							
							<select name="category" onchange="location = this.value;"  class="form-control form-control-sm" style="width:150px;">
								<c:forEach var="category" items="${catList}">
									<option value="/subcategoryupdate?id=${cat.id}&catId=${category.id}"  ${cat.category.id==category.id ?'selected':'' }>${category.category}</option>
								</c:forEach>
								
								
							</select>
							
							</td>
							<td style="width:85px;">
						
								
							 <div class="row">
							 	<span class="col-sm-3"><i class='fa fa-edit' style='font-size: 16px;color:skyblue'  data-toggle="modal" data-placement="top" title="Edit" data-target="#editModal" onClick="autoFillFieldEdit('${cat.id}','${cat.subCategory}','${cat.category.category}')"></i></span>
							 <%-- 	<span class="col-sm-3"><i class='fa fa-plus' style='font-size: 16px;color:darkgreen' data-toggle="modal" data-placement="top" title="Purchase" data-target="#purchaseModal" onClick="autoFillFieldPurchase('${item.id}','${item.itemName}','${item.quantity}','${item.itemMRP}','${item.imageUrl}')"></i></span> --%>
							 	<span class="col-sm-3"> <a href="/subcategorydelete?id=${cat.id}"><i class='fa fa-minus' style='font-size: 16px;color:darkred' data-toggle="modal" data-placement="top" title="delete" data-target="#saleModal"></i></a></span>
							 </div>
								
							</td> 											
						</tr>
						</c:forEach>
						
						
						</tbody>
												
				</table>
						
				
					</div>
				
				</div>
			
			</div>
	
	</div>
	
	


<div class="row">
		<!-- Button to Opne The modal -->
		
		<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> open modal</button> -->
		
		<!-- the  modal -->
		<div class="modal" id="editModal" >
		<div class="modal-dialog col-sm-12">
			<div class="modal-content">
			<form class="form-inline" action="subcategoryupdate" name="updateform" modelAttribute="subCategoryInfo">
				<div class="modal-header col-sm-12">
				
					<h4 class="modal-title"> Category Update </h4>
					<button type="button" class="close" data-dismiss="modal"> &times;</button>
				</div>
				
				<div class="modal-body">
					<div class="row">
					 <div class="col-sm-12">
						<div class="form-group " hidden>
							<label>subcat Id</label>
							<input type="text" class="form-control" name="id" id="id" >
						</div>
					
					
					<div class="form-group">
					
						<label class="col-sm-3">Category </label>
						<input type="text" class="form-control col-sm-6" name="category" id="category" disabled />
						<input type="text" class="form-control col-sm-6" name="catId" id="catId" value="0" hidden />
						
							
					</div>
					<div class="form-group">
							<label class="col-sm-3">Sub Category </label>
							<input type="text" class="form-control col-sm-6" name="subCate" id="subCat"/>
					</div>
				</div>
					
					</div>
							
				
				</div>
				<div class="modal-footer col-sm-12">
				<input type="submit" class="btn btn-success" value="update"  />
					
				
						<button type="button" class="btn btn-danger" data-dismiss="modal"> close</button>
				</div>
			</form>
					
			</div>
		 </div>
		
		</div>
	</div>
	



</body>
</html>