<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"
	rel="stylesheet">
<link
	href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"
	rel="stylesheet">
<link
	href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css"
	rel="stylesheet">
<title>Insert title here</title>
<style>
.row{
	margin:10px;
}
div {
	border: solid 1px #334333;
}
td{
border: solid 1px #334333;
}
</style>
</head>
<body class="container-fluid">

<!-- <p> ${previewData} </p>
<p> ${selfDetails} </p>
 -->
<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>
	<div class="row">

		<div class="col-sm-12" style="text-align: right">

			<h6>${selfDetails.companyName}</h6>
			<h6>GSTIN No:-${selfDetails.gstNo}</h6>
			<h6>PAN No:- ${selfDetails.panNo}</h6>
			<h6>CIN No:- ${selfDetails.cinNO}</h6>



		</div>
</div>
		
	<div class="row">
				<div class="col-sm-6">

					<div class="row" style="">
						<h5>Bill To (Buyer)</h5>
					</div>
					<div class="row">
						<p>${previewData[0].invoiceDetails.buyerDetails}</p>
					</div>
					<div class="row">
						<h5>Shipped To (Site)</h5>
					</div>
					<div class="row">
								<p>${previewData[0].invoiceDetails.siteDetails}</p>
					</div>

				</div>
				<div class="col-sm-6">
					<div class="row">
						<div class="col-sm-6">
							Invoice No:-
							<h6>${previewData[0].invoiceDetails.invoiceNo}${previewData[0].invoiceDetails.id} </h6>

						</div>
						<div class="col-sm-6">Date:- ${previewData[0].invoiceDetails.invoiceDate}</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							Client's/PO/Ref No:-
							<h6>${previewData[0].invoiceDetails.clientPoNo}</h6>

						</div>
						<div class="col-sm-6">
							PO/SO Date
							<h6>${previewData[0].invoiceDetails.poDate}</h6>

						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							Quote/ACC/ Ref No:-
							<h6>${previewData[0].invoiceDetails.quoteRefNo}</h6>

						</div>
						<div class="col-sm-6">
							Due Date:-
							<h6>${previewData[0].invoiceDetails.dueDate}</h6>
						</div>
					</div>
					<div class="row">
					AMC Duration: ${previewData[0].invoiceDetails.amcDateFrom} to ${previewData[0].invoiceDetails.amcDateTo}.
					Payment Mode:- ${previewData[0].invoiceDetails.paymentMode}.
						
						</div>
				</div>


			</div>

		
		<div class="row" style="background-color:white;padding:10px;margin:10px;">
			<table class="table">
				<thead>
					<tr>
						<th>Item Name </th>
						<th>Quantity</th>
						<th> Price </th>
						<th> SGST </th>
						<th> CGST </th>
						<th> Total </th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${previewData}">
					<tr>
						<td>${item.itemName}</td>
						<td>1</td>
						<td>${item.taxAblePrice}</td>
						<td>${item.sgst}</td>
						<td>${item.cgst}</td>
						<td>${item.totalprice }</td>
						
					
					</tr>
					</c:forEach>
					
				
				</tbody>
			
			</table>
			
			<!-- 	<p class="col-sm-12 h5" style="text-align:center">Project Ref :-AMC - IVRS </p>				
				<form action="/savepurchase" method="POST" modelAttribute="purchaseInfo" class="form form-inline col-sm-12 alert-success" style="padding:10px;" > 
				<div class="form-group col-sm-2" >
					<label class="col-sm-12">Item Name</label>
						<input type="text" class="form-control col-sm-12" name="itemName" id="itemName" placeholder="">
					</div>
				<div class="form-group col-sm-2">
					<label> Company Name </label>
					<input type="text" class="form-control col-sm-12" name="companyName" id="companyName" placeholder="">
				</div>
				
				<div class="form-group col-sm-1">
					<label> Quantity </label>
					<input type="text" class="form-control col-sm-12" onKeyUp="calculatePrice()"  value="0" id="quantity" name="quantity" placeholder="">
				</div>
				<div class="form-group col-sm-2">
					<label> Price (RS.)</label>
					<input type="text" class="form-control" value="0.0" onKeyUp="calculatePrice()" id="price" name="price" placeholder="">
				</div>
				<div class="form-group col-sm-1">
					<label>SGST (%) </label>
					<input type="text" class="form-control col-sm-12" value="0" name="sgst" onKeyUp="calculatePrice()" id="sgst"  placeholder="">
				</div>
				<div class="form-group col-sm-1">
					<label>CGST (%) </label>
					<input type="text" class="form-control col-sm-12" value="0" name="cgst" onKeyUp="calculatePrice()" id="cgst"  placeholder="">
				</div>
					<div class="form-group col-sm-2">
					<label>Total</label>
					<input type="text" class="form-control" name="total" id="total" placeholder="" >
				</div>
			
				
				<div class="form-group col-sm-1" style="padding-top:10px;">
					
					<input type="submit" class="btn btn-success btn-block" value="Add" style="margin-top:10px;" name="" placeholder="">
				</div>
				
			</form>
			 -->
			
	</div>
	
	<div class="row">
				<div class="col-sm-6">

					<div class="row">
						<h5>Registered Address</h5>
					</div>
					<div class="row">
						${selfDetails.registerAddress}


					</div>
				</div>
	
			<div class="col-sm-6">

					<div class="row">
						<h5>BCorporate Address</h5>
					</div>
					<div class="row">
						${selfDetails.corporateAddress}

					</div>
					
				</div>
				
	</div>
	<div class="row">
	<ul >
		<li style="color:black"> Declaration-- We declare that this invoice shows the actual price of the goods/Services described and that all particulars   are True and correct. </li>
		<li style="color:black"> This Invoice is recognized subject to payment realization. </li>
		<li style="color:black" > All Disputes are subject to Delhi Jurisdiction only.</li>
	</ul>		
			
			
			
				
				
			
	</div>
	
<div class="row">
<div class="col-12 h5">Bank Details for Payment </div>
	<p class="col-12">Name of the Bank:-  ${selfDetails.bank.bankName} </p> 
	<P class="col-12">Account No:-  <b> ${selfDetails.bank.accountNo} </b> </P>
	<P class="col-12">IFSC/NEFT Code:-<b>  ${selfDetails.bank.ifscCode}</b></P>
	<P class="col-12">Branch:-  ${selfDetails.bank.bankAddress}</P>
	 
	</div>
</body>
</html>