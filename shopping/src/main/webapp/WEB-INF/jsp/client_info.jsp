<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- <link href='webjars/font-awesome/4.0.0/css/font-awesome.css'>
<link href='webjars/font-awesome/4.0.0/css/font-awesome.min.css'>
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"  rel="stylesheet">




<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
 -->
<!-- <link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.4.2/css/all.css'
	integrity='sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns'
	crossorigin='anonymous'> -->
<!-- <link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.css" rel="stylesheet">
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
 -->

<title>Client Detail</title>
</head>
<body class="container-fluid alert-success">
	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>


	<div class="row" style="margin-top:10px;">
		<div class="col-sm-12">
		<div class="card">
			<div class="card-header">
			<h4>Add Client Info</h4>
			</div>
			<div class="card-body">
			
			<form name="myForm1" action="addclient" modelAttribute="clintInfo"	method="POST" class="form form-inline col-sm-12 "
			 onsubmit="return Validation()">

			<div class="form-group col-sm-2">
				<label>Company Name<span class="req" style="color: Red">*</span></label>
				<input type="text" name="companyName"	placeholder="Enter Company Name " class="form-control" required>
			</div>

			<div class="form-group col-sm-2">
				<label>GST NO. <span class="req" style="color: Red">*</span></label>
				<input type="text" name="gstNo" placeholder="Enter Client GST NO  "
					class="form-control" >
			</div>

			<div class="form-group col-sm-2">
				<label>Address <span class="req" style="color: Red">*</span></label>
				<input type="text" name="address"
					placeholder="Enter Client Address " class="form-control" required>
			</div>
			<div class="form-group col-sm-2">
				<label>Email Id <span class="req" style="color: Red">*</span></label>
				<input type="email" name="email"
					placeholder="Enter Client Email Id " class="form-control" required>
			</div>
			<div class="form-group col-sm-2">
				<label>Contact No. <span class="req" style="color: Red">*</span></label>
				<input type="number" name="contactNo"
					placeholder="Enter Client Contact No. " class="form-control"
					required>
			</div>

			<div class="form-group col-sm-2" style="padding-top: 25px;">
				<input type="submit" value="submit"
					class="btn btn-success btn-block">
			</div>


		</form>
			
			</div>
		</div>
		</div>
		</div>
	<c:set var="tableTitle"  value="Client Details"></c:set>

	<c:set var="list" scope="session" value="${clientList}"></c:set>
		
		<!-- <div class="col-sm-12">
			<h4>Add Client Info</h4>
		</div>
 -->
		<!-- <form name="myForm1" action="addclient" modelAttribute="clintInfo"
			method="POST" class="form form-inline col-sm-12 alert-success"
			style="padding: 20px; margin: 10px" onsubmit="return Validation()">

			<div class="form-group col-sm-2">
				<label>Company Name<span class="req" style="color: Red">*</span></label>
				<input type="text" name="companyName"
					placeholder="Enter Company Name " class="form-control" required>
			</div>

			<div class="form-group col-sm-2">
				<label>GST NO. <span class="req" style="color: Red">*</span></label>
				<input type="text" name="gstNo" placeholder="Enter Client GST NO  "
					class="form-control" >
			</div>

			<div class="form-group col-sm-2">
				<label>Address <span class="req" style="color: Red">*</span></label>
				<input type="text" name="address"
					placeholder="Enter Client Address " class="form-control" required>
			</div>
			<div class="form-group col-sm-2">
				<label>Email Id <span class="req" style="color: Red">*</span></label>
				<input type="email" name="email"
					placeholder="Enter Client Email Id " class="form-control" required>
			</div>
			<div class="form-group col-sm-2">
				<label>Contact No. <span class="req" style="color: Red">*</span></label>
				<input type="number" name="contactNo"
					placeholder="Enter Client Contact No. " class="form-control"
					required>
			</div>

			<div class="form-group col-sm-2" style="padding-top: 25px;">
				<input type="submit" value="submit"
					class="btn btn-success btn-block">
			</div>


		</form>
 -->
	
<%-- 		<%@include file="table_filter.jsp" %> --%>
	<%-- <c:set var="totalCount" scope="session" value="${list.size()}"></c:set>
	<c:set var="perPage" scope="session" value="${10}"></c:set>
	<c:set var="startPage" value="${param.start}"></c:set>
	<c:if test="${empty startPage or startPage < 0 }">
		<c:set var="startPage" value="0"></c:set>
	</c:if>
	<c:if test="${totalCount < startPage}">
		<c:set var="startPage" value="${paramStart-perPage}"></c:set>
	</c:if> --%>
	
		<div class="row" style="font-size:14px;margin-top:10px;">
			<div class="col-sm-12">
			<div class="card">
			
				<div class="card-header">
						<%@include file="table_filter.jsp" %>
			
				</div>
				<div class="card-body">
					<table  class="table  table-style   table-sm  table-hover table-bordered table-striped">
			<thead class="table-header-style">
				<tr>
					<th>ID</th>

					<th>Company Name</th>
					<th>GST NO</th>
					<th>Address</th>
					<th>Email Id</th>
					<th>Contact NO</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="myTable" >
				
				<c:forEach var="client" items="${list}" begin="${pageStart}" end="${pageStart + perPage -1}" varStatus="loop" >

					<tr>
						<td>${client.id}</td>
						<td>${client.companyName}</td>
						<td>${client.gstNo}</td>
						<td>${client.address}</td>
						<td>${client.email}</td>
						<td>${client.contactNo}</td>
						<td><span><i
								onClick='editClientDetails(${client.id},"${client.gstNo}","${client.companyName}","${client.address}","${client.email}","${client.contactNo}")'
								class='fas fa-edit' data-toggle="modal" data-target="#myModal"
								style='font-size: 16px; color: green;'></i></span> <span><a
								href="deleteupdate/${client.id}"
								onClick=" return deleteConfirmation()">
								<i
									class='fa fa-trash' style='font-size: 16px; color: red;'></i></a></span></td>
					</tr>
				</c:forEach>
			</tbody>
		
		</table>
					
			
				</div>
		
						</div>
		</div>
		</div>
		

	<%-- <div class="row table-cover" >
		
		<table  class="table  table-style table-responsive  table-sm  table-hover table-bordered table-striped">
			<thead class="table-header-style">
				<tr>
					<th>ID</th>

					<th>Company Name</th>
					<th>GST NO</th>
					<th>Address</th>
					<th>Email Id</th>
					<th>Contact NO</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="myTable" >
				
				<c:forEach var="client" items="${list}" begin="${pageStart}" end="${pageStart + perPage -1}" varStatus="loop" >

					<tr>
						<td>${client.id}</td>
						<td>${client.companyName}</td>
						<td>${client.gstNo}</td>
						<td>${client.address}</td>
						<td>${client.email}</td>
						<td>${client.contactNo}</td>
						<td><span><i
								onClick='editClientDetails(${client.id},"${client.gstNo}","${client.companyName}","${client.address}","${client.email}","${client.contactNo}")'
								class='fas fa-edit' data-toggle="modal" data-target="#myModal"
								style='font-size: 16px; color: green;'></i></span> <span><a
								href="deleteupdate/${client.id}"
								onClick=" return deleteConfirmation()">
								<i
									class='fa fa-trash' style='font-size: 16px; color: red;'></i></a></span></td>
					</tr>
				</c:forEach>
			</tbody>
		
		</table>
	</div>

 --%>


	<!-- <script type="text/javascript">
	$(document).ready(function() {
	    $('#clientId_tlb').DataTable();
	} );
	</script>
	 -->


	<div class="row">
		<!-- Button to Opne The modal -->

		<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> open modal</button> -->

		<!-- the  modal -->
		<div class="modal" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<form action="clientUpdate" method="POST" name="myForm"
						modelAttribute="clientInfo">
						<div class="modal-header">

							<h4 class="modal-title">Client Details</h4>
							<button type="button" class="close" data-dismiss="modal">
								&times;</button>
						</div>

						<div class="modal-body">
							<div class="form-group" hidden>
								<label>Client ID</label> <input type="text" class="form-control"
									name="id">
							</div>

							<div class="form-group">
								<label>GST No </label> <input type="text" class="form-control"
									name="gstNo" />
							</div>
							<div class="form-group">
								<label>Company Name</label> <input type="text"
									class="form-control" name="companyName" />
							</div>
							<div class="form-group">
								<label>Address</label> <input type="text" class="form-control"
									name="address" />
							</div>
							<div class="form-group">
								<label>Email Id</label> <input type="text" class="form-control"
									name="email" />
							</div>
							<div class="form-group">
								<label>Contact No.</label> <input type="text"
									class="form-control" name="contactNo" />
							</div>



						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-success" value="update" />


							<button type="button" class="btn btn-danger" data-dismiss="modal">
								close</button>
						</div>
					</form>

				</div>
			</div>

		</div>
	</div>


	<script type="text/javascript">
	
	function editClientDetails(id,gstNo,companyName,address,email,contactNo){
		debugger;
		
		//document.getElementByName("clientName").value=clientName;
		document.forms['myForm']['id'].value = id;
		document.forms['myForm']['gstNo'].value = gstNo;
		document.forms['myForm']['companyName'].value = companyName;
		document.forms['myForm']['address'].value = address;
		document.forms['myForm']['email'].value = email;
		document.forms['myForm']['contactNo'].value = contactNo;
		
		
	}
	
	function deleteConfirmation(){
		var txt;
		if(confirm("Do You Sure want to delete ?")){
			txt="You pressed OK !"
			return true;
		}else{
			txt="You Pressed Cancel !";
			return false;
		}
	}
</script>

	<script type="text/javascript">
function Validation()
{
var a = document.myForm1.contactNo.value;
if(a=="")
{
alert("please Enter the Contact Number");
document.myForm1.contactNo.focus();
return false;
}
if(isNaN(a))
{
alert("Enter the valid Mobile Number(Like : 9566137117)");
document.myForm1.contactNo.focus();
return false;
}
if((a.length < 1) || (a.length > 10))
{
alert(" Your Mobile Number must be 1 to 10 Integers");
document.myForm1.contactNo.select();
return false;
}

/* var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i
	var b=emailfilter.test(document.form2.mailid.value);
	if(b==false)
	{
	alert("Please Enter a valid Mail ID");
	document.myForm1.email.focus();
	return false;
	} */
}
</script>

</body>
</html>