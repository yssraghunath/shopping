<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body class="container alert-success">

<div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
</div>
<div class="row"  style="margin-top:15%;">
	<div class="col-sm-4 offset-sm-4" style="border:solid white 2px;background-color:#aabbcc">
		<h3>Changer Password</h3>
		<form action="changerpassword" method="POST">
		
		<div class="form-group">
			<input type="text" class="form-control" name="id" value="${user.id}"  hidden >
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="id" value="${user.userName}" disabled>
		</div>
		
		<div class="form-group">
			<input type="password" class="form-control" name="password" placeholder="Enter new password">
		</div>
		<div class="form-group">
			<input type="password" class="form-control" name="confirmPassword" placeholder="Enter confirm password">
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-default btn-block" value="Submit"/>
		</div>
	</form>
	
	</div>
	

</div>

</body>
</html>