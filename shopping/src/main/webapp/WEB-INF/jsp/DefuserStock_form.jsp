<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css"  rel="stylesheet">


<script>

$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();   
	});

	function calculatePriceSale(){
		var quantity=document.forms['saleForm']['quantity'].value;
		var price=document.forms['saleForm']['itemPrice'].value;
		console.log(quantity);
		console.log(price);
		var totalPrice=Number(quantity)*Number(price);
		document.forms['saleForm']['totalPrice'].value=totalPrice;
	}
	function calculatePricePurchase(){
		
		var quantity=document.forms['purchaseForm']['quantity'].value;
		var price=document.forms['purchaseForm']['itemPrice'].value;
		console.log(quantity);
		console.log(price);
		var totalPrice=Number(quantity)*Number(price);
		document.forms['purchaseForm']['totalPrice'].value=totalPrice;
	}
	
	
	function autoFillFieldSale(id,name,quantity,price,imageUrl){
		
		var image='<img alt="defuser" src="resources/images/items/'+imageUrl+'" style="width:100%;height:100%;">';
		document.getElementById('SitemImage').innerHTML=image;
		document.forms['saleForm']['itemId'].value = id;
		document.forms['saleForm']['itemName'].value = name;
		document.forms['saleForm']['quantity'].value = quantity;
		document.forms['saleForm']['itemPrice'].value = price;
		document.forms['saleForm']['totalPrice'].value = ((Number(quantity))* (Number(price)));
	
	}
	function autoFillFieldPurchase(id,name,quantity,price,imageUrl){
		
		var image='<img alt="defuser" src="resources/images/items/'+imageUrl+'" style="width:100%;height:100%;">';
		document.getElementById('PitemImage').innerHTML=image;
		document.forms['purchaseForm']['itemId'].value = id;
		document.forms['purchaseForm']['itemName'].value = name;
		document.forms['purchaseForm']['quantity'].value = quantity;
		document.forms['purchaseForm']['itemPrice'].value = price;
		document.forms['purchaseForm']['totalPrice'].value = ((Number(quantity))* (Number(price)));
		
	}

	function autoFillFieldEdit(id,itemName,itemCode,itemCodeV,itemDescription,itemCategory,itemSubCategory,quantity,itemMRP,itemOLP,itemTP,itemOSP,imageUrl){
	
		var image='<img alt="defuser" src="resources/images/items/'+imageUrl+'" style="width:100%;height:100%;">';
		document.getElementById('editImage').innerHTML=image;
		document.forms['updateForm']['id'].value = id;
		document.forms['updateForm']['itemName'].value =itemName;
		document.forms['updateForm']['itemCode'].value = itemCode;
		document.forms['updateForm']['itemCodeV'].value = itemCodeV;
		document.forms['updateForm']['itemDescription'].value =itemDescription;
		document.forms['updateForm']['itemCategory'].value =itemCategory;
		document.forms['updateForm']['itemSubCategory'].value =itemSubCategory;
		document.forms['updateForm']['quantity'].value = quantity;
		document.forms['updateForm']['itemMRP'].value =itemMRP;
		/* document.forms['updateForm']['itemPrice'].value = price; */
		document.forms['updateForm']['itemOLP'].value =itemOLP;
		document.forms['updateForm']['itemTP'].value =itemTP;
		document.forms['updateForm']['itemOSP'].value =itemOSP;
		document.forms['updateForm']['imageUrl'].value =imageUrl;
		/* document.forms['updateForm']['totalPrice'].value = ((Number(quantity))* (Number(price))); */
		
	}

	
	</script>

<title>stock item details</title>
</head>
<body class="container-fluid alert-success">
	


	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>
	
 		
	<div class="row" style="background-color:white;padding:10px;margin:10px;">
			
				<p class="col-sm-12 h4" >Item Form</p>				
				<form action="savedefuseritem" method="POST" modelAttribute="item" class="form form-inline col-sm-12 alert-success" style="padding:10px;" > 
				<div class="form-group col-sm-2" >
					<label>Item Code</label>
					<input type="text" class="form-control col-sm-12" name="itemCode" id="itemCode" placeholder="" required>
			
				</div>
				<div class="form-group col-sm-2" >
					<label>Vendor Item Code</label>
					<input type="text" class="form-control col-sm-12" name="itemCodeV" id="itemCodeV" placeholder="" >
			
				</div>
			
				<div class="form-group col-sm-2" >
					<label>Item Name</label>
					<input type="text" class="form-control col-sm-12" name="itemName" id="itemName" placeholder="" required>
			
				</div>
				
				<div class="form-group col-sm-2">
					<label> Item Description  </label>
					<input type="text" class="form-control col-sm-12" name="itemDescription" id="itemDescription" placeholder="">
				</div>
				
				<div class="form-group col-sm-2">
					<label> Category </label>
					<!-- <input type="text" class="form-control col-sm-12" name="itemCategory" id="itemCategory" placeholder="" required> -->
					<select name="itemCategory" id="itemCategory" class="form-control col-sm-12" onchange="hello()">
						<c:forEach var="cat" items="${category}">
							<option value="${cat.id}">${cat.category}</option>
						</c:forEach>
						
					</select>
				</div>
				
				<div class="form-group col-sm-2">
					<label>Sub Category </label>
					<!-- <input type="text" class="form-control col-sm-12" name="itemSubCategory" id="itemSubCategory" placeholder="" required> -->
						<select name="itemSubCategory" id="itemSubCategory" class="form-control col-sm-12">
						
					</select>
				</div>
							
				<div class="form-group col-sm-2">
					<label>Quantity  </label>
					<input type="text" class="form-control col-sm-12" value="0" name="quantity"  id="quantity"  placeholder="" required>
				</div>
				<div class="form-group col-sm-2">
					<label>Item MRP </label>
					<input type="text" class="form-control col-sm-12" value="0" name="itemMRP"  id="itemMRP"  placeholder="" required>
				</div>
				
				<div class="form-group col-sm-2">
					<label>Item OLP </label>
					<input type="text" class="form-control col-sm-12" value="0" name="itemOLP"  id="itemOLP"  placeholder="" required>
				</div>
				
							
				<div class="form-group col-sm-2">
					<label> Transfer Price  </label>
					<input type="text" class="form-control col-sm-12" value="0" name="itemTP"  id="itemTP"  placeholder="">
				</div>
				<div class="form-group col-sm-2">
					<label>Item OSP </label>
					<input type="text" class="form-control col-sm-12" value="0" name="itemOSP"  id="itemOSP"  placeholder="">
				</div>
				
					

	
	
				
					
				<div class="form-group col-sm-2" style="padding-top:10px;">
					
					<input type="submit" class="btn btn-success btn-block" value="Add" style="margin-top:10px;" name="" placeholder="">
				</div>
				
			</form>
			
			
	</div>
	
	<div class="row justify-content-center" style="margin:10px;padding:10px;border:solid 10px white">
		<form  name="uploadForm" class="form-inline" method="post" action="uploadfile" onsubmit="return uploadFile();" enctype="multipart/form-data">
			<div class="form-group">
			<label style="margin-right:20px;">Upload Item Record in Excel File </label>
				<input type="file" class="form-control form-control-sm" id="uploadFile" name="file" required>
				
			</div>
			<input  class="btn btn-sm btn-success"   style="margin-left:10px;" type="submit" value="Upload" required>
			<!-- <img alt="help" src="resources/images/imp_img/question.jpg" style="width:30px;height:30px;margin-left:50px;" > -->
			<a href="resources/excels/excel.png" target="new" style="color:white"><span class="h4" style="padding-left:10px;padding-right:10px;background-color:red;border-radius:50%;"><abbr title="HELP"> ? </abbr>  </span></a>
			<p> ${message}</p>
		</form>
	</div>
	<script type="text/javascript">
	
function uploadFile(){
	alert('hello file');	
	
		var result=false;
		document.forms['uploadForm']['fileUpload'].required=true;
		File f=document.forms['uploadForm']['fileUpload'].value;
		console.log("----------- ",f)
		return true;
	}
	
	</script>
	
	
	
	
	<c:set var="tableTitle"  value="Invoice Details"></c:set>
	<c:set var="list" scope="session" value="${itemList}"></c:set>
	
	<div class="row" style="font-size:14px;">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						<%@include file="table_filter.jsp" %>
				
					</div>
					<div class="card-body">
						<table class="table table-style table-sm table-bordered table-striped" >
					
						<thead class="table-header-style">
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Item Code</th> 
							<th>Vendor Code </th>
							<th>Description</th>
							<th>Category</th>
							<th>Sub Category</th>
							<th>Stock</th>
							<th>MRP</th>
							<th>OLP</th>
							<th>TP</th>
							<th>OSP</th>
							<th style="width:85px;">Action</th>
						</tr>
						</thead>
					
					
						<tbody id="myTable">
						<c:forEach  var="item" items="${list}" begin="${pageStart}" end="${pageStart + perPage -1}" varStatus="loop" >
						<tr>
						
							<td>${item.id}</td>
							<td>${item.itemName}</td>
							<td>${item.itemCode}</td> 
							<td>${item.itemCodeV}</td>
							<td>${item.itemDescription}</td>
							<td>${item.itemCategory}</td>
							<td>${item.itemSubCategory}</td>
							<td>${item.quantity} </td>
							<td>${item.itemMRP}</td>
							<td>${item.itemOLP}</td> 
							<td>${item.itemTP}</td>
							<td>${item.itemOSP}</td>
							
							<td style="width:85px;">
								<%-- <div class="row">
									
									<div class="col-sm-4" style="padding-right:0px;">
											
										<button class="btn btn-danger btn-block btn-sm" data-toggle="modal" data-target="#editModal" onClick="autoFillFieldEdit('${item.id}','${item.itemName}','${item.itemCode}','${item.itemCodeV}','${item.itemDescription}','${item.itemCategory}','${item.itemSubCategory}','${item.quantity}','${item.itemMRP}','${item.itemMRP}','${item.itemOSP}','${item.itemOLP}','${item.imageUrl}')" style="display:${display}">Edit</button>
										
									</div>
									<div class="col-sm-4" style="padding-right:4px;" >
										<button class="btn btn-success btn-block btn-sm" data-toggle="modal" data-target="#purchaseModal" onClick="autoFillFieldPurchase('${item.id}','${item.itemName}','${item.quantity}','${item.itemMRP}','${item.imageUrl}')">purchase</button>
									</div>
									<c:set var="display" value="block"></c:set>
									<c:if test="${item.quantity <= 0 }">
										<c:set var="display" value="none"></c:set>
										
									</c:if>
									<div class="col-sm-4" style="padding-left:2px;">
											
										<button class="btn btn-danger btn-block btn-sm" data-toggle="modal" data-target="#saleModal" onClick="autoFillFieldSale('${item.id}','${item.itemName}','${item.quantity}','${item.itemMRP}','${item.imageUrl}')" style="display:${display}">sale</button>
										
									</div>
																		
								</div>                                                                                 data-toggle="tooltip" data-placement="top" title="Hooray!"
							 --%>	
							 <div class="row">
							 	<span class="col-sm-3"><i class='fa fa-edit' style='font-size: 16px;color:skyblue'  data-toggle="modal" data-placement="top" title="Edit" data-target="#editModal" onClick="autoFillFieldEdit('${item.id}','${item.itemName}','${item.itemCode}','${item.itemCodeV}','${item.itemDescription}','${item.itemCategory}','${item.itemSubCategory}','${item.quantity}','${item.itemMRP}','${item.itemOLP}','${item.itemTP}','${item.itemOSP}','${item.imageUrl}')"></i></span>
							 	<span class="col-sm-3"><i class='fa fa-plus' style='font-size: 16px;color:darkgreen' data-toggle="modal" data-placement="top" title="Purchase" data-target="#purchaseModal" onClick="autoFillFieldPurchase('${item.id}','${item.itemName}','${item.quantity}','${item.itemMRP}','${item.imageUrl}')"></i></span>
							 	<span class="col-sm-3"><i class='fa fa-minus' style='font-size: 16px;color:darkred' data-toggle="modal" data-placement="top" title="Sale" data-target="#saleModal" onClick="autoFillFieldSale('${item.id}','${item.itemName}','${item.quantity}','${item.itemMRP}','${item.imageUrl}')"></i></span>
							 </div>
								
							</td>											
						</tr>
						</c:forEach>
						
						
						</tbody>
												
				</table>
						
				
					</div>
				
				</div>
			
			</div>
	
	</div>
	
	
	
	
	
				
		 
	<div class="row">
		<!-- Button to Opne The modal -->
		
		<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> open modal</button> -->
		
		<!-- the  modal -->
		<div class="modal" id="saleModal" >
		<div class="modal-dialog modal-lg"">
			<div class="modal-content">
		
			<form action="saledefuseritem" method="POST" name="saleForm" modelAttribute="item">
				<div class="modal-header">
				
					<h4 class="modal-title">Item Sale</h4>
					<button type="button" class="close" data-dismiss="modal"> &times;</button>
				</div>
				
				<div class="modal-body">
					<div class="row">
					
		
						<div class="col-sm-6" id="SitemImage">
						<!-- here showing your image  don't remove it-->
						</div>
						
						<div class="col-sm-6">
						
						
						<div class="form-group" hidden>
							<label>item Id</label>
							<input type="text" class="form-control" name="itemId" >
						</div>
					
					<div class="form-group">
							<label>Buyer Name</label>
							<input type="text" class="form-control" name="buyerName"/>
					</div>
			
					
						<div class="form-group">
							<label>Item Name </label>
							<input type="text" class="form-control" name="itemName" id="itemName" />
						</div>
						<div class="form-group">
							<label>Quantity</label>
							<input type="text" class="form-control" name="quantity" id="quantity" onKeyUp="calculatePriceSale()" />
						</div>
						<div class="form-group">
							<label>Price</label>
							<input type="text" class="form-control" name="itemPrice" id="itemPrice" onKeyUp="calculatePriceSale()" />
						</div>
						<div class="form-group">
							<label>Total</label>
							<input type="text" class="form-control" name="totalPrice" id="totalPrice"/>
						</div>
			
			
					</div>		
				
				</div>
				</div>
				<div class="modal-footer">
				<input type="submit" class="btn btn-success" value="Sale"  />
					
				
						<button type="button" class="btn btn-danger" data-dismiss="modal"> close</button>
				</div>
				
			
			</form>
			
				
				
			</div>
		 </div>
		
		</div>
	</div>
	
	
	<div class="row">
		<!-- Button to Opne The modal -->
		
		<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> open modal</button> -->
		
		<!-- the  modal -->
		<div class="modal" id="purchaseModal" >
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			<form action="purchasedefuseritem" method="POST" name="purchaseForm" modelAttribute="item">
				<div class="modal-header">
				
					<h4 class="modal-title"> Item Purchase </h4>
					<button type="button" class="close" data-dismiss="modal"> &times;</button>
				</div>
				
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6" id="PitemImage">
								<!-- here showing image..  don't remove it -->
						</div>
						<div class="col-sm-6">
						<div class="form-group" hidden>
							<label>item Id</label>
							<input type="text" class="form-control" name="itemId" id="itemId" >
						</div>
					
					<div class="form-group">
							<label>Supplier Name</label>
							<input type="text" class="form-control" name="vendorName" id="venderName"/>
					</div>
					
						<div class="form-group">
							<label>Item Name </label>
							<input type="text" class="form-control" name="itemName" id="itemName" />
						</div>
						<div class="form-group">
							<label>Quantity</label>
							<input type="text" class="form-control" name="quantity" id="quantity" onKeyUp="calculatePricePurchase()" />
						</div>
						<div class="form-group">
							<label>Price</label>
							<input type="text" class="form-control" name="itemPrice" id="itemPriec" onKeyUp="calculatePricePurchase()"/>
						</div>
						<div class="form-group">
							<label>Total</label>
							<input type="text" class="form-control" name="totalPrice" id="totalPrice"/>
						</div>
			
			
						
						</div>
					
					</div>
							
				
				</div>
				<div class="modal-footer">
				<input type="submit" class="btn btn-success" value="Purchase"  />
					
				
						<button type="button" class="btn btn-danger" data-dismiss="modal"> close</button>
				</div>
			</form>
					
			</div>
		 </div>
		
		</div>
	</div>
	

<div class="row">
		<!-- Button to Opne The modal -->
		
		<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> open modal</button> -->
		
		<!-- the  modal -->
		<div class="modal" id="editModal" >
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			<form action="updatedefuseritem" method="POST" name="updateForm" modelAttribute="item">
				<div class="modal-header">
				
					<h4 class="modal-title"> Update Form </h4>
					<button type="button" class="close" data-dismiss="modal"> &times;</button>
				</div>
				
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6 offset-sm-3" id="editImage">
								<!-- here showing image..  don't remove it -->
						</div>
					</div>
					<div class="row">
						
						<div class="form-group col-sm-6" hidden>
							<label>item Id</label>
							<input type="text" class="form-control" name="id" id="id" >
						</div>
					
			 	<div class="form-group" hidden>
							<label>imageURl</label>
							<input type="text" class="form-control" name="imageUrl" id="imageUrl"/>
					</div>
				<!--		
						<div class="form-group">
							<label>Item Name </label>
							<input type="text" class="form-control" name="itemName" id="itemName" />
						</div>
						<div class="form-group">
							<label>Quantity</label>
							<input type="text" class="form-control" name="quantity" id="quantity" onKeyUp="calculatePricePurchase()" />
						</div>
						<div class="form-group">
							<label>Price</label>
							<input type="text" class="form-control" name="itemPrice" id="itemPriec" onKeyUp="calculatePricePurchase()"/>
						</div>
						<div class="form-group">
							<label>Total</label>
							<input type="text" class="form-control" name="totalPrice" id="totalPrice"/>
						</div> -->
						
		<div class="form-group col-sm-6" >
					<label>Item Code</label>
					<input type="text" class="form-control col-sm-12" name="itemCode" id="itemCode1" placeholder="">
			
				</div>				
			
			
			
			<div class="form-group col-sm-6" >
					<label>Vendor Item Code</label>
					<input type="text" class="form-control col-sm-12" name="itemCodeV" id="itemCodeV1" placeholder="">
			
				</div>
			
				<div class="form-group col-sm-6" >
					<label>Item Name</label>
					<input type="text" class="form-control col-sm-12" name="itemName" id="itemName1" placeholder="">
			
				</div>
				
				<div class="form-group col-sm-6">
					<label> Item Description  </label>
					<input type="text" class="form-control col-sm-12" name="itemDescription" id="itemDescription1" placeholder="">
				</div>
				
				<div class="form-group col-sm-6">
					<label> Category </label>
					<input type="text" class="form-control col-sm-12" name="itemCategory" id="itemCategory1" placeholder="">
				</div>
				
				<div class="form-group col-sm-6">
					<label>Sub Category </label>
					<input type="text" class="form-control col-sm-12" name="itemSubCategory" id="itemSubCategory1" placeholder="">
				</div>
							
				<div class="form-group col-sm-6" hidden>
					<label>Quantity  </label>
					<input type="text" class="form-control col-sm-12" value="0" name="quantity"  id="quantity1"  placeholder="">
				</div>
				<div class="form-group col-sm-6">
					<label>Item MRP </label>
					<input type="text" class="form-control col-sm-12" value="0" name="itemMRP"  id="itemMRP1"  placeholder="">
				</div>
				
				<div class="form-group col-sm-6">
					<label>Item OLP </label>
					<input type="text" class="form-control col-sm-12" value="0" name="itemOLP"  id="itemOLP1"  placeholder="">
				</div>
				
							
				<div class="form-group col-sm-6">
					<label> Transfer Price  </label>
					<input type="text" class="form-control col-sm-12" value="0" name="itemTP"  id="itemTP1"  placeholder="">
				</div>
				<div class="form-group col-sm-6">
					<label>Item OSP </label>
					<input type="text" class="form-control col-sm-12" value="0" name="itemOSP"  id="itemOSP1"  placeholder="">
				</div>
			
			
						
					
					
					</div>
							
				
				</div>
				<div class="modal-footer">
				<input type="submit" class="btn btn-success" value="Update"  />
					
				
						<button type="button" class="btn btn-danger" data-dismiss="modal"> close</button>
				</div>
			</form>
					
			</div>
		 </div>
		
		</div>
	</div>
	
	<script type="text/javascript">
	
	//const url='http://localhost:7777/subcat?id=1';
	$(document).ready(function(){
		
		//findSubCategory();
		var id=document.getElementById('itemCategory').value;
		var ops='';
		var body_content='';
			debugger;
			//$.get('http://103.192.67.20:8080/common.mobileapp.api-0.0.1/querydata/api?id=1&gsno=140756',function(data,status){
			$.get('subcat?id='+id+'',function(data,status){
		//	console.log(data+"  "+status);
		//	console.log(data[0].id)
		//	console.log(data.length)
			
			for(var v=0;v<data.length;v++){
				ops=ops+'<option>'+data[v].subCategory+'</option>'
			}
			//console.log("opt "+ops);
			document.getElementById("itemSubCategory").innerHTML=ops;
 	
 });	
				
		
		
	});
	function hello()
	{
		//findSubCategory();		
	
		 var id=document.getElementById('itemCategory').value;
		var ops='';

		console.log('val '+id)
			$.get('subcat?id='+id+'',function(data,status){
			//console.log(data+"  "+status);
			//console.log(data[0].id)
			//console.log(data.length)
			
			for(var v=0;v<data.length;v++){
				ops=ops+'<option>'+data[v].subCategory+'</option>'
			}
			console.log("opt "+ops);
			document.getElementById("itemSubCategory").innerHTML=ops;
			});
		

 }
		
		
	</script>
	

</body>
</html>