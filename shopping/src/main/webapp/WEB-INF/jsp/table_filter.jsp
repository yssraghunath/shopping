<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="resources/css/mystyle.css" rel="stylesheet">
</head>
<body>
<!--  table pagination  -->
	<c:set var="totalCount" scope="session" value="${list.size()}"></c:set>
	<c:set var="perPage" scope="session" value="${10}"></c:set>
	<c:set var="pageStart" value="${param.start}"></c:set>
	
	<c:if test="${empty pageStart or pageStart < 0 }">
		<c:set var="pageStart" value="0"></c:set>
	</c:if>
	<c:if test="${totalCount < pageStart}">
		<c:set var="pageStart" value="${pageStart - perPage}"></c:set>
	</c:if>

<!--  table header and search and prev and next button  -->	
	
	<div  class="row">
		<div class="col-sm-4">
				<h4> ${tableTitle}</h4>
		</div>
		<div class="col-sm-5">
			<input class="form-control form-control-sm" id="myInput" type="text" placeholder="Search..">
		</div>
		<div class="col-sm-3">
				
			<a href="?start=${pageStart - perPage}" class="btn btn-primary btn-sm" style="margin-right:10px;margin-bottom:10xp;">Prev</a>
				<span>		${pageStart} - ${pageStart + perPage} / ${totalCount}</span>
			<a href="?start=${pageStart + perPage}" class="btn btn-primary btn-sm" style="margin-left:10px;margin-bottom:10xp;" >Next</a>
		
		</div>
	</div>
	
	
	 
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
   var value = $(this).val().toLowerCase(); // find only chareter
    var value = $(this).val();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>



</body>
</html>