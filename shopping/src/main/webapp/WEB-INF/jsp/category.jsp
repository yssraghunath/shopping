<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>category</title>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css"  rel="stylesheet">


<script type="text/javascript">

function Validation(){
	
	
}

function autoFillFieldEdit(id,category){
	document.forms['updateform']['id'].value=id;
	document.forms['updateform']['category'].value=category;
	
	
}
</script>


</head>
<body class="container-fluid alert-success">
	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>


	<div class="row" style="margin-top:10px;">
		<div class="col-sm-6 offset-sm-3">
		<div class="card">
			<div class="card-header">
			<h4>Add New  Category</h4>
			</div>
			<div class="card-body">
			
			<form name="myForm1" action="addcategory" modelAttribute="categoryInfo"	method="POST" class="form form-inline col-sm-12 "
			 onsubmit="return Validation()">

			<div class="form-group col-sm-9">
				<label>Category <span class="req" style="color: Red">*</span></label> &nbsp; &nbsp;
				<input type="text" name="category"	placeholder="Enter category Name " class="form-control" required>
			</div>

			<div class="form-group col-sm-3">
				<input type="submit" value="submit"
					class="btn btn-success btn-block">
			</div>


		</form>
			
			</div>
		</div>
		</div>
		</div>


<c:set var="tableTitle"  value="Category Details"></c:set>
	<c:set var="list" scope="session" value="${catList}"></c:set>
	
	<div class="row" style="font-size:14px;">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
				 	 	<%@include file="table_filter.jsp" %>  
				
					</div>
					<div class="card-body">
						<table class="table table-style table-sm table-bordered table-striped" >
					
						<thead class="table-header-style">
						<tr>
							<th>Id</th>
							<th>Name</th>
						<th style="width:85px;">Action</th>
						</tr>
						</thead>
					
					
						<tbody id="myTable">
						<%-- begin="${pageStart}" end="${pageStart + perPage -1}" --%>
					<c:forEach  var="cat" items="${catList}"  varStatus="loop" > 
						<tr>
						
							<td>${cat.id}</td>
							 <td>${cat.category}</td>
							
							<td style="width:85px;">
						
								
							 <div class="row">
							 	<span class="col-sm-3"><i class='fa fa-edit' style='font-size: 16px;color:skyblue'  data-toggle="modal" data-placement="top" title="Edit" data-target="#editModal" onClick="autoFillFieldEdit('${cat.id}','${cat.category}')"></i></span>
							 <%-- 	<span class="col-sm-3"><i class='fa fa-plus' style='font-size: 16px;color:darkgreen' data-toggle="modal" data-placement="top" title="Purchase" data-target="#purchaseModal" onClick="autoFillFieldPurchase('${item.id}','${item.itemName}','${item.quantity}','${item.itemMRP}','${item.imageUrl}')"></i></span> --%>
							 	<span class="col-sm-3"> <a href="/categorydelete?id=${cat.id}"><i class='fa fa-minus' style='font-size: 16px;color:darkred' data-toggle="modal" data-placement="top" title="delete" data-target="#saleModal"></i></a></span>
							 </div>
								
							</td> 											
						</tr>
						</c:forEach>
						
						
						</tbody>
												
				</table>
						
				
					</div>
				
				</div>
			
			</div>
	
	</div>
	
	


<div class="row">
		<!-- Button to Opne The modal -->
		
		<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> open modal</button> -->
		
		<!-- the  modal -->
		<div class="modal" id="editModal" >
		<div class="modal-dialog col-sm-12">
			<div class="modal-content">
			<form class="form-inline" action="categoryupdate" method="POST" name="updateform" modelAttribute="category">
				<div class="modal-header col-sm-12">
				
					<h4 class="modal-title"> Category Update </h4>
					<button type="button" class="close" data-dismiss="modal"> &times;</button>
				</div>
				
				<div class="modal-body">
					<div class="row">
					 <div class="col-sm-12">
						<div class="form-group " hidden>
							<label>item Id</label>
							<input type="text" class="form-control" name="id" id="id" >
						</div>
					
					<div class="form-group">
							<label class="col-sm-3">Category </label>
							<input type="text" class="form-control col-sm-6" name="category" id="category"/>
					</div>
				</div>
					
					</div>
							
				
				</div>
				<div class="modal-footer col-sm-12">
				<input type="submit" class="btn btn-success" value="update"  />
					
				
						<button type="button" class="btn btn-danger" data-dismiss="modal"> close</button>
				</div>
			</form>
					
			</div>
		 </div>
		
		</div>
	</div>
	




</body>
</html>