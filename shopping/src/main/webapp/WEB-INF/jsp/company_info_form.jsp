<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"  rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css"  rel="stylesheet">

<title>company details</title>
</head>
<body class="container-fluid alert-success">
	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>
<c:set var="list"  scope="session"  value="${companyDetilsList}"></c:set>
<c:set var="size" value="${list.size()}"></c:set>
<c:set var="display" value="block"></c:set>
<c:if test="${size > 0 }">
		<c:set var="display" value="none"></c:set>
</c:if>
	
	
<form action="companyinfo" method="POST" modelAttribute="companyDetails" class="form form-inline col-sm-12 alert-success" style="padding:10px; display:${display};" >
	<div class="row" style="background-color:white;padding:10px;margin:10px;">
			
			<div class="row" style="padding:10px;">
			
			
			<p class="h4 col-sm-12" > Company Info </p>
			
								
			 
				<div class="form-group col-sm-4">
					<label> Company Name </label>
					<input type="text" class="form-control form-control-sm col-sm-12" name="companyName" id="companyName" placeholder="">
				</div>
				
				<div class="form-group col-sm-2">
					<label> CIN NO. </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  id="cinNO" name="cinNO" placeholder="">
				</div>
				
				<div class="form-group col-sm-2">
					<label> GST NO. </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  id="gstNo" name="gstNo" placeholder="">
				</div>
				<div class="form-group col-sm-2">
					<label> Pan NO. </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  id="panNO" name="panNo" placeholder="">
				</div>
				
				<div class="form-group col-sm-2">
					<label>Invoice NO. </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  id="invoiceNo" name="invoiceNo" placeholder="">
				</div>
			</div>	
			
			<div class="row col-sm-12" style="padding:10px;">
			
			
			<p class="col-sm-12 h4" >Company's Address</p>
			
								
			 
				<div class="form-group col-sm-6" >
					<label> Register Address </label>
					<input type="text" class="form-control form-control-sm col-sm-12" name="registerAddress" id="registerAddress" placeholder="">
				</div>
				
				<div class="form-group col-sm-6">
					<label> Corporate Address </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  name="corporateAddress" id="corporateAddress" placeholder="">
				</div>
				
				
			</div>
		
			<div class="row" style="padding:10px;">
			
			
				<p class="col-sm-12 h4"> Bank Details </p>				
			 
				<div class="form-group col-sm-3">
					<label> Bank's Name  </label>
					<input type="text" class="form-control form-control-sm col-sm-12" name="bankName" id="bankName" placeholder="">
				</div>
				
				<div class="form-group col-sm-3">
					<label> Account No </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  name="accountNo" id="accountNo" placeholder="">
				</div>
			
				<div class="form-group col-sm-3">
					<label> IFSC / NEFT Code </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  name="ifscCode" id="ifscCode" placeholder="">
				</div>
			
				<div class="form-group col-sm-3">
					<label> Bank's Address </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  name="bankAddress" id="bankAddress" placeholder="">
				</div>
				
				
			</div>	
			
				
			
				
				<div class="col-sm-12" style="text-align:center; padding-top:10px;">
					
					<input type="submit" class="btn btn-success" value="Add" style="margin-top:10px;" name="" placeholder="">
				</div>
				
			
			
			
	</div>
	</form>

	<c:forEach var="company" items="${companyDetilsList}">
	
		<form action="companyinfoupdate" method="POST" modelAttribute="companyDetails">
		<div class="row" style="background-color:#efe;padding:10px;margin:10px;">
			
			<div class="row" style="padding:10px;">
			
			
			<p class="h4 col-sm-10" > Company Info </p>
			
			
		
			 
				<div class="form-group col-sm-4">
					<label> Company Name </label>
					<input type="text" class="form-control form-control-sm col-sm-12" name="companyName" value="${company.companyName}" id="companyName" placeholder="">
				</div>
				
				<div class="form-group col-sm-2">
					<label> CIN NO. </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  id="cinNO" value="${company.cinNO}" name="cinNO" placeholder="">
				</div>
				
				<div class="form-group col-sm-2">
					<label> GST NO. </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  id="gstNo" value="${company.gstNo}" name="gstNo" placeholder="">
				</div>
				<div class="form-group col-sm-2">
					<label> Pan NO. </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  id="panNO" value="${company.panNo}" name="panNo" placeholder="">
				</div>
				
				<div class="form-group col-sm-2">
					<label>Invoice NO. </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  id="invoiceNo" value="${company.invoiceNo}" name="invoiceNo" placeholder="">
				</div>
			</div>	
			
			<div class="row col-sm-12" style="padding:10px;">
			
			
			<p class="col-sm-12 h4" >Company's Address</p>
			
								
			 
				<div class="form-group col-sm-6" >
					<label> Register Address </label>
					<input type="text" class="form-control form-control-sm col-sm-12" name="registerAddress" value="${company.registerAddress}" id="registerAddress" placeholder="">
				</div>
				
				<div class="form-group col-sm-6">
					<label> Corporate Address </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  name="corporateAddress" value="${company.corporateAddress}" id="corporateAddress" placeholder="">
				</div>
				
				
			</div>
		
			<div class="row" style="padding:10px;">
			
			
				<p class="col-sm-12 h4"> Bank Details </p>				
			 
				<div class="form-group col-sm-3">
					<label> Bank's Name  </label>
					<input type="text" class="form-control form-control-sm col-sm-12" name="bankName" value="${company.bank.bankName}"  id="bankName" placeholder="">
				</div>
				
				<div class="form-group col-sm-3">
					<label> Account No </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  name="accountNo" value="${company.bank.accountNo}" id="accountNo" placeholder="">
				</div>
			
				<div class="form-group col-sm-3">
					<label> IFSC / NEFT Code </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  name="ifscCode" value="${company.bank.ifscCode}" id="ifscCode" placeholder="">
				</div>
			
				<div class="form-group col-sm-3">
					<label> Bank's Address </label>
					<input type="text" class="form-control form-control-sm col-sm-12"  name="bankAddress" value="${company.bank.bankAddress}" id="bankAddress" placeholder="">
				</div>
				<div class="form-group col-sm-3 offset-sm-6">
					<input class="btn btn-success" type="submit" value="update"/>
				</div>
				
				
			</div>	
			
			
				
			
	</div>
	</form>
	</c:forEach>

</body>
</html>