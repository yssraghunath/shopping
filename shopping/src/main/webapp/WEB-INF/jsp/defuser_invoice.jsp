<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.shopping.cart.entity.UserEntity" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport"	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<title>Invoice ${orderList[0].orderId.orderFormate} / ${orderList[0].orderId.id}</title>
<style type="text/css">

table,th,td{
	border:solid 1px black;
}

</style>

</head>

<body class="container">
<input type="text" id="pageName" value="${pageName}" hidden>
    <%-- <div class="row">
    ${selfDetails}
    ${orderList}
      
      <input type="text" id="pageName" value="${pageName}" hidden>
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h2>${TITLE} </h2><h3 class="pull-right">Order Id :${orderList[0].orderId.orderFormate} / ${orderList[0].orderId.id}</h3>
    		</div>
    		<hr>
    		<div class="row">
    		
    			<div class="col-xs-6">
    				<address>
    				<strong>Sold By : </strong><br>
    					${selfDetails.companyName}<br>
    					 ${selfDetails.corporateAddress}<br>
    					 GST No. : ${selfDetails.gstNo}
    					
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    				
    					<strong>Order Date & Time </strong><br>
    					${orderList[0].orderId.orderDate}<br><br>
    				</address>  			
    				
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Billing Address : </strong><br>
    					${user.name} <br>
    					Address : ${user.address}<br>
    					Mobile No : ${user.mobile}<br>
    					Email Id : ${user.emailId}<br>
    				   Customer Id : ${user.userName}<br>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    			
    				<address>
        			<strong>Shipped To:</strong><br>
    					${orderList[0].orderId.name} <br>
    					Address : ${orderList[0].orderId.address}<br>
    					Mobile No : ${orderList[0].orderId.mobileNo}<br>
    					Email Id : ${orderList[0].orderId.emailId}<br>
    				    Customer Id : ${user.userName}<br>
    
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
     --%>
    <%-- <div class="row" style="width:12">
    	<div class="col-xs-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Order summary</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-sm table-condensed">
    					<!-- <table class="table table-style table-sm table-bordered"> -->
						<thead>
							<tr>
								<th>S.N.</th>
								<th>Item Name</th>
								<th>Item Description</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>Total Price</th>
								
							</tr>
						</thead>
						<tbody>
						<c:set var="totalItem" value="0"></c:set>
						<c:set var="totalAmount" value="0"></c:set>
							<c:forEach var ="order" items="${orderList}" varStatus="loop">
								<tr>
									<c:set var="totalItem" value="${totalItem + order.quantity}"></c:set>
									<c:set var="totalAmount" value="${totalAmount + order.totalPrice}"></c:set>
									
									<td>${loop.count}</td>
									<td>${order.itemCode.itemName}</td>
									<td>${order.itemCode.itemDescription}</td>
									<td>${order.quantity}</td>
									<td>${order.itemPrice}</td>
									<td>${order.totalPrice}</td>
								
								</tr>
							</c:forEach>
							<tr style="border:solid 2px black;background-color:#white;">
									<th> </th>
									<th colspan="2">Total Number Of Item</th>
									<th>${totalItem}</th>
									<th>Total Amount </th>
									<th colspan="2">${totalAmount}</th>
									
								
							</tr>
						</tbody>
					
					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

 --%>
  
    	<div class="row" style="text-align:center;">
			<h3> <u>Tax Invoice</u></h3>
		</div>
		<div class="row">
			 <h2>${TITLE} </h2><h3 class="pull-right">Order Id :${orderList[0].orderId.orderFormate} / ${orderList[0].orderId.id}</h3>
		</div>
		
		<div class="row">
			<div class="col-xs-6">
    				<address>
    				<strong>Sold By : </strong><br>
    					${selfDetails.companyName}<br>
    					 ${selfDetails.corporateAddress}<br>
    					 GST No. : ${selfDetails.gstNo}
    					
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    				
    					<strong>Order Date & Time </strong><br>
    					${orderList[0].orderId.orderDate}<br><br>
    				</address>  			
    				
    			</div>
			
			
		</div>
		<div class="row">
			<div class="col-xs-6">
    				<address>
    					<strong>Billing Address : </strong><br>
    					${orderList[0].userId.name} <br>
    					Address : ${orderList[0].userId.address}<br>
    					Mobile No : ${orderList[0].userId.mobile}<br>
    					Email Id : ${orderList[0].userId.emailId}<br>
    				    Customer Id : ${orderList[0].userId.userName}<br>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    			
    				<address>
        			<strong>Shipped To:</strong><br>
    					${orderList[0].orderId.name} <br>
    					Address : ${orderList[0].orderId.address}<br>
    					Mobile No : ${orderList[0].orderId.mobileNo}<br>
    					Email Id : ${orderList[0].orderId.emailId}<br>
    				    Customer Id : ${orderList[0].userId.userName}<br>
    
    				</address>
    			</div>
    	
			
			
		</div>
		
		
		<div class="row">
			<table class="table table-sm table-condensed">
    					<!-- <table class="table table-style table-sm table-bordered"> -->
						<thead>
							<tr>
								<th>S.N.</th>
								<th>Item Name</th>
								<th>Item Description</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>Total Price</th>
								
							</tr>
						</thead>
						<tbody>
						<c:set var="totalItem" value="0"></c:set>
						<c:set var="totalAmount" value="0"></c:set>
							<c:forEach var ="order" items="${orderList}" varStatus="loop">
								<tr>
									<c:set var="totalItem" value="${totalItem + order.quantity}"></c:set>
									<c:set var="totalAmount" value="${totalAmount + order.totalPrice}"></c:set>
									
									<td>${loop.count}</td>
									<td>${order.itemCode.itemName}</td>
									<td>${order.itemCode.itemDescription}</td>
									<td>${order.quantity}</td>
									<td>${order.itemPrice}</td>
									<td>${order.totalPrice}</td>
								
								</tr>
							</c:forEach>
							<tr style="border:solid 2px black;background-color:#white;">
									<th> </th>
									<th colspan="2">Total Number Of Item</th>
									<th>${totalItem}</th>
									<th>Total Amount </th>
									<th colspan="2">${totalAmount}</th>
									
								
							</tr>
						</tbody>
					
					</table>
	</div>
	<div class="row justify-content-center">
		
			<button  class="btn btn-default col-sm-1" onclick="myFunction()" id="print" style="display:block">Print Invoice</button>	
			<button class="btn btn-default col-sm-1" style="margin-left:10px;" onclick="goBack('${user.role.roleType}')" id="back" style="display:block">Back</button>	
		
	</div>
		
		
	
	 	
	<script type="text/javascript">
	
	document.getElementById("print").style.display = "block";
	document.getElementById("back").style.display = "block";
      function myFunction() {
    	  debugger;
    	   var page=document.getElementById("pageName").value;
    	   document.getElementById("print").style.display = "none";
   		   document.getElementById("back").style.display = "none";
   		
    		 window.print(); 
    		 
    	//	console.log("  ",window.origin+"/"+page);
    		
    	//location.assign(window.origin+"/"+page);
    	document.getElementById("print").style.display = "block";
     	document.getElementById("back").style.display = "block";
     		
	
      }
      
      function goBack(role){
    	  debugger;
    	  var page=document.getElementById("pageName").value;
    	  console.log(" print "+role)
    	  if(role=='SUPER ADMIN'){
    		  location.assign(window.origin+"/"+page);
    	  }else if(role=='USER'){
    		  location.assign(window.origin+"/product");
    	  }
    	  
      }
      
    </script>
    
	</body>
	</html>