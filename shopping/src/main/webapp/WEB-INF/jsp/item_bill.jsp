<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Invoice Bill</title>
<script type="text/javascript">

	function setValue(){
		//debugger;
		// ${item.itemName },${item.hnsCode},${item.cgst},${item.sgst},${item.price}
	var item=document.getElementById('itemName1').value;
	var array = item.split(","); 
	  //  document.write(array[0]);
	   
	  
	  	document.getElementById('hnsCode').value=array[1];
	   	document.getElementById('cgst').value=array[2];
	   	document.getElementById('sgst').value=array[3];
	   	document.getElementById('igst').value=Number(document.getElementById('cgst').value)+Number(document.getElementById('sgst').value);
	    document.getElementById('taxAblePrice').value=array[4];
	    document.getElementById('itemName').value=array[0];
	
	    calculatePrice();
	
	//console.log(itemValue.itemName);	
	}
	
	function calculatePrice(){
		
		
		var cgst =Number(document.getElementById('cgst').value);
	   	var sgst =Number(document.getElementById('sgst').value);
	   	document.getElementById('igst').value=(cgst+sgst);
	   	var totalTax=cgst+sgst;
	    var price = document.getElementById('taxAblePrice').value;
	    
	    var totalTaxAmount=(Number(price)/100)*totalTax;
	    
	    document.getElementById('totalTaxAmount').value=totalTaxAmount;
	    document.getElementById('totalprice').value=(Number(price)+Number(totalTaxAmount));
	    
	    
	
	}
	function addItem(){
		
		var name=document.forms["item_form"]["invoiceId"].value;
		var n=document.getElementById('itemName').value;
		console.log("  ",n);
		alert('Add SuccessFully');
		return true;;
	}
</script>


</head>

<body class="container-fluid alert-success">
	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>
		


		<div class="row" style="margin:10px;">
			<div class="col-md-6 offset-md-3">
				<form name="item_form" action="itemSave" method="POST" modelAttribute="item" onSubmit="return addItem()">
					<div class="card">
					<div class="card-header">
						<h4>Item Form</h4>
					</div>
					
						<div class="card-body">
						<div class="form-group row">
								<label for="item" class="col-sm-4 col-form-label col-form-label-sm">Invoice Id  <span class="req" style="color: Red">*</span></label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm" name="invoiceId" value="${invoiceId}" >
								</div>
							</div>
							 
							
							<div class="form-group row">
								<label for="item" class="col-sm-4 col-form-label col-form-label-sm">Item  <span class="req" style="color: Red">*</span></label>
								<div class="col-sm-6">
									<%-- <input type="text" class="form-control form-control-sm" value="${itemDetails}" name="itemName"> --%>
									<select class="form-control col-sm-12" name="itemName1" id="itemName1" onchange="setValue()" required>
										<option value="" disabled selected>Select your option</option>
										<c:forEach var="item" items="${itemDetails}">
										
										   <option value="${item.itemName },${item.hnsCode},${item.cgst},${item.sgst},${item.price}">${item.itemName}</option>
										</c:forEach>
									</select>
									<input type="text" class="form-control form-control-sm" name="itemName" id="itemName" hidden>
								</div>
							</div>

							<div class="form-group row">
								<label for="hsn_sac" class="col-sm-4 col-form-label col-form-label-sm">HSN/SAC
									code  <span class="req" style="color: Red">*</span></label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm" name="hnsCode" id="hnsCode" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="taxable_price" class="col-sm-4 col-form-label col-form-label-sm" >Taxable
									Price  <span class="req" style="color: Red">*</span></label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm" name="taxAblePrice"  id="taxAblePrice" onKeyUp="calculatePrice()" required>
								</div>
							</div>
							
							
							
							
							<div class="form-group row">
								<label for="cgst" class="col-sm-4 col-form-label col-form-label-sm">CGST<span class="req" style="color: Red">*</span></label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm" name="cgst" id="cgst" onKeyUp="calculatePrice()" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="sgst" class="col-sm-4 col-form-label col-form-label-sm">SGST<span class="req" style="color: Red">*</span></label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm" name="sgst" id="sgst" onKeyUp="calculatePrice()" required>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="igst" class="col-sm-4 col-form-label col-form-label-sm">IGST<span class="req" style="color: Red">*</span></label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm" name="igst" id="igst" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="tax_amount" class="col-sm-4 col-form-label col-form-label-sm">Total
									Tax Amount  <span class="req" style="color: Red">*</span></label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm" name="totalTaxAmount" id="totalTaxAmount" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="total_price" class="col-sm-4 col-form-label col-form-label-sm">Total
									Price  <span class="req" style="color: Red">*</span></label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm form-control form-control-sm-sm" name="totalprice" id="totalprice" required>
								</div>
							</div>
							<div style="text-align: center;">
								<button type="submit" class="btn btn-primary">Save</button>
							</div>
						</div>
					</div>
			</div>
			</form>
		</div>
	<a href="preview/?id=${invoiceId}"><button class="btn btn-primary">Preview</button></a>	
		<div class="row">
			<table class="table table-sm table-bordered table-striped" style="margin:20px;">
				<thead style="background-color:#a1fbc9;color:#051842">
					<tr>
						<th>
							Item Name
						</th>
						<th>
							HSN code
						</th>
						<th>
							Taxable
						</th>
						<th>
							SGST
						</th>
						<th>
							CGST
						</th>
						<th>
							IGST
						</th>
						<th>
							Total
						</th>
						
					</tr>
				</thead>
			
			<c:forEach var="item" items="${previewData}"> 
			<tbody style="background-color:white;">
			<tr >
					<td>${item.itemName}</td>
						<td>${item.hnsCode}</td>
						<td>${item.taxAblePrice}</td>
						<td>${item.sgst}</td>
						<td>${item.cgst}</td>
						<td>${item.igst}</td>
						<td>${item.totalprice }</td>
						</tr>
				</tbody>
			</c:forEach>
					
			
			
			</table>
		
		
		
		</div>
<script>
	
</script>


</body>
</html>