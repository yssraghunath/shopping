<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"	rel="stylesheet">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
<script src="webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<style type="text/css">
/*  .regbtn{
 	position: absolute;
 	 right: 5%;
 	
 } */

</style>

<title> ${TITLE} </title>
</head>
<body class="container-fluid alert-success">
<div class="row" style="margin-top:20%;">
    <div class="col-sm-4 offset-sm-4 col-xs-12">
    	<div class="card">
<div class="card-header">
		<div class="row">
	  	<span class="col-xs-8 h4" style="margin:10xp">Login Form</span>
	   <button class="col-xs-4 ml-auto pull-sm-right  btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal"> New Registration </button>
	
	  </div>
	
</div>
<div class="card-body">
<form name="myForm1" action="login" modelAttribute="user"	method="POST" class="form  alert-success"
				style="padding: 20px; background-color:white; onsubmit="return validate()">
	<div class="form-group">
					
					<input type="text" name="userName"	placeholder="Enter userid Name " class="form-control">
					<p id="u_sms" style="color:red;display:none" >Please Enter user name</p>
				</div>
				<div class="form-group">
					
					<input type="password" name="password" placeholder="Enter password. " class="form-control"	>
					<p id="pass_sms"  style="color:red;display:none">Please Enter Password</p>
				</div>
				
				<p  style="color:red;"> ${error_message}</p>

				<div class="form-group" style="padding-top: 25px;">
					<input type="submit" value="submit"
						class="btn btn-success btn-block">
								
					
				</div>

	</form>			
		
</div>
<div class="card-footer">
	<a href="forgetpassword">forget password</a>
</div> 

</div>
    </div>
</div>
	
<%-- 	<div class="row" style="margin:10px;margin-top:20%;">
	<div class="col-sm-4 offset-sm-4" style="text-algin:center;background-color:white;border:solid  0px;padding:10px;border-radius:5%;">
	  <div class="row">
	  	<span class="col-xs-8 h4" style="margin:10xp">Login Form</span>
	   <button class="col-xs-4 ml-auto pull-sm-right  btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal"> New Registration </button>
	
	  </div>
	   
    <form name="myForm1" action="login" modelAttribute="user"	method="POST" class="form  alert-success"
				style="padding: 20px; background-color:white; onsubmit="return validate()">
			
				<div class="form-group">
					
					<input type="text" name="userName"	placeholder="Enter userid Name " class="form-control">
					<p id="u_sms" style="color:red;display:none" >Please Enter user name</p>
				</div>
				<div class="form-group">
					
					<input type="password" name="password" placeholder="Enter password. " class="form-control"	>
					<p id="pass_sms"  style="color:red;display:none">Please Enter Password</p>
				</div>
				<p  style="color:red;"> ${error_message}</p>

				<div class="form-group" style="padding-top: 25px;">
					<input type="submit" value="submit"
						class="btn btn-success btn-block">
								
					
				</div>


			</form>
	
  </div>
</div>
 --%>	  
		
	</div>
	
	<div class="row">
		<!-- Button to Opne The modal -->

		<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> open modal</button> -->

		<!-- the  modal -->
		<div class="modal" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<form name="myForm1" action="registration" modelAttribute="clintInfo"
			method="POST" class="form"
		 onsubmit="return Validation()">
						<div class="modal-header">

							<h4 class="modal-title">Registration Form </h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>

						<div class="modal-body">
							
							<div class="form-group">
								<label>Customer Name</label> 
								<input type="text"	class="form-control" name="name" />
							</div>
							
							<div class="form-group">
								<label>Email Id</label>
								 <input type="text" class="form-control" name="emailId" />
							</div>
							<div class="form-group">
								<label>Contact No.</label> <input type="text"
									class="form-control" name="mobile" />
							</div>
							
							<div class="form-group">
								<label>Address</label> 
								<textarea rows="3" cols="50" type="text" class="form-control"
									name="address" ></textarea>
									
							</div>


						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-success" value="Register" />


							<button type="button" class="btn btn-danger" data-dismiss="modal">
								close</button>
						</div>
					</form>

				</div>
			</div>

		</div>
	</div>

	<script type="text/javascript">


	function validate(){
		debugger;
	var validUser=false;
	var validPass=false;

	var name=document.forms["loginForm"]["userName"].value;
	var pass=document.forms["loginForm"]["password"].value;
	document.getElementById("u_sms").style.display = "block";
	if(name == ""){
		document.getElementById("u_sms").style.display = "block";
	//	alert("hello ");
		validUser=false;
	}else{
		document.getElementById("u_sms").style.display = "none";
		validUser=true;
	//	alert("valid user");
		
	}
	
	
	 if((pass =="") || (pass==null)){
		document.getElementById("pass_sms").style.display = "block";
		//alert("hello ");
		validPass=false;
	}else{
		document.getElementById("pass_sms").style.display = "none";
		validPass=true;
		//alert("valid pass");
		
	}
	 
	
	
	 if(validUser==true && validPass==true){
		return true;
	}else{
		return false;
	} 
	}
</script>	
	
	
	
</body>
</html>