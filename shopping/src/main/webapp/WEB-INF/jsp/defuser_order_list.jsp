<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>order details</title>
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"  rel="stylesheet">

</head>
<body class="container-fluid alert-success">
<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>

		
	

	<c:set var="tableTitle"  value="Order Details"></c:set>
	<c:set var="list" scope="session" value="${orderNumberList}"></c:set>
	<div class="row">
		<div class="col-sm-12">
			<div class="card ">
				<div class="card-header">
					<%@include file="table_filter.jsp" %>
				</div>
				<div class="card-body">
				<table   class="table table-style table-sm table-bordered table-striped display nowrap"  id="example">
			<thead class="table-header-style">
			<tr>
				<th>S.N. </th>
				<th> Order No. </th>
				<th>User Name</th>
				<th> Order Date </th>
				<th>Action</th>
				</tr>
			</thead>
			<tbody id="myTable">
				
				<c:forEach var="order" items="${list}" begin="${pageStart}" end="${pageStart + perPage -1}" varStatus="loop">
				<tr>
					<td>${loop.count}</td>
				 	<td>${order.id}</td>
					<td>${order.userName}</td>
					<td>${order.orderDate }</td>
				<%--  	<td><button class="btn btn-sm btn-block">${order.orderStatus }</button></td> --%>
					<td>
					 <a href="customerorder?id=${order.id}"> <button class="btn btn-sm btn-success">Show Details</button></a>
					 
					 </td>
					
					
				</tr>
				
				</c:forEach>
				
			</tbody>
			
		</table>
	
				</div>
			</div>
		
		</div>
	</div>

	
			

</body>
</html>