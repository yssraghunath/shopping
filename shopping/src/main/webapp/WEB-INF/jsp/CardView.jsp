<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CardView</title>

<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"	rel="stylesheet">
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
<script src="resources/js/popper.min.js"></script>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

</head>
<body>
	<div class="container-fluid">
		<div class="row">
		
			<div class="col-sm-2">
				<jsp:include page="ItemForm.jsp"></jsp:include>
			</div>
			<div class="col-sm-10">
					<!-- Button to Open the Modal -->
			<button type="button" class="btn btn-sm btn-success" data-toggle="modal"
				data-target="#myModal">Add Card</button>
				
				<table class="table">
					<thead>
						<tr>
							<th>Firstname</th>
							<th>Lastname</th>
							<th>Email</th>
							<th>Email</th>
							<th>Email</th>
							<th>Email</th>
							<th>Email</th>
							<th>Email</th>
							<th>Email</th>
						</tr>
					</thead>
					<tbody>


						<c:forEach items="${listcard}" var="cards">
							<tr>
								<td>${cards.itemname}</td>
								<td>${cards.serialno}</td>
								<td>${cards.purchasedate}</td>
								<td>${cards.modeltype}</td>
								<td>${cards.warranty}</td>
								<td>${cards.issuedto}</td>
								<td>${cards.installedby}</td>
								<td>${cards.returned}</td>
								<td>${cards.price}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
		
			<!-- The Modal -->
			<div class="modal fade" id="myModal">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">

						<!-- Modal Header -->
						<div class="modal-header">
							<h4 class="modal-title">Modal Heading</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>

						<!-- Modal body -->
						<div class="modal-body">
							<div class="card text-white bg-info">
								<div class="card-body">
									<h4 class="card-title">Card title</h4>
									<form:form class="form-horizontal" action="addcarddetails"
										method="post" modelAttribute="card">

										<div class="form-group row">
											<label for="itemname" class="col-sm-2 col-form-label">itemname</label>
											<div class="col-sm-5">
												<input type="text" name="itemname"
													class="form-control form-control-sm" id="itemname"
													placeholder="itemname">
											</div>
										</div>
										<div class="form-group row">
											<label for="inputEmail3" class="col-sm-2 col-form-label">serialno</label>
											<div class="col-sm-5">
												<input type="text" name="serialno"
													class="form-control form-control-sm" id="inputEmail3"
													placeholder="serialno">
											</div>
										</div>
										<div class="form-group row">
											<label for="inputEmail3" class="col-sm-2 col-form-label">purchasedate</label>
											<div class="col-sm-5">
												<input type="text" name="purchasedate"
													class="form-control form-control-sm" id="inputEmail3"
													placeholder="purchasedate">
											</div>
										</div>
										<div class="form-group row">
											<label for="inputEmail3" class="col-sm-2 col-form-label">modeltype</label>
											<div class="col-sm-5">
												<input type="text" name="modeltype"
													class="form-control form-control-sm" id="inputEmail3"
													placeholder="modeltype">
											</div>
										</div>
										<div class="form-group row">
											<label for="inputEmail3" class="col-sm-2 col-form-label">warranty</label>
											<div class="col-sm-5">
												<input type="text" name="warranty"
													class="form-control form-control-sm" id="inputEmail3"
													placeholder="warranty">
											</div>
										</div>
										<div class="form-group row">
											<label for="inputEmail3" class="col-sm-2 col-form-label">issuedto</label>
											<div class="col-sm-5">
												<input type="text" name="issuedto"
													class="form-control form-control-sm" id="inputEmail3"
													placeholder="issuedto">
											</div>
										</div>
										<div class="form-group row">
											<label for="inputEmail3" class="col-sm-2 col-form-label">installedby</label>
											<div class="col-sm-5">
												<input type="text" name="installedby"
													class="form-control form-control-sm" id="inputEmail3"
													placeholder="installedby">
											</div>
										</div>
										<div class="form-group row">
											<label for="inputEmail3" class="col-sm-2 col-form-label">returned</label>
											<div class="col-sm-5">
												<input type="text" name="returned"
													class="form-control form-control-sm" id="inputEmail3"
													placeholder="returned">
											</div>
										</div>
										<div class="form-group row">
											<label for="inputEmail3" class="col-sm-2 col-form-label">price</label>
											<div class="col-sm-5">
												<input type="text" name="price"
													class="form-control form-control-sm" id="inputEmail3"
													placeholder="price">
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-10 offset-sm-2">
												<button type="submit" class="btn btn-primary">Add</button>
												<button type="reset" class="btn btn-danger">Reset</button>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>

						<!-- Modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Close</button>
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>
	</div>


</body>
</html>