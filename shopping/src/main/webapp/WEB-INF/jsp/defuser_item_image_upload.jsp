<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>file upload</title>
 <link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"  rel="stylesheet">
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>

</head>
<body class="container alert-success">

<div class="row h1  justify-content-center" style="text-align: center;" >
		<%@include file="headerpage.html" %>
	</div>
<hr>
	<div class="row justify-content-center" style="margin-top:20%;padding:10px;border:solid 10px white">
		<form  name="uploadForm" class="form-inline" method="post" action="uploaditemimage" onsubmit="return uploadFile();" enctype="multipart/form-data">
			<div class="form-group">
			<input type="text" name="itemId" value="${itemCode}" hidden>
			<label style="margin-right:20px;">Upload Item Image </label>
				<input type="file" class="form-control form-control-sm" id="uploadFile" name="file">
				
			</div>
			
			<input  class="btn btn-sm btn-success"   style="margin-left:10px;" type="submit" value="Upload" required>
			<!-- <img alt="help" src="resources/images/imp_img/question.jpg" style="width:30px;height:30px;margin-left:50px;" > -->
			<span class="h4" style="margin-left:20px;padding-left:10px;padding-right:10px;background-color:red; color:white;border-radius:50%;"><abbr title="image should be .jpg or .png"> ? </abbr>  </span>
			<p> ${message}</p>
		</form>
		
				
	</div>
	<div class="row jsutify-content-center ">
		<p style="color:red"> image size should be less then 1048576 bytes</p>
	</div>
	<div class="row justify-content-center">
	<a href="defuseritem"><button class="btn btn-sm btn-danger">Skip </button> </a>
	</div>
</body>
</html>