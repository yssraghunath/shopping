<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
 <link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"  rel="stylesheet">
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
 <link rel='stylesheet'	href='https://use.fontawesome.com/releases/v5.4.2/css/all.css' integrity='sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns'
	crossorigin='anonymous'>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<title>user profile</title>
<!-- <script type="text/javascript">
	function myFunction(){
		document.getElementById("role").value="user";
	}
</script> -->
</head>
<body onload="myFunction()" class="container-fluid alert-success">
	
	<div>
		<%@include file="check_session.jsp" %>
	</div>
	
	
		<%-- <div class="col-sm-12">
				
				<c:set var="list" scope="session" value="${list}"></c:set>
				<c:set var="totalCount" scope="session" value="${list.size()}"></c:set>
				<c:set var="perPage" scope="session" value="${15}"></c:set>
				<c:set var="pageStart" value="${param.start}"></c:set>
				<c:if test="${empty pageStart or pageStart < 0 }">
					<c:set var="pageStart" value="0"></c:set>
				</c:if>
				<c:if test="${totalCount < pageStart}">
					<c:set var="pageStart" value="${pageStart - perPage}"></c:set>
				</c:if>
				
				
			<div class="row" style="color:white;margin-top:10px; padding:10px; background-color:green">
				<div class="col-sm-4">
					<h4>User Details</h4>
				</div>
				<div class="col-sm-5">
			<input class="form-control form-control-sm" id="myInput" type="text" placeholder="Search..">
		</div>
		<div class="col-sm-3">
				
			<a href="?start=${pageStart - perPage}" class="btn btn-primary btn-sm" style="margin-right:10px;margin-bottom:10xp;">Prev</a>
				<span>		${pageStart} - ${pageStart + perPage} / ${totalCount}</span>
			<a href="?start=${pageStart + perPage}" class="btn btn-primary btn-sm" style="margin-left:10px;margin-bottom:10xp;" >Next</a>
		
		</div>
			</div>
			<table class="table   table-style   table-sm  table-hover table-bordered table-striped" style="background-color:white;border:1px black;">
			<thead>
			
				<tr><th>Id</th>
					<th>Name</th>
					<th>User Name</th>
					<th>Mobile</th>
					<th> Address </th>
					<th>Role</th>
					<th width="120px;">Action</th>
				</tr>
				</thead>
				<tbody id="myTable">
					<c:forEach var="user" items="${list}" begin="${pageStart}" end="${pageStart + perPage -1}" varStatus="loop">
						<tr>
							<td>${user.id}</td>
							<td>${user.name}</td>
							<td>${user.userName}</td>
							<td>${user.mobile}</td>
							<td>${user.address}</td>
							<td >
						
							<select name="role"  class="form-control form-control-sm" style="width:150px;">
								<c:forEach var="role" items="${roles}">
									<option value="${role.id}"  ${role.id==user.role.id ?'selected':'' }>${role.roleType}</option>
								</c:forEach>
								
								
							</select>
							</td>
							<td width="120px;">
							
							<span><i onClick='editClientDetails(${client.id},"${client.gstNo}","${client.companyName}","${client.address}","${client.email}","${client.contactNo}")'
								class='fas fa-edit' data-toggle="modal" data-target="#myModal"
								style='font-size: 16px; color: green;'></i></span>
						   <span><a	href="deleteupdate/${client.id}" onClick=" return deleteConfirmation()">
								<i	class='fa fa-trash' style='font-size: 16px; color: red;'></i></a></span>
							
							</td>
							
						</tr>
					</c:forEach>
				
				</tbody>
			</table>
		</div>
	
	</div>
	 --%>
	 <c:set var="tableTitle"  value="User Details"></c:set>

	<c:set var="list" scope="session" value="${list}"></c:set>
	 
	<div class="card">
		<div class="card-header">
			<%-- <div class="row" style="color:white;margin-top:10px; padding:10px; background-color:green">
				<div class="col-sm-4">
					<h4>User Details</h4>
				</div>
				<div class="col-sm-5">
			<input class="form-control form-control-sm" id="myInput" type="text" placeholder="Search..">
		</div>
		<div class="col-sm-3">
				
			<a href="?start=${pageStart - perPage}" class="btn btn-primary btn-sm" style="margin-right:10px;margin-bottom:10xp;">Prev</a>
				<span>		${pageStart} - ${pageStart + perPage} / ${totalCount}</span>
			<a href="?start=${pageStart + perPage}" class="btn btn-primary btn-sm" style="margin-left:10px;margin-bottom:10xp;" >Next</a>
		
		</div>
			</div> --%>
				<%@include file="table_filter.jsp" %>
			
		</div>
		
		<div class="card-body">
		<table class="table   table-style table-sm  table-hover table-bordered table-striped" style="background-color:white;border:1px black;">
			<thead>
			
				<tr><th>Id</th>
					<th>Name</th>
					<th>User Name</th>
					<th>Mobile</th>
					<th>Email</th>
					<th> Address </th>
					<th>Role</th>
					<!-- <th width="120px;">Action</th> -->
				</tr>
				</thead>
				<tbody id="myTable">
					<c:forEach var="user" items="${list}" begin="${pageStart}" end="${pageStart + perPage -1}" varStatus="loop">
						<tr>
							<td>${user.id}</td>
							<td>${user.name}</td>
							<td>${user.userName}</td>
							<td>${user.mobile}</td>
							<td>${user.emailId}</td>
							<td>${user.address}</td>
							<td >
						
							<select name="role" onchange="location = this.value;"  class="form-control form-control-sm" style="width:150px;">
								<c:forEach var="role" items="${roles}">
									<option value="/updateuser?ud=${user.id}&rd=${role.id}"  ${role.id==user.role.id ?'selected':'' }>${role.roleType}</option>
								</c:forEach>
								
								
							</select>
							</td>
							<%-- <td width="120px;">
							
							
							action
							<span><i onClick='editClientDetails(${user.id},"${user.name}","${user.address}","${user.emailId}","${user.mobile}")'
								class='fas fa-edit' data-toggle="modal" data-target="#myModal"
								style='font-size: 16px; color: green;'></i></span>
						   <span><a	href="deleteupdate/${user.id}" onClick=" return deleteConfirmation()">
								<i	class='fa fa-trash' style='font-size: 16px; color: red;'></i></a></span>
							
							</td> --%>
							
						</tr>
					</c:forEach>
				
				</tbody>
			</table>
		</div>
	
	</div>
	
	
	
	
	
	<div class="row">
		<!-- Button to Opne The modal -->

		<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> open modal</button> -->

		<!-- the  modal -->
		<div class="modal" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<form action="clientUpdate" method="POST" name="myForm"
						modelAttribute="clientInfo">
						<div class="modal-header">

							<h4 class="modal-title">User Details</h4>
							<button type="button" class="close" data-dismiss="modal">
								&times;</button>
						</div>

						<div class="modal-body">
							<div class="form-group" hidden>
								<label> ID </label> <input type="text" class="form-control"
									name="id">
							</div>

							<div class="form-group">
								<label> Name </label> <input type="text"
									class="form-control" name="name" />
							</div>
							<div class="form-group">
								<label>Address</label> <input type="text" class="form-control"
									name="address" />
							</div>
							<div class="form-group">
								<label>Email Id</label> <input type="text" class="form-control"
									name="emailId" />
							</div>
							<div class="form-group">
								<label>Contact No.</label> <input type="text"
									class="form-control" name="mobile" />
							</div>



						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-success" value="update" />


							<button type="button" class="btn btn-danger" data-dismiss="modal">
								close</button>
						</div>
					</form>

				</div>
			</div>

		</div>
	</div>
	
	
	
	

<script type="text/javascript">
$(document).ready(function(){
	  $("#myInput").on("keyup", function() {
		  debugger;
	    var value = $(this).val().toLowerCase(); // find only chareter
	    var value = $(this).val();
	    $("#myTable tr").filter(function() {
	      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	    });
	  });
	});
	
	
	
function editClientDetails(id,name,address,emailId,mobile){
	debugger;
	
	//document.getElementByName("clientName").value=clientName;
	document.forms['myForm']['id'].value = id;
	document.forms['myForm']['name'].value = name;
	document.forms['myForm']['address'].value = address;
	document.forms['myForm']['emailId'].value = emailId;
	document.forms['myForm']['mobile'].value = mobile;
	
	
}

function deleteConfirmation(){
	var txt;
	if(confirm("Do You Sure want to delete ?")){
		txt="You pressed OK !"
		return true;
	}else{
		txt="You Pressed Cancel !";
		return false;
	}
}
</script>
</body>
</html>