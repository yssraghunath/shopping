<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.css"  rel="stylesheet">
<script>
	function calculatePrice(){
		
		var i_quantity=document.getElementById("quantity").value;
		var i_price=document.getElementById("price").value;
		var i_CGST=document.getElementById("sgst").value;
		var i_SGST=document.getElementById("cgst").value;
		var CGST=(i_price/100)*i_CGST;
		var SGST=(i_price/100)*i_SGST;
		CGST=CGST*i_quantity;
		SGST=SGST*i_quantity;
		if(quantity !=0){
			
			var total_price=i_quantity*i_price+(SGST+CGST);
			document.getElementById("total").value=total_price;
		}
	}
	
	function setAutoValue(){
		debugger;
		var details=document.getElementById('itemDetails').value;
		//var array = item.split(","); 
		var array = details.split(",");
		console.log(" ",array[0]);
		console.log(" ",array[1]);
		console.log(" ",array[2]);
		console.log(" ",array[3]);
		document.getElementById('itemName').value=array[0];
		document.getElementById('hnsCode').value=array[1];
		document.getElementById('cgst').value=array[2];
		document.getElementById('sgst').value=array[3];
		document.getElementById('igst').value=Number(cgst)+Number(sgst);
		
	}
	</script>

<title>purchage details</title>
</head>
<body class="container-fluid alert-success">
	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>
		
		


		
	<div class="row" style="background-color:white;padding:10px;margin:10px;">
			
				<p class="col-sm-12 h4" >Item Purchase Form</p>				
				<form action="savepurchase" method="POST" modelAttribute="purchaseInfo" class="form form-inline col-sm-12 alert-success" style="padding:10px;" > 
				<div class="form-group col-sm-2" >
					<label>Item Name</label>
					<input type="text" class="form-control col-sm-12" name="itemName" id="itemName" placeholder="" hidden>
		 			
		<select class="form-control col-sm-12" name="itemDetails" id="itemDetails" onchange="setAutoValue()">
					<option value="0">Choose Item</option>
					<c:forEach var="item" items="${itemList}">
						<option value="${item.itemName},${item.hnsCode},${item.cgst},${item.sgst}">${item.itemName}</option>
					</c:forEach>
						
						
		</select>
					
		
				</div>
				<!-- <div class="form-group col-sm-2">
					<label> Company Name </label>
					<input type="text" class="form-control col-sm-12" name="companyName" id="companyName" placeholder="">
				</div> -->
				
				<div class="form-group col-sm-2">
					<label> HSN/SAC </label>
					<input type="text" class="form-control col-sm-12" name="hnsCode" id="hnsCode" placeholder="">
				</div>
				
				
				
				<div class="form-group col-sm-1">
					<label> Quantity </label>
					<input type="text" class="form-control col-sm-12" onKeyUp="calculatePrice()"  value="0" id="quantity" name="quantity" placeholder="">
				</div>
				<div class="form-group col-sm-2">
					<label> Price (RS.)</label>
					<input type="text" class="form-control" value="0.0" onKeyUp="calculatePrice()" id="price" name="price" placeholder="">
				</div>
				<div class="form-group col-sm-1">
					<label>SGST (%) </label>
					<input type="text" class="form-control col-sm-12" value="0" name="sgst" onKeyUp="calculatePrice()" id="sgst"  placeholder="">
				</div>
				<div class="form-group col-sm-1">
					<label>CGST (%) </label>
					<input type="text" class="form-control col-sm-12" value="0" name="cgst" onKeyUp="calculatePrice()" id="cgst"  placeholder="">
				</div>
					<div class="form-group col-sm-2">
					<label>Total</label>
					<input type="text" class="form-control" name="total" id="total" placeholder="" >
				</div>
			
				
				<div class="form-group col-sm-1" style="padding-top:10px;">
					
					<input type="submit" class="btn btn-success btn-block" value="Add" style="margin-top:10px;" name="" placeholder="">
				</div>
				
			</form>
			
			
	</div>
	
	
	
	<c:set var="tableTitle"  value="Purchase Details"></c:set> 
	<c:set var="list" scope="session" value="${purchaseInfoList}"></c:set>
	
	
	
	<div class="row" >
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header">
				<%@include file="table_filter.jsp" %>			
			</div>
			<div class="card-body">
				<table class="table table-style  table-sm table-bordered table-striped">
					
						<thead class="table-header-style">
						<tr>
							<th>Id</th>
							<th>Name</th>
							<!-- <th>Comapany</th> -->
							<th>HSN/SAC Code </th>
							<th>Quantity </th>
							<th>Price</th>
							<th>CGST</th>
							<th>SGST</th>
							<th>Total</th>
							<th>Date</th>
					</tr>
						</thead>
					
					
						<tbody id="myTable">
						<c:forEach  var="purchaseInfo" items="${list}"  begin="${startPage}" end="${startPage + perPage -1}"  varStatus="loop" >
						<tr  >
							<td>${purchaseInfo.id}</td>
							<td>${purchaseInfo.itemName}</td>
							<%-- <td>${purchaseInfo.companyName}</td> --%>
							<td>${purchaseInfo.hnsCode}</td>
							<td>${purchaseInfo.quantity}</td>
							<td>${purchaseInfo.price}</td>
							<td>${purchaseInfo.cgst}</td>
							<td>${purchaseInfo.sgst}</td>
							<td>${purchaseInfo.total}</td>
							<td>${purchaseInfo.entryDate}</td>
						</tr>
						</c:forEach>
						</tbody>
					
					
				</table>
				
			
			</div>
		
		</div>
	
	</div>
					</div>
	
	

</body>
</html>