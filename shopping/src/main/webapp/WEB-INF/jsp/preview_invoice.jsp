<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
<title>preview invoice</title>
<script type="text/javascript">
function createPDF(){
	var contents="<h4> Tax Invoice</h4>";
	var doc = new jsPDF()
	doc.text(contents, 10, 10)
	doc.save('a4.pdf')
	
}


</script>
</head>
<body>
	<div class="header">
	
	<h4>Preview</h4>
	</div>
	<div class="body">
		<p> ${previewData} </p>
		<p> ${selfDetails} </p>
	
	</div>
	<div class="footer">
		<button class="btn btn-primary" onClick="createPDF()"> GENERATE PDF </button>
	</div>
	
	


</body>
</html>