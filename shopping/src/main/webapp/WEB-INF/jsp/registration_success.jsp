<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.css" rel="stylesheet">
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<style type="text/css">
 

</style>

<title>registration success</title>
</head>
<body class="container-fluid alert-success">
	<div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html"%>
	</div>
	<hr>
	<div class="row" style="margin-top:20%;color:black;">
		<div class="col-sm-6 offset-sm-3">
			<h4 style="color:red;">Congratulations ! You are registered successfully. </h4>
			<mark>your username : <b>${user.userName}</b> <br> and 
			password : <b>${user.password} </b></mark>
			
			<a href="signin"><button class="btn btn-danger btn-sm"> Go Back </button></a>
			
		<form action="login" modelAttribute="user" method="POST">
		<input type="text" name="userName" value="${user.userName}"  hidden>
		<input type="password" name="password" value="${user.password}" hidden>
		Click here for 
		<input type="submit" class="btn btn-success btn-sm" value="login"/>
	</form>
		</div>
		
	</div>
	
	


	
	
</body>
</html>