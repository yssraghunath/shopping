<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"  rel="stylesheet">
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<title>Qutation</title>

<script type="text/javascript">
      function myFunction() {
    		document.getElementById("print").style.display = "none";
    window.print();
}
    </script>

</head>
<body>
<div class="container">

        <div class="row" style="border: 1px solid black;">
            <div class="col-sm-6">
                Name:Address
                <div class="row" style="border-top: 1px solid black;">
                    <div class="col-sm-12">
                        CRO
                        <div class="row">
                            <div class="col-sm-12">
                                GREF Record Pune.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6" style="border-left: 1px solid black;">
                Dated:
                <div class="row" style="border-top: 1px solid black;">
                    <div class="col-sm-12">
                        Order in Favour of :- YSS SoftTech (P) Ltd
                        <div class="row">
                            <div class="col-sm-12">
                                Office:- D- 25, Sector 7. Noida. U.P. 201301..
                                <div class="row">
                                    <div class="col-sm-12">
                                        Tel:- +91-120-4549777, Fax:- +91-120-2511322.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<br>


 <div class="row">

            <div class="table-responsive">
                <table class="table table-sm table-bordered table-striped" style="margin-bottom:0px;">
<thead>
                    <tr>
                        <th width="300px;" style="text-align: center;">Ref.No</th>
                        <th style="text-align: center;">Period</th>
                        <th style="text-align: center;">Duration</th>
                        <th width="600px;" style="text-align: center;">Term</th>
                    </tr>
</thead>
<tbody>
                    <tr>
                        <td style="text-align: center">AMC/IVRS/01</td>
                        <td style="text-align: center;">AMC</td>
                        <td style="text-align: center">1 Year</td>
                        <td style="text-align: center">As per Contract</td>
                    </tr>
                    </tbody>
                    <thead>
                    <tr>
                        <td>Support Type:- Std. Day Support.</td>
                        <td colspan="2" style="text-align: center;">Monday to Friday 10:30 am to 6pm</td>
                        <td style="text-align: center;">Amount(INR)</td>
                    </tr>
                    </thead>
                </table>
                <table class="table table-sm table-bordered table-striped" style="margin-top:0px;margin-bottom:0px;">
                   <thead>
                    <tr>
                        <th width="539px;" style="text-align: center;">Description</th>
                        <th width="150px;">Order Value</th>
                        <th width="150px;">AMC Rate</th>
                        <th>AMC price</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Annual Maintenance Contract for GREF IVRS<br>(Comprehensive)</td>
                        <td>1,60,000.00</td>
                        <td>20%</td>
                        <td>32000.00</td>
                    </tr>
                    </tbody>
                    <tbody>
                    <tr>
                        <td>GST</td>
                        <td colspan="2">@18%</td>
                        <td>5670.00</td>
                    </tr>
                    </tbody>
                    <tbody>
                    <tr>
                        <td>Price of Comprehensive AMC of IVRS</td>
                        <td colspan="2">Total</td>
                        <td>32.670.00</td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-sm table-bordered table-striped" style="margin-top:0px;">
                   <tbody>
                    <tr>
                        <td>
                            In word:-Thirty Two Thousnad Six Hundread Sevety Rupees Only.
                        </td>
                    </tr>
                    </tbody>
                    <tbody>
                    <tr>
                        <td>100% Advance payment with Purchase order is mandatory for effective Service Level
                            Contract/AMC.</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="row">
           <p>Note:-
        <br>
        Support type
       <br>
                Day Support (Remote/online):- Monday to Friday, 10.00 am to 6.00 pm.
                <br>
                4 Critical Support call for holidays, Saturdays/Sundays.
                <br>
                10 Hours onsite support for software & Hardware in a year.
                 <br>   
                Replacement of Hardware is subject to Warranty Covered.</p>
        </div>
        <div class="row">
            <p>OUR COMMERCIAL TERMS AND CONDITIONS:-
                <br>
                     1) Purchase Order In favor of YSS SoftTech Pvt. Ltd.
                                                   D--25, Sector 7, NOIDA. UP. 201301.
                                                   <br>
                     2) PAYMENT: 100% Advance with Purchase Order.<br>
                     3) Support & Maintenance & Warranty:- As per SLA & AMC document.
            </p>
        </div>
          <button onclick="myFunction()" id="print" style="display:block">Print this page</button>
    </div>
</body>

</html>