<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">





<title>invoice details </title>
</head>
<body class="container-fluid alert-success">
	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>

		
	
	
	<c:set var="tableTitle"  value="Invoice Details"></c:set>
	<c:set var="list" scope="session" value="${invoiceDetailsList}"></c:set>
	<%@include file="table_filter.jsp" %>
	
	<%-- <c:set var="totalCount" scope="session" value="${list.size()}"></c:set>
	<c:set var="perPage" scope="session" value="10"></c:set>
	<c:set var="pageStart" value="${param.start}"></c:set>
	
	<c:if test="${empty pageStart or pageStart < 0 }">
		<c:set var="pageStart" value="0"></c:set>
	</c:if>
	<c:if test="${totalCount < pageStart}">
		<c:set var="pageStart" value="${pageStart - perPage}"></c:set>
	</c:if>
	
	<div class="row" style="background-color:#6da071;padding:10px;margin:10px;margin-bottom:0px;">
		<div class="col-sm-4" style="color:white">
				<h4>List of All Invoice</h4>
		</div>
		<div class="col-sm-5">
			<input class="form-control" id="myInput" type="text" placeholder="Search..">
		</div>
		<div class="col-sm-3">
				
			<a href="?start=${startPage - perPage}" class="btn btn-primary btn-sm" style="margin-right:10px;margin-bottom:10xp;">Prev</a>
				<span style="color:white">		${pageStart} - ${pageStart + perPage} / ${totalCount}</span>
			<a href="?start=${startPage + perPage}" class="btn btn-primary btn-sm" style="margin-left:10px;margin-bottom:10xp;" >Next</a>
		
		</div>
	</div> --%>
	<div class="row table-cover">
		<table   class="table table-style table-sm table-bordered table-striped display nowrap"  id="example">
			<thead class="table-header-style">
			<tr>
				<th> Invoice Id </th>
				<th> Buyer </th>
				<th> Date </th>
				<th> AMC FROM </th>
				<th> AMC TO	</th>
				<th> Status </th>
				</tr>
			</thead>
			<tbody id="myTable">
				<c:forEach var="invoice" items="${list}" begin="${pageStart}" end="${pageStart + perPage -1}"  varStatus="loop" >
				<tr>
					<td>${invoice.id}</td>
					<td>${invoice.buyerDetails}</td>
					<td>${invoice.invoiceDate}</td>
					<td>${invoice.amcDateFrom}</td>
					<td>${invoice.amcDateTo}</td>
					<td>
					 <a href="preview/?id=${invoice.id}"> <button class="btn btn-sm btn-success">Show Details</button></a>
					 
					 </td>
					
					
				</tr>
				
				</c:forEach>
				
			</tbody>
			
		</table>
	
	</div>
	

</body>
</html>