<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.min.css"	rel="stylesheet">
<link href="webjars/bootstrap/4.0.0/css/bootstrap.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
<script src="webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<title>customer address</title>

<script type="text/javascript">
	function formSubmit(){
		console.log(jjjjj)
		return false;
	}

</script>

</head>
<body class="container-fluid alert-success">

	<div class="col-sm-5 offset-sm-3 justify-content-center" style="background-color:#aaa; margin-top:20%; border:solid white 2px; padding-top:15px;border-radius:10px;" >
		<form  name="addrssForm" action="cartinvoice" method="post" modelAttribute="orderNo" onsubmit="return formSubmit()">
			<div class="form-group form-inline" >
				<label class="col-sm-3">Name </label>
				<input type="text" class="form-control col-sm-9" name="name" value="${clintInfo.name}">
			</div>
			<div class="form-group form-inline" >
				<label class="col-sm-3">Mobile No </label>
				<input type="text" class="form-control col-sm-9" name="mobileNo" value="${clintInfo.mobile}">
			</div>
			<div class="form-group form-inline" >
				<label class="col-sm-3">Address </label>
				<textarea class="col-sm-9" rows="3" cols="50" name="address">${clintInfo.address}</textarea>
			</div>
			<div class="form-group form-inline" >
				<label class="col-sm-3">Email Id </label>
				<input type="text" class="form-control col-sm-9" name="emailId" value="${clintInfo.emailId}">
			</div>
			<div class="form-group">
				<input type="submit" class="form-control col-sm-6 offset-sm-3" value="submit">
			</div>
		
		</form>
	
	</div>
</body>
</html>