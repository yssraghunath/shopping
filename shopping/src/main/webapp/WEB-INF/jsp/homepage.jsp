<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<style type="text/css">
	img{
		width:100%;
		height:100%;
	}
</style>


<title>home page</title>
</head>
<body class="container-fluid alert-success">
	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
 --%>
	<%-- <div class="row">
		<%@include file="menu2.html" %>
	</div>
	<div class="row">
		<%@include file="menu3.html" %>
	</div>
	 --%>
	<%--  <div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>	
	<div>
		<%@include file="check_session.jsp" %>
	</div>
	 
	<div class="row" style="margin-top:1px;">
	 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
      <li data-target="#myCarousel" data-slide-to="5"></li>
      <li data-target="#myCarousel" data-slide-to="6"></li>
      
    </ul>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" style="height:760px;">
      <div class="carousel-item active">
        <img src="resources/images/slider/image (1).JPG"  alt="Los Angeles">
      </div>

      <div class="carousel-item">
        <img src="resources/images/slider/image (2).JPG"  alt="Chicago">
      </div>
    
      <div class="carousel-item">
        <img src="resources/images/slider/image (3).JPG"  alt="New york">
      </div>
      <div class="carousel-item">
      	<img src="resources/images/slider/image (4).JPG" alt="india ">
      </div>
       <div class="carousel-item">
      	<img src="resources/images/slider/image (5).JPG" alt="india ">
      </div>
      <div class="carousel-item">
      	<img src="resources/images/slider/image (6).JPG" alt="india " style="height:100%; width:100%;">
      </div>
      <div class="carousel-item">
      	<img src="resources/images/slider/image (7).JPG" alt="india ">
      </div>
      
  
    </div>

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" data-slide="next">
      <span class="carousel-control-next-icon"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
	
	
	
	</div>
	

</body>
</html>