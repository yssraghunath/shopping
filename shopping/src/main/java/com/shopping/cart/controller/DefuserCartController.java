package com.shopping.cart.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shopping.cart.entity.Cart;
import com.shopping.cart.entity.DefuserItemEntity;
import com.shopping.cart.services.IGenericService;

@Controller
public class DefuserCartController {
	@Autowired
	IGenericService<DefuserItemEntity> defuserItemService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/add_cart",method=RequestMethod.POST)
	public String itemCart(@ModelAttribute("item")DefuserItemEntity item,
			HttpSession session,Model model) {
		
		System.out.println("cart value --------------"+item);
		double price=item.getItemMRP();
		int quantity=(int)item.getQuantity();
		if(session.getAttribute("carts")==null) {
			System.out.println("session is null");
			List<Cart> carts=new ArrayList<>();
			long id=1;
			DefuserItemEntity dItem=defuserItemService.findOne(new DefuserItemEntity(),item.getId());
			carts.add(new Cart(dItem,price,quantity));
			session.setAttribute("carts",carts);
			session.setAttribute("cartSize", carts.size());
			
		}else {
			List<Cart> carts=(List<Cart>)session.getAttribute("cart");
			DefuserItemEntity dItem=defuserItemService.findOne(item, item.getId());
			carts.add(new Cart(dItem,price,quantity));
			session.setAttribute("carts",carts);
			session.setAttribute("cartSize", carts.size());
		}
		System.out.println((List<Cart>)session.getAttribute("cart"));
		model.addAttribute("carts",(List<Cart>)session.getAttribute("cart"));
		
		return "redirect:product";
	}
	

}
