package com.shopping.cart.controller;



import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.shopping.cart.entity.AddToCartEntity;
import com.shopping.cart.entity.ClientInfoEntity;
import com.shopping.cart.entity.DefuserItemCategoryEntity;
import com.shopping.cart.entity.DefuserItemEntity;
import com.shopping.cart.entity.DefuserItemSubcategoryEntity;
import com.shopping.cart.entity.DefuserPurchaseEntity;
import com.shopping.cart.entity.DefuserSalesEntity;
import com.shopping.cart.entity.OrderEntity;
import com.shopping.cart.entity.OrderNumberEntity;
import com.shopping.cart.entity.SelfCompanyDetailsEntity;
import com.shopping.cart.entity.UserEntity;
import com.shopping.cart.services.IGenericService;


@Controller
public class DefuserController {
	
	private static final Logger logger = LoggerFactory.getLogger(DefuserController.class);
	//private String UPLOADED_FOLDER ="D://NTPC_ALERT_SERVICES//";
	@Value("${filepath.uploadpath}")
	private String UPLOADED_FOLDER;
	@Value("${order.formate}")
	private String ORDERFOMATE; 
	@Value("${image.upload}")
	private String IMAGE_PATH;
	
	@Autowired
	ServletContext context;
	@Value("${project.name}")
	private String projectName;
	
	
	@Autowired
	private HttpSession session;
	
	
	@Autowired
	private IGenericService<DefuserItemEntity> defuserItemService;
	
	@Autowired
	private IGenericService<DefuserPurchaseEntity> defuserPurchaseService;
	
	@Autowired
	private IGenericService<DefuserSalesEntity> defuserSalesService;
	
	@Autowired
	private IGenericService<AddToCartEntity> addToCartService;
	
	@Autowired
	private IGenericService<SelfCompanyDetailsEntity> selfService;
	
/*	@Autowired
	private IGenericService<ClientInfoEntity> clintService;
	
*/	
	@Autowired
	private IGenericService<UserEntity> userService;

	@Autowired
	private IGenericService<OrderNumberEntity> orderNumberService;
	@Autowired
	private  IGenericService<OrderEntity> orderService;
	
	@Autowired
	private IGenericService<DefuserItemCategoryEntity> itemCategoryService;
	@Autowired
	private IGenericService<DefuserItemSubcategoryEntity> itemSubcategoryService;
	
	
	DataFormatter dataFormatter=new DataFormatter();
	SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	
	//list of item Descending order
	@RequestMapping("/defuseritem")
	public String getDefuserItemList(Model model) {
		List<DefuserItemEntity> itemList=defuserItemService.findAll(new DefuserItemEntity(),"ORDER BY id DESC");
		List<DefuserItemCategoryEntity> itemCategoryList=itemCategoryService.findAll(new DefuserItemCategoryEntity());
		List<DefuserItemSubcategoryEntity> itemSubCategoryList=itemSubcategoryService.findAll(new DefuserItemSubcategoryEntity());
		logger.info("total item list "+itemList);
		model.addAttribute("itemList", itemList);
		model.addAttribute("category", itemCategoryList);
		model.addAttribute("subCategory", itemSubCategoryList);
		
		return "DefuserStock_form";
	}
	
	//save item 
	@RequestMapping(value="/savedefuseritem",method=RequestMethod.POST)
	public String saveDefuserItem(Model model,@ModelAttribute("item") DefuserItemEntity item) {
		DefuserItemEntity item1=null;
		if(item !=null) {
			item.setImageUrl(imageUrlUpdate(item.getItemCode()));
			long id=Long.parseLong(item.getItemCategory());
			DefuserItemCategoryEntity itemCategory=itemCategoryService.findOne(new DefuserItemCategoryEntity(), id);
			item.setItemCategory(itemCategory.getCategory());
			defuserItemService.save(item);
			
			item1=defuserItemService.findOne(new DefuserItemEntity(),"where itemCode ='"+item.getItemCode()+"'");
			logger.info("item code "+item.getItemCode()+"   item id "+item1.getId());
			DefuserPurchaseEntity pItem=new DefuserPurchaseEntity();
			Date date = new Date();  
		    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
		    String strDate= formatter.format(date); 
			pItem.setDateAndTime(strDate);
			pItem.setItemId(item1.getId());
			pItem.setItemName(item.getItemName());
			pItem.setItemPrice(item1.getItemOSP());
			pItem.setQuantity(item1.getQuantity()+"");
			pItem.setTotalPrice(item1.getQuantity()*item1.getItemOSP());
			pItem.setVendorName("Add Stock");
			defuserPurchaseService.save(pItem);
			
		
		}else {
			System.out.println("data not found...............");
		}
		System.out.println("item --"+item);
		return "redirect:/imageupload?id="+item1.getId();
	}
	

	//showing image upload page..
	@RequestMapping("/imageupload")
	public String gString(@RequestParam("id") long id,Model model) {
		model.addAttribute("itemCode",id);
		return "defuser_item_image_upload";
	}
	
	@RequestMapping("uploaditemimage")
	public String uploadItemImage(@RequestParam("itemId") long itemId,@RequestParam("file") MultipartFile file,Model model) throws Exception {
		String fileName=itemId+"";
	
		if(file.isEmpty()) {
			logger.info("image file not found");
			model.addAttribute("message","no file upload");
		}else {
			File doc=new File("");
			if(!doc.exists()) {
				logger.info("DIR : NOT EXISTS !");
				doc.mkdirs();
			}
			
			
			String originalFileName=file.getOriginalFilename();
			logger.info("original file name        ---- "+originalFileName);
			String [] word=originalFileName.split("\\S+");
			for(int i=0;i<word.length;i++) {
				fileName=fileName+word[i].replaceAll(" ", "");
				logger.info("-----Trim file name----- " + fileName);
			}
			
			
			
			String folder=System.getProperty("user.dir");
			logger.info("system folder ----------------------- "+folder);
		//	folder=folder.replace("/","//");
			logger.info(" inner folder--------------------  "+IMAGE_PATH);
			logger.info("file name  ------------"+originalFileName);
			folder=folder+IMAGE_PATH+originalFileName;
			logger.info("complate Path ----------------------- "+folder);
			byte[] bytes=file.getBytes();
		DefuserItemEntity item=defuserItemService.findOne(new DefuserItemEntity(),itemId);
		logger.info(" get item "+item);
			
				item.setImageUrl(originalFileName);
				defuserItemService.update(item);
			
		Path path=Paths.get(folder);
		logger.info("Image Path : "+path);
	
			Files.write(path,bytes);
	//		uploadDocuments(file,itemId);
			model.addAttribute("message","file successfully uploaded");
		}
		
		return "redirect:defuseritem";
	}
	
	
	
	
	
	@RequestMapping(value="/updatedefuseritem",method=RequestMethod.POST)
	public String updateDefuserItem(Model model,@ModelAttribute("item") DefuserItemEntity item) {
		logger.info("-------------New Item----------\n"+item);
		DefuserItemEntity oldItem=defuserItemService.findOne(new DefuserItemEntity(), item.getId());
		logger.info("-------------Old Item ----------\n"+oldItem);
		if(oldItem !=null) {
			
			defuserItemService.update(item);
		}else {
			System.out.println("data not found...............");
		}
		System.out.println("item --"+item);
		return "redirect:/defuseritem";
	}
	
	@RequestMapping(value="/purchasedefuseritem",method=RequestMethod.POST)
	public String purchaseDefuserItem(Model model,@ModelAttribute("item") DefuserPurchaseEntity item) {
		if(item !=null) {
			//defuserPurchaseService.save(item);
			System.out.println("PURCHASE ..............."+item);
			long itemId=item.getItemId();
			DefuserItemEntity itemTotal=defuserItemService.findOne(new DefuserItemEntity(),itemId);	
			//String itemQuantity=
					int q1=(int) itemTotal.getQuantity();
					int q2=Integer.parseInt(item.getQuantity());
					
					System.out.println("Total q "+ q1+"\n purchase q "+q2);
					int q=q1+q2;
					System.out.println("Total q "+ q);
					itemTotal.setQuantity(q);
					defuserItemService.update(itemTotal);
					
					Date date = new Date();  
				    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
				    String strDate= formatter.format(date); 
				 //   item.setItemPrice(1000.00);
					item.setDateAndTime(strDate);
					System.out.println("purchase item \n  "+ item);
					try {
						defuserPurchaseService.save(item);
					}catch(Exception e){
						logger.info(e.getMessage());
					}
					
		}else {
			System.out.println("data not found...............");
		}
		//System.out.println("item --"+item);
		return "redirect:/defuseritem";
	}
	
	@RequestMapping(value="/saledefuseritem",method=RequestMethod.POST)
	public String saleDefuserItem(Model model,@ModelAttribute("item") DefuserSalesEntity item) {
		if(item !=null) {
			System.out.println("SALE  ..............."+item);
			long itemId=item.getItemId();
			DefuserItemEntity itemTotal=defuserItemService.findOne(new DefuserItemEntity(),itemId);	
			//String itemQuantity=
					int q1=(int) itemTotal.getQuantity();
					int q2=Integer.parseInt(item.getQuantity());
					
					System.out.println("Total q "+ q1+"\n purchase q "+q2);
					int q=q1-q2;
					System.out.println("Total q "+ q);
					itemTotal.setQuantity(q);
					defuserItemService.update(itemTotal);
					
					Date date = new Date();  
				    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
				    String strDate= formatter.format(date); 
				    
					item.setDateAndTime(strDate);
					System.out.println("Total item \n  "+ item);
					defuserSalesService.save(item);
			System.out.println("SALE ..............."+item);
		}else {
			System.out.println("data not found...............");
		}
		System.out.println("item --"+item);
		return "redirect:/defuseritem";
	}
	
	
	@RequestMapping("/saledefuserlist")
	public String getSalesDetails(Model model){
		List<DefuserSalesEntity> itemsList=defuserSalesService.findAll(new DefuserSalesEntity());
		logger.info("sales item list "+itemsList);
		model.addAttribute("itemsList", itemsList);
		return "DefuserSale_form";
	}
	
	@RequestMapping("/purchasedefuserlist")
	public String getPurchaseDetails(Model model){
		List<DefuserPurchaseEntity> itemsList=defuserPurchaseService.findAll(new DefuserPurchaseEntity());
		logger.info("purchase item list \n  "+itemsList+" \n ");
		model.addAttribute("itemsList", itemsList);
		return "DefuserPurchase_form";
	}

	
	
	
	@RequestMapping(value= {"/","/product"})
	public String getProductDetails(Model model,HttpSession session ) {
		     context.setAttribute("TITLE", projectName);
			UserEntity user=(UserEntity) session.getAttribute("user");
			List<AddToCartEntity> cartItemList=new ArrayList<>();
			List<DefuserItemEntity> itemList=defuserItemService.findAll(new DefuserItemEntity(),"order by id Desc");
			if(user !=null) {
				cartItemList=addToCartService.findAll(new AddToCartEntity(), "where userId = '"+user.getId()+"'");
				model.addAttribute("role", user.getRole().getRoleType());
						}
			logger.info("total cart item list "+cartItemList);
		   model.addAttribute("cartItemList", cartItemList);
			logger.info("total item list "+itemList);
			model.addAttribute("itemList", itemList);
			Map<String,List<String>> map=getCategoryList();
			model.addAttribute("category",map);
			@SuppressWarnings("unchecked")
			List<AddToCartEntity> cartList=(List<AddToCartEntity>)session.getAttribute("carts");
			model.addAttribute("cartItemList", cartList);
		
			return "product_list";		
		
		
	}
	
	
	@RequestMapping(value= {"/productslist"})
	public String getProductDetailsFilter(Model model,HttpSession session,@RequestParam String cat,@RequestParam String subcat) {
		     context.setAttribute("TITLE", projectName);
			UserEntity user=(UserEntity) session.getAttribute("user");
			List<AddToCartEntity> cartItemList=new ArrayList<>();
			logger.info(" {}  "+cat+"    {}  "+subcat);
			List<DefuserItemEntity> itemList=new ArrayList<>();
			if((cat==null)||(cat.equals("")) && (subcat==null)||(subcat.equals(""))) {
				itemList=defuserItemService.findAll(new DefuserItemEntity(),"order by id Desc");
			}else {
				String condition="where itemCategory = '"+cat+"' AND itemSubCategory ='"+subcat+"'"; 
					 itemList=defuserItemService.findAll(new DefuserItemEntity(),condition);
				
			}
			logger.info("{ list of product } "+itemList);
			if(user !=null) {
				cartItemList=addToCartService.findAll(new AddToCartEntity(), "where userId = '"+user.getId()+"'");
				model.addAttribute("role", user.getRole().getRoleType());
						}
			logger.info("total cart item list "+cartItemList);
		   model.addAttribute("cartItemList", cartItemList);
			logger.info("total item list "+itemList);
			model.addAttribute("itemList", itemList);
			Map<String,List<String>> map=getCategoryList();
			model.addAttribute("category",map);
			@SuppressWarnings("unchecked")
			List<AddToCartEntity> cartList=(List<AddToCartEntity>)session.getAttribute("carts");
			model.addAttribute("cartItemList", cartList);
		
			return "product_list";		
		
		
	}


	public Map<String,List<String>> getCategoryList(){
		
		Map<String,List<String>> map=new HashedMap<>();
		
		List<DefuserItemCategoryEntity> itemCategoryList=itemCategoryService.findAll(new DefuserItemCategoryEntity());	//List<AddToCartEntity> cartItemList=addToCartService.findAll(new AddToCartEntity(), "where userId = '"+user.getId()+"'");
		for(DefuserItemCategoryEntity entity:itemCategoryList) {
			List<DefuserItemSubcategoryEntity> subCatList=itemSubcategoryService.findAll(new DefuserItemSubcategoryEntity(), "where category = '"+entity.getId()+"'");
			List<String> subCatName=new ArrayList<>();
			for(DefuserItemSubcategoryEntity subcat:subCatList) {
				subCatName.add(subcat.getSubCategory());
			}
			map.put(entity.getCategory(), subCatName);
		}
		return map;
	}
	
	
	@RequestMapping(value="/addcart", method=RequestMethod.POST)
	public String addToCartItem(Model model,@ModelAttribute("item") DefuserPurchaseEntity item,HttpSession session ) {
		
		logger.info("item----------------add cart---------------------------------- "+item);
		UserEntity user=(UserEntity) session.getAttribute("user");
	//	if(user !=null) {
		long itemCode=item.getItemId();
		DefuserItemEntity realItem=defuserItemService.findOne(new DefuserItemEntity(),itemCode);
		logger.info("real item ---------------------"+realItem);
	//	UserEntity user=(UserEntity) session.getAttribute("user");
		logger.info("real user ---------------------"+user);
		AddToCartEntity cartItem=new AddToCartEntity();
		cartItem.setItemCode(realItem);
		cartItem.setUserId(user);
		Date date = new Date();  
	    String strDate= dateFormatter.format(date); 
	    cartItem.setAddToCardDate(strDate);
	    int itemQuantity=Integer.parseInt(item.getQuantity());
		cartItem.setQuantity(itemQuantity);
		double itemPrice=item.getItemPrice();
		cartItem.setItemPrice(itemPrice);
		cartItem.setTotalPrice(itemQuantity*itemPrice);
		logger.info("cart item "+cartItem);
		
		if(session.getAttribute("carts")==null) {
			logger.info("session value null ");
			List<AddToCartEntity> cartList=new ArrayList();	
			cartList.add(cartItem);
			session.setAttribute("carts",cartList);
		}else {
			logger.info("session value ! null ");
			List<AddToCartEntity> cartList=(List<AddToCartEntity>)session.getAttribute("carts");
			cartList.add(cartItem);
			logger.info("session value null "+cartList);
			session.setAttribute("carts",cartList);
		}
		
		//addToCartService.save(cartItem);
		return "redirect:/product";
		/*}else {
			logger.info("please login first");
			return "redirect:/signin";
		}*/
	}
	
	@RequestMapping(value="/removecart/{itemCode}",method=RequestMethod.GET)
	public String RemoveToCartItem(Model model,@PathVariable int itemCode) {
		
		
		UserEntity user=(UserEntity) session.getAttribute("user");
		List<AddToCartEntity> list=(List<AddToCartEntity>)session.getAttribute("carts");
		list.remove(itemCode-1);
		session.setAttribute("carts", list);
		logger.info("remove from cart item id "+itemCode);
		long id=itemCode;
		AddToCartEntity entity=addToCartService.findOne(new AddToCartEntity(), id);
		logger.info("remove item form cart "+entity);
		if(entity !=null) {
		//	addToCartService.delete(entity);
		}
		
		return "redirect:/product";
	}
	
	
	@RequestMapping("/addorderaddress")
	public String orderAddAddress(Model model,HttpSession session) {
		if(session.getAttribute("user") !=null) {
			UserEntity user=(UserEntity) session.getAttribute("user");
			model.addAttribute("clintInfo",user);
			return "order_address";
		}else {
			return "redirect:signin";
		}
		
	}
	
	@RequestMapping("/cartinvoice")
	public String previewInvoice(Model model,HttpSession session,@ModelAttribute ("orderNo") OrderNumberEntity orderNo) {
		
		logger.info("order no ----"+orderNo);
		
		UserEntity user=(UserEntity) session.getAttribute("user");
		String orderId="";
		if(user !=null) {
		
		@SuppressWarnings("unchecked")
		List<AddToCartEntity> cartItemList=(List<AddToCartEntity>)session.getAttribute("carts");
		SelfCompanyDetailsEntity selfDetails=selfService.findOne(new SelfCompanyDetailsEntity(), 1);
		//System.out.println("self Details "+selfDetails);
		model.addAttribute("selfDetails",selfDetails);
		model.addAttribute("cartItemList",cartItemList);
		//model.addAttribute("user", user);
		//generate order number
	//	OrderNumberEntity orderNumber=new OrderNumberEntity();
		orderNo.setUserName(user.getUserName());
		Date currentDate=new Date();
		String strDate= dateFormatter.format(currentDate);
		orderNo.setOrderDate(strDate);
		String  yy=new Date().getYear()+1900+"";
		yy=yy.substring(2);
		int y=Integer.parseInt(yy)+1;
		System.out.println(yy+"-"+y);
		String orderFormate=ORDERFOMATE+"/"+yy+"-"+y;
		orderNo.setOrderFormate(orderFormate);;
		OrderNumberEntity orderNumber1=orderNumberService.update(orderNo);
		List<OrderEntity> orderList=new ArrayList<>();
		orderId=orderNumber1.getId()+"";
		logger.info("order id "+orderId);
		for(AddToCartEntity cart:cartItemList) {
				
			OrderEntity order=new OrderEntity();
			order.setOrderId(orderNumber1);
			order.setUserId(user);
			logger.info(cart.getItemCode().getId()+" ");
			//DefuserItemEntity realItem=defuserItemService.findOne(new DefuserItemEntity(),itemCode);
			order.setItemCode(cart.getItemCode());
			order.setItemPrice(cart.getItemPrice());
			order.setOrderDate(strDate);
			order.setOrderStatus("");
			order.setQuantity(cart.getQuantity());
			order.setTotalPrice(cart.getTotalPrice());
			orderService.save(order);
			orderList.add(order);
			addToCartService.delete(cart);
			logger.info("order is success ful \n "+order);
		
			model.addAttribute("orderList",orderList);	
			
		}

		
		
		logger.info("order id = "+orderId);
	//	model.addAttribute("clientInfo",clientInfo);
	//	model.addAttribute("pageName", "/product");
		session.removeAttribute("carts");
		return "redirect:customerinvoice/?id="+orderId;
		//return "defuser_invoice";
		}else {
			return "redirect:/product";
	//		return "redirect:customerinvoice/?id=${orderId}";	
		}
	
	
	//return "order_address";	
	}
	

	@RequestMapping("/order")
	public String orderAcceptOrReject(Model model,@RequestParam("itemId") String itemId,
			@RequestParam("orderValue") String orderValue,
			@RequestParam("deliveryValue") String deliveryValue) {
		
		long id=Long.parseLong(itemId);
		
		OrderEntity order=orderService.findOne(new OrderEntity(), id);
		if(!orderValue.equals("")) {
			order.setOrderStatus(orderValue);
		}
		if(!deliveryValue.equals("")) {
			order.setDeliveryStatus(deliveryValue);
		}
		
		return "redirect:/orders";
	}
	
	@RequestMapping("/orders")
	public String getOrderList(Model model) {
		List<OrderNumberEntity> orderNumberList=orderNumberService.findAll(new OrderNumberEntity(),"order by id Desc");
		List<OrderEntity> orderList=orderService.findAll(new OrderEntity());
		model.addAttribute("orderNumberList",orderNumberList);
		model.addAttribute("orderList",orderList);
		return "defuser_order_list";
	}
	
	
	//it's can  see admin invoce  
	@RequestMapping("/customerorder")
	public String getOneOrderDetails(Model model,@RequestParam("id") String id) {
		SelfCompanyDetailsEntity selfDetails=selfService.findOne(new SelfCompanyDetailsEntity(), 1);
		//System.out.println("self Details "+selfDetails);
		model.addAttribute("selfDetails",selfDetails);
		List<OrderEntity> orderList=orderService.findAll(new OrderEntity(),"where orderId ='"+id+"'");
		logger.info("\n---------------------- order list -----------------------\n "+orderList);
		model.addAttribute("orderList",orderList);
		return "customer_order";
	}
	
	
	@RequestMapping("/customerinvoice")
	public String getOneOrderIncoiceDetails(Model model,@RequestParam("id") String id) {
		SelfCompanyDetailsEntity selfDetails=selfService.findOne(new SelfCompanyDetailsEntity(), 1);
		//System.out.println("self Details "+selfDetails);
		model.addAttribute("selfDetails",selfDetails);
		List<OrderEntity> orderList=orderService.findAll(new OrderEntity(),"where orderId ='"+id+"'");
		logger.info("\n---------------------- order list -----------------------\n "+orderList);
		model.addAttribute("pageName","/customerorder?id="+id);
		model.addAttribute("orderList",orderList);
		return "defuser_invoice";
	}
	

	@RequestMapping("orderstatus")
	public String acceptOrder(Model model,@RequestParam("id") String id,@RequestParam("status") String status) {
	
		logger.info("\n---------------------- "+id+" Order Status update "+status+" -----------------------\n ");
		long oid=Long.parseLong(id);
			OrderEntity order=orderService.findOne(new OrderEntity(), oid);
			logger.info(" order "+order);
			order.setOrderStatus(status);
			long orderId=order.getOrderId().getId();
			logger.info("\n---------------------- "+id+" Order Status update  -----------------------\n ");
			orderService.update(order);
			
		return "redirect:/customerorder?id="+orderId+"";
	}

	@RequestMapping("deliveries")
	public String deliveryPage(){
		UserEntity user=(UserEntity) session.getAttribute("user");
		if(user !=null) {
			return "defuser_delivery";
		}else {
			return "index";
		}
		
	}


	//  
	@RequestMapping(value="/delivery",method=RequestMethod.GET)
	public String deliverOneOrderDetails(Model model,@RequestParam("id") String id) {
		
		SelfCompanyDetailsEntity selfDetails=selfService.findOne(new SelfCompanyDetailsEntity(), 1);
		//System.out.println("self Details "+selfDetails);
		System.out.println("id.............."+id);
		model.addAttribute("selfDetails",selfDetails);
		UserEntity user=(UserEntity) session.getAttribute("user");
		model.addAttribute("role", user.getRole().getRoleType());
		List<OrderEntity> orderList=orderService.findAll(new OrderEntity(),"where orderId ='"+id+"'");
		logger.info("\n---------------------- order list -----------------------\n "+orderList);
		model.addAttribute("orderList",orderList);
		return "defuser_delivery";
	}

	
	// supradmin or admin 
	@RequestMapping("deliverystatus")
	public String acceptDelivery(Model model,@RequestParam("id") String id,@RequestParam("status") String status) {
	
		logger.info("\n---------------------- id "+id+" delivery Status update  status "+status+" -----------------------\n ");
		long oId=setDeliveryStatusAndDate(id,status);
	/*	long oid=Long.parseLong(id);
		OrderEntity order=orderService.findOne(new OrderEntity(), oid);
		logger.info(" order "+order);	
		long orderId=order.getOrderId().getId();
		order.setDeliveryStatus(status);
		Date date = new Date();  
		 // SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
		 String deliveryDate= dateFormatter.format(date); 
		 order.setDeliveryDate(deliveryDate);
			logger.info("\n---------------------- "+id+"delivery Status update  -----------------------\n ");
			orderService.update(order); */
		return "redirect:/customerorder?id="+oId+"";
	}


	//delivery man 
	@RequestMapping(value="/setdeliverydate",method=RequestMethod.GET)
	public String updatedeliverOneOrderDetails(Model model,@RequestParam("id") String id) {
	
		long oId=setDeliveryStatusAndDate(id,"ACCEPTED");
		/*
		SelfCompanyDetailsEntity selfDetails=selfService.findOne(new SelfCompanyDetailsEntity(), 1);
		//System.out.println("self Details "+selfDetails);
		System.out.println("id.............."+id);
		model.addAttribute("selfDetails",selfDetails);
		
		long idi=Long.parseLong(id);
		OrderEntity order=orderService.findOne(new OrderEntity(), idi);
		Date date = new Date();  
		   // SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
		   String deliveryDate= dateFormatter.format(date); 
		    
		order.setDeliveryDate(deliveryDate);
		order.setDeliveryStatus("ACCEPTED");
		OrderEntity updateOrder=orderService.update(order);
		long dId=updateOrder.getOrderId().getId();*/
		return "redirect:/delivery?id="+oId;
	}
	

	public long setDeliveryStatusAndDate(String id,String status) {
		long oid=Long.parseLong(id);
		OrderEntity order=orderService.findOne(new OrderEntity(), oid);
		logger.info(" order "+order);	
		long orderId=order.getOrderId().getId();
		order.setDeliveryStatus(status);
		Date date = new Date();  
		 // SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
		 String deliveryDate= dateFormatter.format(date); 
		 order.setDeliveryDate(deliveryDate);
			logger.info("\n---------------------- "+id+"delivery Status update  -----------------------\n ");
			OrderEntity order1=orderService.update(order); 

		if(status.equals("ACCEPTED")) {
			DefuserSalesEntity saleItem=new DefuserSalesEntity();
			saleItem.setBuyerName(order.getUserId().getUserName());
			saleItem.setDateAndTime(deliveryDate);
			saleItem.setItemName(order.getItemCode().getItemName());
			saleItem.setItemId(order.getItemCode().getId());
			saleItem.setItemPrice(order.getItemPrice());
			saleItem.setQuantity(order.getQuantity()+"");
			saleItem.setTotalPrice(order.getTotalPrice());
			//Defuser item id
			long itemId=order.getItemCode().getId();
			DefuserItemEntity itemTotal=defuserItemService.findOne(new DefuserItemEntity(),itemId);	
			//String itemQuantity=
					int q1=(int) itemTotal.getQuantity();
					int q2=Integer.parseInt(saleItem.getQuantity());
					
					System.out.println("Total q "+ q1+"\n sale q "+q2);
					int q=q1-q2;
					System.out.println("Total q "+ q);
					itemTotal.setQuantity(q);
					defuserItemService.update(itemTotal);  
				     
				    
					defuserSalesService.save(saleItem);
			System.out.println("SALE ..............."+saleItem);

		
		}
		return order1.getOrderId().getId();
	}
	
	
	
	
	
	
	@RequestMapping(value="/uploadfile",method=RequestMethod.POST)
	public String uploadExcelFile(@RequestParam("file") MultipartFile file,ModelMap modelMap) throws Exception  {
		
		
		if(file.isEmpty() ) {
			logger.info("file is empty...");
			modelMap.addAttribute("Message", "no file uploaded");
		}{
			modelMap.addAttribute("file", file);
			modelMap.addAttribute("Message", "Successfull upload");
			System.out.println(" file --- "+file.getSize());
			System.out.println(" file --- "+file.getOriginalFilename());
			uploadDocuments(file);
			
		}
		return "redirect:/defuseritem";
	}
	
	public  void readFile(File excelFile) throws EncryptedDocumentException, InvalidFormatException, IOException {
		//List<Table> tableList=new ArrayList<Table>();
		//DefuserItemEntity defuserItem=new DefuserItemEntity();
		Workbook workbook=WorkbookFactory.create(excelFile);
	//	System.out.println("work book get number of sheet  "+workbook.getNumberOfSheets());
		for(Sheet sheet:workbook) {
			//System.out.println("sheet "+sheet.getSheetName());
				for(Row row:sheet) {
				//	System.out.println("row "+row.toString());
					
					//System.out.println("number of cell "+row.getPhysicalNumberOfCells());
					
					//System.out.println("number of cell "+row.getCell(0));
					//System.out.println("number of cell "+row.getCell(1));
					//System.out.println("number of cell "+row.getCell(2));
					
			//	System.out.println("row number "+row.getRowNum());
				if(row.getRowNum()!=0) {
					DefuserItemEntity defuserItem=new DefuserItemEntity();
					//item code
					Cell cell=row.getCell(0);
					 String itemCode=dataFormatter.formatCellValue(cell);
					 defuserItem.setItemCode(itemCode);
					
					 //vendor item code
					 cell=row.getCell(1);
					 String vendorCode=dataFormatter.formatCellValue(cell);
					defuserItem.setItemCodeV(vendorCode);
					
					//item Name
					 cell=row.getCell(2);
					 String itemName=dataFormatter.formatCellValue(cell);
					 defuserItem.setItemName(itemName);
					 
					 //item description
					 cell=row.getCell(3);
					 String itemDesc=dataFormatter.formatCellValue(cell);
					 defuserItem.setItemDescription(itemDesc);
					
					 //item category
					 cell=row.getCell(4);
					 String category=dataFormatter.formatCellValue(cell);
					 defuserItem.setItemCategory(category);
					 
					//item sub  category
					 cell=row.getCell(5);
					 String subCategory=dataFormatter.formatCellValue(cell);
					 defuserItem.setItemSubCategory(subCategory);
					 
					 
					
					 //quantity
					 cell=row.getCell(6);
					 long quantity=Long.parseLong(dataFormatter.formatCellValue(cell));
					 defuserItem.setQuantity(quantity);;
					
					 //MRP
					 cell=row.getCell(7);
					 double mrp=Double.parseDouble(dataFormatter.formatCellValue(cell));
					 defuserItem.setItemMRP(mrp);
					
					//olp
					 cell=row.getCell(8);
					 double olp=Double.parseDouble(dataFormatter.formatCellValue(cell));
					 defuserItem.setItemOLP(olp);
					
					//TP
					 cell=row.getCell(9);
					 double tp=Double.parseDouble(dataFormatter.formatCellValue(cell));
					 defuserItem.setItemTP(tp);
					
					//OSP
					 cell=row.getCell(10);
					 double osp=Double.parseDouble(dataFormatter.formatCellValue(cell));
					 defuserItem.setItemOSP(osp);
					 System.out.println("------------------------\n "+defuserItem+"\n ------------------------------------"); 
					 if(defuserItem !=null) {
						 System.out.println("------------------------\n "+defuserItem+"\n ------------------------------------"); 
						 defuserItem.setImageUrl(imageUrlUpdate(defuserItem.getItemCode()));
						 defuserItemService.save(defuserItem);
					 }else {
						 logger.info(" Data not found...");
					 }
					 
					// tableList.add(new Table(id,name,price));
					 
					
				}
					 
					
				//	tableList.add(new Table(id,name,price));
					
					/*for(Cell cell:row) {
						String cellValue=dataFormatter.formatCellValue(cell);
					//	System.out.print(cellValue+"\t");
					}*/
					System.out.println("");
				}
			
		}
		
	}
	
	
	
		private String uploadDocuments(MultipartFile multipartFile) throws Exception {
			
			String currentDate="\n-----------------------------------------------------\n "+new Date()+"  \n-----------------------------------------------------\n ";
			int  DUPLICATE_NUMBE_COUNT=0;

			String folder_name = "";
			String fileName = "";
			String fileNameO="";
			String newDocName = "";
			String newPath = "";
			String loginId="ExcelFile";
			try {
				if (!multipartFile.isEmpty()) {
					File doc = new File(UPLOADED_FOLDER  + "/");
					
					if (!doc.exists()) {
						logger.info(" Dir " + loginId + " NOT Exists");
						doc.mkdirs();
					}
					

					String s = multipartFile.getOriginalFilename();
					String[] words = s.split("\\s+");
					System.out.println("word "+words);
					for (int i = 0; i < words.length; i++) {
						fileName = fileName + words[i].replaceAll(" ", " ");
					
						/*mobile.txt*/
						logger.info("-----Trim file name----- " + fileName);
						
					}
					
					folder_name = UPLOADED_FOLDER  + loginId + "/" + fileName;
					
					/* D:/NTPC_ALERT_SERVICES/219/mobile.txt */
					
					logger.info("----- Orignal folder_name :----- " + folder_name);
				
					File file = new File(fileName);
					File filePath = file.getAbsoluteFile();
					logger.info("------ Get Real Path Where Doc from Hit.. -------- " + filePath); 
					
//					if (fileNamePrefix != null) {
//						newDocName = fileNamePrefix + fileName;
//						logger.info("New Document nmae " + newDocName);
//					} else {
//						newDocName = fileName;
//						logger.info("New Document nmae " + newDocName);
	//
//					}
					
					newDocName = fileName;
					logger.info("-----New Document name ---- " + newDocName);
					
					/*  D:/NTPC_ALERT_SERVICES/219/mobile.txt  */
					
					newPath = UPLOADED_FOLDER  + loginId + "/" + newDocName;
					logger.info("-----rename file -----: " + newPath);
					File newFile = new File(newPath);
					logger.info("New File ----- " + newFile);

					if (!file.isDirectory()) {
						file.renameTo(newFile);
					}
					
					  try {
						// Get the file and save it somewhere
						byte[] bytes = multipartFile.getBytes();
						logger.info("----Bytes----" + bytes);
														
						Path path = Paths.get(UPLOADED_FOLDER +fileName);
						logger.info("-----path -----" + path);
					
						 Files.write(path, bytes);
						 
					} catch (IOException e) {
						// TODO Auto-generated catch block
						System.err.println(e);
					}
					
					
					//newDocName=path.toString();
					//logger.info("newDocName After To String -------" + newDocName );
				}
			} catch (Exception e) {
				logger.info("----- IOException ----- : " + e.getMessage());
			}
			
			
			//new FileUploadService().Read();
           //new FileUploadService().Read(fileName);
			logger.info("----- ----- "+ fileName);
			boolean txtfile=fileName.endsWith(".txt");
			boolean xlsfile=fileName.endsWith(".xls");
			boolean xlsxfile=fileName.endsWith(".xlsx");
			//logger.info(fileName.endsWith(".txt"));
			//logger.info(fileName.endsWith(".xls"));
			//logger.info(fileName.endsWith(".xlsx"));
			if(txtfile) {
			//	readFileAndStoreInDataBase(UPLOADED_FOLDER  + loginId + "_" +fileName,loginId);
			}else if(xlsfile || xlsxfile){
				//readExcelFile(UPLOADED_FOLDER  + loginId + "_" +fileName,loginId);
			
			}
			File f=new File(UPLOADED_FOLDER   +fileName);
			readFile(f);
			System.out.println(f.getAbsolutePath());
	
			 	return fileName;
		}

		
		
		@RequestMapping(path="/tbl", method=RequestMethod.GET)
		public String goHome(){
			return "testpaggination";
		}
		
		
		//@RequestMapping(path="/updateurl", method=RequestMethod.GET)
		public String imageUrlUpdate(String itemCode) {
			//String itemCode="CKW 410";
			System.out.println("item code "+itemCode);
			String s= System.getProperty("user.dir");
			logger.info("System path "+s);
			s=s.replace("/","//");
			String png=s+"/src/main/webapp/images/items/"+itemCode+".png";
			String PNG=s+"/src/main/webapp/images/items/"+itemCode+".PNG";
			String jpg=s+"/src/main/webapp/images/items/"+itemCode+".jpg";
			String JPG=s+"/src/main/webapp/images/items/"+itemCode+".JPG";
			String fileName="";
			
			if(new File(png).exists()) {
				fileName=itemCode+".png";
			}else if(new File(PNG).exists())
			{
				fileName=itemCode+".PNG";
			}else if(new File(JPG).exists()){
				fileName=itemCode+".JPG";
			}
			else if(new File(jpg).exists()){
				fileName=itemCode+".jpg";
			}else {
				fileName="NOT_IMG.png";
			}
		
		//folder.exists()?s="a":"b") 
		/*List<DefuserItemEntity> itemList=defuserItemService.findAll(new DefuserItemEntity());
		System.out.println();
			
		System.out.println("kkkkkkkkkk "+folder.getName());
		
		File[] listOfFiles = folder.listFiles();
		
	
		    
		for (int i = 0; i < listOfFiles.length; i++) {
		  if (listOfFiles[i].isFile()) {
		    System.out.println("File " + listOfFiles[i].getName());
		    String fname=listOfFiles[i].getName();
		    
		    String fileName=fname.split("\\.")[0];
		    System.out.println("file name "+fileName);
		    String con=" where itemCode = '"+fileName+"'";
		    System.out.println("condition "+con);
		   DefuserItemEntity item=defuserItemService.findOne(new DefuserItemEntity(),con);
		   System.out.println(item);
		   if(item !=null) {
			   item.setImageUrl(fname);
			   DefuserItemEntity itemUpdate=defuserItemService.update(item);
			   System.out.println("update item "+itemUpdate.getImageUrl()+"   itemcode "+itemUpdate.getItemCode());
		   }
		  } else if (listOfFiles[i].isDirectory()) {
		    System.out.println("Directory " + listOfFiles[i].getName());
		  }
		}
*/		
			return fileName;
		}
	
	
}
