package com.shopping.cart.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shopping.cart.entity.ItemEntity;
import com.shopping.cart.entity.PurchaseInfoEntity;
import com.shopping.cart.services.IGenericService;

@Controller
public class PurchaseController {

	private static final Logger logger = LoggerFactory.getLogger(PurchaseController.class);

	@Autowired
	private IGenericService<PurchaseInfoEntity> purchaseService;
	@Autowired
	private IGenericService<ItemEntity> itemService;
	
	
	@RequestMapping("/purchase")
	public String purchaseDetails(Model model) {
		List<PurchaseInfoEntity> purchaseInfoList=purchaseService.findAll(new PurchaseInfoEntity());
		List<ItemEntity> itemList=itemService.findAll(new ItemEntity());
		logger.info("list ----------"+purchaseInfoList);
		model.addAttribute("purchaseInfoList", purchaseInfoList);
		model.addAttribute("itemList", itemList);
		return "purchase_form";
	}
	
	@RequestMapping(value="/savepurchase",method=RequestMethod.POST)
	public String savePurchaseInfo(Model model,@ModelAttribute PurchaseInfoEntity purchaseInfo) {
		logger.info("purchase info "+purchaseInfo);	
		System.out.println("----------------"+purchaseInfo);
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String currentData=dateFormat.format(new Date());
		purchaseInfo.setEntryDate(currentData);
		purchaseService.save(purchaseInfo);
		
		return "redirect:/purchase";
	}
}
