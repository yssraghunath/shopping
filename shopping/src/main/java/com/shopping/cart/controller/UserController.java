package com.shopping.cart.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.shopping.cart.entity.ClientInfoEntity;
import com.shopping.cart.entity.UserEntity;
import com.shopping.cart.entity.UserRoleEntity;
import com.shopping.cart.services.IGenericService;
import com.shopping.cart.utility.AES;



@Controller
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	
	@Value("${userid.prifix}")
	private String userIdPrifix;
	
	@Autowired
	IGenericService<UserEntity> userServices;
	@Autowired
	IGenericService<UserRoleEntity> userRoleServices;
	DataFormatter dataFormatter=new DataFormatter();
	SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	@RequestMapping("/userlist")
	public String userList(Model model) {
		List<UserEntity> userList=userServices.findAll(new UserEntity());
		List<UserRoleEntity> roleList=userRoleServices.findAll(new UserRoleEntity());
		model.addAttribute("list",userList);
		model.addAttribute("roles",roleList);
		return "user_profile";
	}

	@RequestMapping("/login")
	public String getLogin(Model model,@ModelAttribute("user") UserEntity user,HttpSession session) {
	logger.info("\n --------------------------------\n loing user : "+user.getUserName()+"  password  : "+user.getPassword()+"\n  ----------------------------------");
	
	String pageName="homepage";
	try {
		if(user!=null) {
			String query="WHERE userName='"+user.getUserName()+"' AND password ='"+AES.encrypt(user.getPassword())+"'";
			UserEntity entity=userServices.findAll(new UserEntity(),query).get(0);
			
			System.out.println(" -------entity--------------- "+entity);
		if(entity !=null) {
			model.addAttribute("error_message","");
			 session.setAttribute("user", entity);
			//pageName="homepage";
			 String pp="index";
			 logger.info("--------------\n USER ROLE IS :"+entity.getRole()+"   \n----------------------");
			 if(entity.getRole().getRoleType().equals("USER")) {
				
				 pp="product";
			 }else {
				 pp="home";
			 }
			 return "redirect:"+pp;
		}else {
			logger.info("--------------\n USER DETAILS NOT IN DATABASE   \n----------------------");
			model.addAttribute("error_message","This User not here");
			pageName="redirecat:/signin";
		}
				
		}else {
			logger.info("--------------\n USER DETAILS NULL   \n----------------------");	
		}
		
		return pageName;	
	}catch(NullPointerException exp) {
		exp.getMessage();
		logger.info("------\n  NULL POINTER EXCEPTION  HERE "+exp.getMessage()+"\n");
		model.addAttribute("error_message","something went wrong");
		//return "redirect:/";
		return "index";
	}catch(Exception exp) {
		exp.getMessage();
		logger.info("------\n  SOME EXCEPTION  HERE "+exp.getMessage()+"\n");
		model.addAttribute("error_message","Not a Valid User");
		//return "redirect:/";
		return "index";
	}
	
	
		 
	}
	
	
	
	@RequestMapping("/home")
	public String goToHomePage(Model model,HttpSession session) {
		UserEntity entity=(UserEntity) session.getAttribute("user");
		System.out.println("session is "+entity);
		
		if(entity==null) {
			return "redirect:/signin";
		}else if(entity.getRole().getRoleType().equals("USER")) {
			model.addAttribute("role", entity.getRole().getRoleType());
			return "product_list";
		}else if(entity.getRole().getRoleType().equals("DELIVERY MAN")) {
			model.addAttribute("role", entity.getRole().getRoleType());
			return "defuser_delivery";
		}else if(entity.getRole().getRoleType().equals("ADMIN") || entity.getRole().getRoleType().equals("SUPER ADMIN")) {
			model.addAttribute("role", entity.getRole().getRoleType());
			return "homepage";
		}else {
			return "redirect:/signin";
		}
		
		
	}
	
	
	@RequestMapping(value="/registration", method=RequestMethod.POST)
	public String customerRegistrationInfo(Model model,@ModelAttribute ("userInfo") UserEntity userInfo) throws Exception {
		
		System.out.println("----------sdfffffffffffffffffff-------' "+userInfo);
		UserRoleEntity role=null;
		List<UserEntity> list=userServices.findAll(new UserEntity());
		long maxId=0;
		if(list.size()<=0) {
			//super admin	
			role=userRoleServices.findOne(new UserRoleEntity(), 1);
			
		}else {
			//user
			role=userRoleServices.findOne(new UserRoleEntity(), 3);
			maxId=userServices.findAll(new UserEntity(),"order by id desc").get(0).getId();
			
		}
		
		userInfo.setUserName(userIdPrifix+(maxId+1));
		
		userInfo.setPassword(AES.encrypt(userInfo.getMobile()));
		System.out.println("role "+role);
		
		
		userInfo.setEnteryDate(dateFormatter.format(new Date()));
		userInfo.setRole(role);
		userServices.save(userInfo);
		/*user.setPassword(AES.decrypt(user.getPassword()));
		model.addAttribute("user",user);
		System.out.println("-----------------' "+clientInfo);*/
		return "redirect:registration?id="+userInfo.getId();
	}
	
	@RequestMapping("/registration")
	public String registrationSuccess(Model model,@RequestParam long id) throws Exception {
		UserEntity user=userServices.findOne(new UserEntity(), id);
		user.setPassword(AES.decrypt(user.getPassword()));
		logger.info("new user "+user);
		model.addAttribute("user",user);
		return "registration_success";
		
	}
	
	
	//update user role ud => userId and rd => roleId
	@RequestMapping("/updateuser")
	public String updateUserRole(@RequestParam long ud,@RequestParam long rd) {
		UserEntity user=userServices.findOne(new UserEntity(), ud);
		UserRoleEntity role=userRoleServices.findOne(new UserRoleEntity(), rd);
		user.setRole(role);
		userServices.update(user);
		 
		return "redirect:userlist";
	}
	
	@RequestMapping("/deleteuser")
	public String deleteUserRole(@RequestParam long ud) {
		UserEntity user=userServices.findOne(new UserEntity(), ud);
		//UserRoleEntity role=userRoleServices.findOne(new UserRoleEntity(), rd);
		///user.setRole(role);
		userServices.update(user);
		 
		return "redirect:userlist";
	}

	@GetMapping("/forgetpassword")
	public String forgetPassword(Model model) {
		
		model.addAttribute("message","");
		return "forgetpassword";
	}
	
	@PostMapping("/forgetpassword")
	public String forgetPassword2(Model model,@RequestParam String emailId,@RequestParam String mobile) {
		logger.info("{  email }  :"+emailId+"   { mobile } :"+mobile);
	UserEntity user=userServices.findOne(new UserEntity(), "where emailId ='"+emailId+"' and mobile = '"+mobile+"'");
	if(user!=null) {
		model.addAttribute("user",user);
		return "change_password";
	}else {
		model.addAttribute("message","Details not match");
		return "forgetpassword";
	}
	}

	@PostMapping("/changerpassword")
	public String changePassword2(Model model,@RequestParam long id,@RequestParam String password) throws Exception {
		logger.info("{  id }  :"+id+"   { password } :"+password);
	UserEntity user=userServices.findOne(new UserEntity(), id);
	user.setPassword(AES.encrypt(password));
	UserEntity u=userServices.update(user);
	model.addAttribute("user",u);
	return "signin";

	}

	
	@RequestMapping("/logout")
	public String userLogout(HttpSession session) {
		System.out.println("-------- USER LOGOUT----------"+session.getAttribute("user"));
		session.removeAttribute("user");
		
		return "redirect:/signin";
	}
}
