

package com.shopping.cart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shopping.cart.entity.ClientInfoEntity;
import com.shopping.cart.entity.UserEntity;
import com.shopping.cart.entity.UserRoleEntity;
import com.shopping.cart.services.IGenericService;

import ch.qos.logback.classic.Logger;

@Controller
public class ClientInfoController {

	@Autowired
	private IGenericService<ClientInfoEntity> clientInfoService;
	
	@Autowired
	IGenericService<UserEntity> userServices;
	@Autowired
	IGenericService<UserRoleEntity> userRoleServices;

	
	
	@RequestMapping(value="/addclient", method=RequestMethod.POST)
	public String saveClintInfo(Model model,@ModelAttribute ("clientInfo") ClientInfoEntity clientInfo) {
		
		System.out.println("-----------------' "+clientInfo);
		
		clientInfoService.save(clientInfo);
		long id=clientInfo.getId();
		clientInfo.setCustomerId("yss"+id);
		clientInfoService.update(clientInfo);
		System.out.println("-----------------' "+clientInfo);
		return "redirect:clientDetails";
	}
	
	@RequestMapping("/client")
	public String goToClintPage() {
		
		return "client_info";
	}
	
	@RequestMapping("/clientDetails")
	public String getClintDetails(Model model){
		List<ClientInfoEntity> clientList=clientInfoService.findAll(new ClientInfoEntity());
		model.addAttribute("clientList", clientList);
		System.out.println("client details "+clientList);
		return "client_info";
	}
	
	@RequestMapping(value="/clientUpdate",method=RequestMethod.POST)
	public String editClientDetails(Model model,@ModelAttribute("clientInfo")ClientInfoEntity clientInfo){
		System.out.println("------------------------------ client details by id "+clientInfo);
		ClientInfoEntity client=clientInfoService.update(clientInfo);
		System.out.println("------------------------------ client details by id "+client);
		return "redirect:/clientDetails";
	}
	
	
	@RequestMapping("deleteupdate/{id}")
	public String deleteClientInfo(Model model,@PathVariable long id) {
		System.out.println("---------------------"+id);
		ClientInfoEntity client=clientInfoService.findOne(new ClientInfoEntity(), id);
		System.out.println("---------------------"+client);
		clientInfoService.delete(client);
		
	return "redirect:/clientDetails";	
	}
	
	
	// -------------------------------- customer 
	@RequestMapping("/customer")
	public String customerPage() {
		
		return "customer";
	}
	
	/*@RequestMapping(value="/registration", method=RequestMethod.POST)
	public String customerRegistrationInfo(Model model,@ModelAttribute ("clientInfo") ClientInfoEntity clientInfo) {
		
		System.out.println("-----------------' "+clientInfo);
		
		clientInfoService.save(clientInfo);
		long id=clientInfo.getId();
		clientInfo.setCustomerId("yss"+id);
		ClientInfoEntity client =clientInfoService.update(clientInfo);
		System.out.println("-----------------' "+client);
		UserEntity user=new UserEntity();
		user.setName(client.getCompanyName());
		user.setMobile(client.getContactNo());
		user.setPassword(client.getContactNo());
		UserRoleEntity role=userRoleServices.findOne(new UserRoleEntity(), 2);
		System.out.println("role "+role);
		user.setRole(role);
		user.setUserName(client.getCustomerId());
		userServices.save(user);
		System.out.println("-----------------' "+clientInfo);
		return "redirect:/customer";
	}
	*/
}
