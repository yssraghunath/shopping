package com.shopping.cart.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.shopping.cart.entity.DefuserItemCategoryEntity;
import com.shopping.cart.entity.DefuserItemSubcategoryEntity;
import com.shopping.cart.services.IGenericService;

@Controller
public class DefuserItemCategoryController {

	private static final Logger logger=LoggerFactory.getLogger(DefuserItemCategoryController.class);
	@Autowired
	IGenericService<DefuserItemCategoryEntity> categoryService;
	
	@Autowired
	IGenericService<DefuserItemSubcategoryEntity> subCategoryService;
	
	
	@RequestMapping("/category")
	public String getCategory(Model model) {
	List<DefuserItemCategoryEntity> catList=categoryService.findAll(new DefuserItemCategoryEntity());	
	
		model.addAttribute("catList",catList);
		logger.info("{ all list of category  :  }  "+catList);
	return "category";	
	}
	
	@RequestMapping(value="/addcategory",method=RequestMethod.POST)
	public String saveCategory(Model model,@ModelAttribute("categoryInfo") DefuserItemCategoryEntity categoryInfo) {
		logger.info("{ all list of category  :  }  "+categoryInfo);
		categoryService.save(categoryInfo);
		logger.info("{ all list of category  :  }  "+categoryInfo);
		
		return "redirect:category";	
		}
	
	@RequestMapping(value="/categoryupdate",method=RequestMethod.POST)
	public String updateCategory(Model model,@ModelAttribute("category") DefuserItemCategoryEntity category) {
		
		categoryService.update(category);
		
		return "redirect:category";	
		}
	
	@RequestMapping(value="/categorydelete",method=RequestMethod.GET)
	public String deleteCategory(Model model,@RequestParam long id) {
		DefuserItemCategoryEntity category=categoryService.findOne(new DefuserItemCategoryEntity(), id);
		categoryService.delete(category);
		
		return "redirect:category";	
		}
	
	
	
	//----------------------------------------------------------------------------------
	@RequestMapping("/subcategory")
	public String getSubCategory(Model model) {
	List<DefuserItemSubcategoryEntity> subCatList=subCategoryService.findAll(new DefuserItemSubcategoryEntity());	
	List<DefuserItemCategoryEntity> catList=categoryService.findAll(new DefuserItemCategoryEntity());	
	model.addAttribute("catList",catList);
//	logger.info("{ all list of category  :  }  "+catList);
	
		model.addAttribute("subCatList",subCatList);
	//	logger.info("{ all list of category  :  }  "+subCatList);
	return "subcategory";	
	}
	
	@RequestMapping(value="/addsubcategory",method=RequestMethod.POST)
	public String saveSubCategory(Model model,@ModelAttribute("subCategoryInfo") DefuserItemSubcategoryEntity subCategoryInfo,@RequestParam("cid") String cid) {
		logger.info("{ all list of category  :  }  "+subCategoryInfo);
		logger.info("{ all list of category  :  }  "+cid);
		long id=Long.parseLong(cid);
		DefuserItemCategoryEntity category=categoryService.findOne(new DefuserItemCategoryEntity(), id);
				subCategoryInfo.setCategory(category);
	  subCategoryService.save(subCategoryInfo);
		logger.info("{ all list of category  :  }  "+subCategoryInfo);
		
		return "redirect:subcategory";	
		}
	
	
	@RequestMapping(value="/subcategoryupdate")
	public String updateSubCategory(Model model,@RequestParam long id, @RequestParam(name="subCate", required=false ) String subCat,@RequestParam(name="catId", required=false) long catId ) {
		
		logger.info("{   id  :  }  "+id +"{   category Id   :  }  "+catId+"{   subcategory   :  }  "+subCat);


	    DefuserItemSubcategoryEntity subCategory=subCategoryService.findOne(new DefuserItemSubcategoryEntity(),id);
	    if(subCat != null && !(subCat.equals(""))) {
	    	
	    subCategory.setSubCategory(subCat);
	    }
	    if(catId!=0) {
	    	DefuserItemCategoryEntity category=categoryService.findOne(new DefuserItemCategoryEntity(),catId);
	    	subCategory.setCategory(category);
	    }
	    
		subCategoryService.update(subCategory);
		
		return "redirect:subcategory";	
		}
	
	@RequestMapping(value="/subcategorydelete",method=RequestMethod.GET)
	public String deleteSubCategory(Model model,@RequestParam long id) {
		DefuserItemSubcategoryEntity category=subCategoryService.findOne(new DefuserItemSubcategoryEntity(), id);
		logger.info("{ sucategory delete } : "+category);
		subCategoryService.delete(category);
		
		return "redirect:subcategory";	
		}
}
