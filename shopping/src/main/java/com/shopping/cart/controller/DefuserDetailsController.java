package com.shopping.cart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shopping.cart.entity.DefuserItemEntity;
import com.shopping.cart.services.IGenericService;

@RestController
public class DefuserDetailsController {
	
	@Autowired
	IGenericService<DefuserItemEntity> defuserItemService;
	
	@RequestMapping(path="/employees", method=RequestMethod.GET)
	public List<DefuserItemEntity> getAllEmployees(){
		return defuserItemService.findAll(new DefuserItemEntity());
	}
	
    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
	public DefuserItemEntity getEmployeeById(@PathVariable("id") long id){
		return defuserItemService.findOne(new DefuserItemEntity(), id);
	}

}
