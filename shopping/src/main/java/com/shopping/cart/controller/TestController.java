package com.shopping.cart.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {
	
	@Autowired
	ServletContext context;
	@Value("${project.name}")
	private String projectName;
	
	Logger logger=LoggerFactory.getLogger(TestController.class);
	//@RequestMapping("/signin")
	@RequestMapping(value="/signin")
	public String getLoginPage() {
		
		logger.info("welcome "+projectName);
		context.setAttribute("TITLE", projectName);
		return "index";
	}
	
	
	
	@RequestMapping("/txt")
	public String gString() {
		
		return "defuser_item_image_upload";
	}

	@RequestMapping("/pagenation2")
	public String gPagination2() {
		
		return "testpaggination";
	}

	
	@RequestMapping(value="/hello")
	public String hello(@CookieValue(value="hitCounter",defaultValue="0")Long hitCounter,
			HttpServletResponse response
			) {
		
		hitCounter++;
		System.out.println("counter "+hitCounter);
		Cookie cookie=new Cookie("hitCounter",hitCounter.toString());
	//	response.addCookie(cookie);
		return "testPagination2";
	}
	
	/*@RequestMapping(value="/hello2")
	public String hellos(@ModelAttribute("item")DefuserItemEntity item,
			HttpSession session,Model model) {
		if(session.getAttribute("cart") ==null) {
			List<Cart> carts=new ArrayList<>();
			    carts.add(new Cart(item.getItemCode(),(int)item.getQuantity(),item.getItemMRP()));
			   session.setAttribute("cart", carts);
			   
			  
		}else {
			List<Cart> carts=(List<Cart>)session.getAttribute("cart");
		    carts.add(new Cart(item.getItemCode(),(int)item.getQuantity(),item.getItemMRP()));
		   session.setAttribute("cart", carts);
		   
		}
	
		 model.addAttribute("carts",(List<Cart>)session.getAttribute("cart"));
		return "testPagination2";
	}*/
}

