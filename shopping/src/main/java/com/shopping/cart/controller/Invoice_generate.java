package com.shopping.cart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.shopping.cart.entity.ClientInfoEntity;
import com.shopping.cart.entity.InvoiceDetailsEntity;
import com.shopping.cart.entity.InvoiceItemDetailEntity;
import com.shopping.cart.entity.PurchaseInfoEntity;
import com.shopping.cart.entity.SelfCompanyDetailsEntity;
import com.shopping.cart.services.IGenericService;

@Controller
public class Invoice_generate {
	
	@Autowired
	IGenericService<InvoiceDetailsEntity> invoiceService;
	@Autowired
	IGenericService<InvoiceItemDetailEntity> invoiceItemService;
	@Autowired
	IGenericService<SelfCompanyDetailsEntity> selfService;
	@Autowired
	IGenericService<ClientInfoEntity> clientService;
	
	@Autowired
	IGenericService<PurchaseInfoEntity> purchaseService;
	
	
	@RequestMapping("/invoice")
	public String InvoiceGenerate(Model model)
	{
		List<ClientInfoEntity> clientInfo=clientService.findAll(new ClientInfoEntity());
		model.addAttribute("clientInfo",clientInfo);
		
		return "invoice_form";
	}

	@RequestMapping("/invoicebill/{id}")
	public String chooseItem(Model model,@PathVariable long id)
	{
		model.addAttribute("ivoiceId",id);
		System.out.println("item id..............."+id);
		return "";
	}
	
	@RequestMapping(value="/savebuyer",method=RequestMethod.POST)
	public String saveBuyerDetails(Model model,@ModelAttribute("invoiceDetails") InvoiceDetailsEntity invoiceDetails) {
		System.out.println("buyer details ..........................................."+invoiceDetails);
		invoiceService.save(invoiceDetails);
		model.addAttribute("invoiceId", invoiceDetails.getId());
		List<PurchaseInfoEntity> itemDetails=purchaseService.findAll(new PurchaseInfoEntity());
		model.addAttribute("itemDetails", itemDetails);
		System.out.println("item  details ......"+itemDetails);
		System.out.println("buyer details ..........................................."+invoiceDetails);
		return "addInvoiceItem";
	}
	
	//InvoiceItemDetailEntity
	
	@RequestMapping(value="/itemSave",method=RequestMethod.POST)
	public String saveBuyerItem(Model model,@ModelAttribute("item") InvoiceItemDetailEntity item) {
		
		System.out.println("iddddddddddddddddddddddddddd "+item);
		InvoiceDetailsEntity invoice=invoiceService.findOne(new InvoiceDetailsEntity(), item.getInvoiceId());
		System.out.println("getId..........................................."+invoice.getId());
		long id=invoice.getId();
		item.setInvoiceDetails(invoice);
		
		invoiceItemService.save(item);
		model.addAttribute("invoiceId", invoice.getId());
		System.out.println("items details ..........................................."+item);
		List<PurchaseInfoEntity> itemDetails=purchaseService.findAll(new PurchaseInfoEntity());
		model.addAttribute("itemDetails", itemDetails);
		System.out.println("item  details ......"+itemDetails);
		
		List<InvoiceItemDetailEntity> previewData =invoiceItemService.findAll(new InvoiceItemDetailEntity(), "where invoiceId ='"+id+"'");
		model.addAttribute("previewData",previewData);
		SelfCompanyDetailsEntity selfDetails=selfService.findOne(new SelfCompanyDetailsEntity(), 1);
		System.out.println("self Details "+selfDetails);
		model.addAttribute("selfDetails",selfDetails);
	
		
		
		return "item_bill";
	}
	
	@RequestMapping("/preview")
	public String previewInvoice(Model model,@RequestParam (value="id") long id) {
		
		System.out.println("id -------------"+id);
		
		List<InvoiceItemDetailEntity> previewData =invoiceItemService.findAll(new InvoiceItemDetailEntity(), "where invoiceId ='"+id+"'");
		model.addAttribute("previewData",previewData);
		SelfCompanyDetailsEntity selfDetails=selfService.findOne(new SelfCompanyDetailsEntity(), 1);
		System.out.println("self Details "+selfDetails);
		model.addAttribute("selfDetails",selfDetails);
	//	return "preview_invoice";
		return "invoiceDetails";
		
	}
	
	@RequestMapping("/invoicedetails")
	public String getListInvoice(Model model) {
		
		List<InvoiceDetailsEntity> invoiceDetailsList=invoiceService.findAll(new InvoiceDetailsEntity());
		model.addAttribute("invoiceDetailsList", invoiceDetailsList);
		return "invoice_details_list";
	}
	
	
	
}




