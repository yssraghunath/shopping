package com.shopping.cart.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shopping.cart.entity.CompanyDetails;
import com.shopping.cart.entity.SelfBankDetailsEntity;
import com.shopping.cart.entity.SelfCompanyDetailsEntity;
import com.shopping.cart.services.IGenericService;

@Controller
public class CompanyInfoController {
	
	
	private static final Logger logger = LoggerFactory.getLogger(ItemController.class);
	
	@Autowired
	IGenericService<SelfBankDetailsEntity> bankService;
	@Autowired
	IGenericService<SelfCompanyDetailsEntity> companyService;
	
	@RequestMapping("/companyinfo")
	public String getCompanyDetails(Model model) {
		
		List<SelfCompanyDetailsEntity> companyDetilsList=companyService.findAll(new SelfCompanyDetailsEntity());
		logger.info("company list"+companyDetilsList);
		model.addAttribute("companyDetilsList",companyDetilsList);
		
		return "company_info_form";
	}

	@RequestMapping(value="companyinfo",method=RequestMethod.POST)
	public String saveCompanyDetails(Model mode,@ModelAttribute("companyDetails") CompanyDetails companyDetails) {
		
		logger.info("--------------------------------------\n "+companyDetails+"\n ------------------------------ ");
		
		if(companyDetails !=null) {
		
			
			//SelfBankDetailsEntity bankDetails=bankService.findOne(new SelfBankDetailsEntity(), 1);
			SelfBankDetailsEntity bankDetails=new SelfBankDetailsEntity();
			bankDetails.setAccountNo(companyDetails.getAccountNo());
			bankDetails.setBankName(companyDetails.getBankName());
			bankDetails.setBankAddress(companyDetails.getBankAddress());
			bankDetails.setIfscCode(companyDetails.getIfscCode());
			bankService.save(bankDetails);
		
			//SelfCompanyDetailsEntity myCompanyDetails=companyService.findOne(new SelfCompanyDetailsEntity(),1);
			SelfCompanyDetailsEntity myCompanyDetails=new SelfCompanyDetailsEntity();
			
			myCompanyDetails.setCompanyName(companyDetails.getCompanyName());
			myCompanyDetails.setCinNO(companyDetails.getCinNO());
			myCompanyDetails.setGstNo(companyDetails.getGstNo());
			myCompanyDetails.setPanNo(companyDetails.getPanNo());
			myCompanyDetails.setInvoiceNo(companyDetails.getInvoiceNo());
			
			myCompanyDetails.setRegisterAddress(companyDetails.getRegisterAddress());
			myCompanyDetails.setCorporateAddress(companyDetails.getCorporateAddress());
			myCompanyDetails.setBank(bankDetails);
			companyService.save(myCompanyDetails);
					
			

		}
				
		return "redirect:/companyinfo";
	}
	
	@RequestMapping(value="companyinfoupdate",method=RequestMethod.POST)
	public String updateCompanyDetails(Model mode,@ModelAttribute("companyDetails") CompanyDetails companyDetails) {
		
		logger.info("--------------------------------------\n update "+companyDetails+"\n ------------------------------ ");
		
		if(companyDetails !=null) {
		
			
			SelfBankDetailsEntity bankDetails=bankService.findOne(new SelfBankDetailsEntity(), 1);
			//SelfBankDetailsEntity bankDetails=new SelfBankDetailsEntity();
			bankDetails.setAccountNo(companyDetails.getAccountNo());
			bankDetails.setBankName(companyDetails.getBankName());
			bankDetails.setBankAddress(companyDetails.getBankAddress());
			bankDetails.setIfscCode(companyDetails.getIfscCode());
			bankService.update(bankDetails);
		
		   SelfCompanyDetailsEntity myCompanyDetails=companyService.findOne(new SelfCompanyDetailsEntity(),1);
			//SelfCompanyDetailsEntity myCompanyDetails=new SelfCompanyDetailsEntity();
			
			myCompanyDetails.setCompanyName(companyDetails.getCompanyName());
			myCompanyDetails.setCinNO(companyDetails.getCinNO());
			myCompanyDetails.setGstNo(companyDetails.getGstNo());
			myCompanyDetails.setPanNo(companyDetails.getPanNo());
			myCompanyDetails.setInvoiceNo(companyDetails.getInvoiceNo());
			
			myCompanyDetails.setRegisterAddress(companyDetails.getRegisterAddress());
			myCompanyDetails.setCorporateAddress(companyDetails.getCorporateAddress());
			myCompanyDetails.setBank(bankDetails);
			companyService.update(myCompanyDetails);
					
			

		}
				
		return "redirect:/companyinfo";
	}
}
