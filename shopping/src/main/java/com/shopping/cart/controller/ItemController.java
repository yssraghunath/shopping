package com.shopping.cart.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.shopping.cart.entity.CardEntity;
import com.shopping.cart.entity.ClientInfoEntity;
import com.shopping.cart.entity.ItemAndService;
import com.shopping.cart.entity.ItemEntity;
import com.shopping.cart.services.IGenericService;

@Controller
public class ItemController {

	private static final Logger logger = LoggerFactory.getLogger(ItemController.class);
	
	@Autowired
	private IGenericService<CardEntity> icardService;
	
	@Autowired
	private IGenericService<ItemEntity> itemService;
	
	@RequestMapping(value= {"/cardDetails"})
	public ModelAndView CardViewDetails(@ModelAttribute CardEntity card) {
		
		ModelAndView model = new ModelAndView();
		List<CardEntity> cardList = icardService.findAll(card);
		logger.info("\n\tCard Details...\n" + cardList.size() + cardList);
		model.addObject("listcard", cardList);
		model.setViewName("CardView");
		return model;
	}
	
	
	@RequestMapping(value= {"/item"})
	public ModelAndView ItemView(@ModelAttribute CardEntity card) {
		
		ModelAndView model = new ModelAndView();
		
		model.setViewName("ItemDashboard");
		return model;
	}
	
	@RequestMapping(value = {"/addcarddetails"} , method=RequestMethod.POST)
	public ModelAndView AddCard(@ModelAttribute CardEntity card) {
		
		logger.info("CardDeails" + card);
		ModelAndView model = new ModelAndView();
		icardService.save(card);
		logger.info("\n\tCard Details Added Succesfully...\n");
		model.addObject(new CardEntity());
		model.setViewName("redirect:/cardDetails");
		return model;
		
	}
	
	
	@RequestMapping("/saveitem")
	public String saveItemAndService(@ModelAttribute("item") ItemEntity item) {
		
		System.out.println("helllo");
		itemService.save(item);
		return "redirect:/itemDetails";
	}
	
	@RequestMapping("/itemDetails")
	public String itemDetais(Model model) {
		
		List<ItemEntity> itemList=itemService.findAll(new ItemEntity());
		model.addAttribute("itemList", itemList);
		
		System.out.println("helllo");
		return "item_form";
	}

}
