package com.shopping.cart.utility;

import java.util.List;

public interface IUtility<T> {
	void save(T entity);
	T update(T entity);
	void delete(T entity);
	List<T> findAll(T entity);
	List<T> findAll(T entity,String condition);
	T findOne(T entity,long id);
	T findOne(T entity,String condition);
	
	}

