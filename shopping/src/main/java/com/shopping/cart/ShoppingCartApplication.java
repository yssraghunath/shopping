package com.shopping.cart;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import com.shopping.cart.utility.AES;



@SpringBootApplication
public class ShoppingCartApplication extends SpringBootServletInitializer {

private static final Logger logger = LoggerFactory.getLogger(ShoppingCartApplication.class);
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(ShoppingCartApplication.class, args);
		logger.info(AES.encrypt("777277"));
	
		
		try {
			String pcIp = InetAddress.getLocalHost().getHostAddress();
			String pcName = InetAddress.getLocalHost().getHostName();
			
			System.out.println("\n------------------------------------------------\n\tSystem IP => "+pcIp+"\n\tSystem Name =>"+pcName+"\n------------------------------------------------");
		} catch (UnknownHostException e) {
			
			logger.info("\n\n\t Error = >" + e.getMessage() + "\n");			
		}
		
	}
}

