package com.shopping.cart.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelFileReaderAndWriter {

	 static DataFormatter dataFormatter = new DataFormatter();
	public static void main(String args[]) throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		System.out.println("Welcome");
		
		File excelFile=new File("C:\\Users\\Administrator\\Desktop\\test.xlsx");
		if(excelFile.exists())
		{
			System.out.println("File is there");
			readFile(excelFile);
			
			
		}else {
			System.out.println("File Not Found ");
		}
	}
	
	public static void readFile(File excelFile) throws EncryptedDocumentException, InvalidFormatException, IOException {
		List<Table> tableList=new ArrayList<Table>();
		Workbook workbook=WorkbookFactory.create(excelFile);
		System.out.println("work book get number of sheet  "+workbook.getNumberOfSheets());
		for(Sheet sheet:workbook) {
			System.out.println("sheet "+sheet.getSheetName());
				for(Row row:sheet) {
				//	System.out.println("row "+row.toString());
					
					System.out.println("number of cell "+row.getPhysicalNumberOfCells());
					
					//System.out.println("number of cell "+row.getCell(0));
					//System.out.println("number of cell "+row.getCell(1));
					//System.out.println("number of cell "+row.getCell(2));
					
				System.out.println("row number "+row.getRowNum());
				if(row.getRowNum()!=0) {
					
					Cell cell=row.getCell(0);
					 int id=Integer.parseInt(dataFormatter.formatCellValue(cell));
					
					 cell=row.getCell(1);
					 String name=dataFormatter.formatCellValue(cell);
					
					  cell=row.getCell(2);
					 String price=dataFormatter.formatCellValue(cell);
					 tableList.add(new Table(id,name,price));
					 System.out.println(id+"  "+name+"  "+price); 
					
				}
					 
					
				//	tableList.add(new Table(id,name,price));
					
					/*for(Cell cell:row) {
						String cellValue=dataFormatter.formatCellValue(cell);
					//	System.out.print(cellValue+"\t");
					}*/
					System.out.println("");
				}
			
		}
		
		
		for(Table t:tableList) {
			System.out.println(t);
		}
	}
	
	
}

class Table{
	
	int id;
	String name;
	String price;

	Table(){
		System.out.println("Default construction");
	}
	Table(int id,String name,String price){
		this.id=id;
		this.name=name;
		this.price=price;
	}
	
	@Override
	public String toString() {
		return "Table [id=" + id + ", name=" + name + ", price=" + price + "]";
	}
	
	

}
