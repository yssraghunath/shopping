package com.shopping.cart.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopping.cart.dao.IGenericDao;

@Service
public class ImpGenericService<T> implements IGenericService<T> {

	@Autowired
	IGenericDao<T> dao;
	
	@Override
	public void save(T entity) {
		// TODO Auto-generated method stub
	dao.save(entity);	
	}

	@Override
	public T update(T entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public void delete(T entity) {
		// TODO Auto-generated method stub
		dao.delete(entity);
	}

	@Override
	public List<T> findAll(T entity) {
		// TODO Auto-generated method stub
		return dao.findAll(entity);
	}

	@Override
	public List<T> findAll(T entity, String condition) {
		// TODO Auto-generated method stub
		return dao.findAll(entity, condition);
	}

	@Override
	public T findOne(T entity, long id) {
		// TODO Auto-generated method stub
		return dao.findOne(entity, id);
	}

	@Override
	public T findOne(T entity, String condtion) {
		// TODO Auto-generated method stub
		return dao.findOne(entity, condtion);
	}

}
