package com.shopping.cart.restcontroller;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.slf4j.Logger;
import com.shopping.cart.entity.DefuserItemEntity;
import com.shopping.cart.entity.DefuserItemSubcategoryEntity;
import com.shopping.cart.services.IGenericService;



@RestController
@CrossOrigin
public class SubCategoryController {
	private static final Logger logger = LoggerFactory.getLogger(SubCategoryController.class);
	@Autowired
	private IGenericService<DefuserItemSubcategoryEntity> subCategoryService;
	@Autowired
	private IGenericService<DefuserItemEntity> defuserItemService;
	

	@RequestMapping("/subcat")
	public ResponseEntity<List<DefuserItemSubcategoryEntity>> getSubCategoryList(@RequestParam String id){
		
		logger.info("{ }"+id);
		List<DefuserItemSubcategoryEntity> list=subCategoryService.findAll(new DefuserItemSubcategoryEntity(),"where category ='"+id+"'");
		return new ResponseEntity<List<DefuserItemSubcategoryEntity>>(list,HttpStatus.OK);
	}
	
	@RequestMapping("/productlist")
	public ResponseEntity<List<DefuserItemEntity>> getLProducatList(@RequestParam String cat,@RequestParam String subcat){
		logger.info(" {}  "+cat+"    {}  "+subcat);
		String condition="where itemCategory = '"+cat+"' AND itemSubCategory ='"+subcat+"'"; 
			List<DefuserItemEntity> list=defuserItemService.findAll(new DefuserItemEntity(),condition);
			logger.info("{ list of product } "+list);
		return new ResponseEntity<List<DefuserItemEntity>>(list,HttpStatus.OK);
		}
}
