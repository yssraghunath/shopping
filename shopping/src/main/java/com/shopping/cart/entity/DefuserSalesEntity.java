package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="defuser_sales")
public class DefuserSalesEntity {

	
	@Id
	@GeneratedValue
	private long id;
	private long itemId;
	private String itemName;
	private String quantity;
	private double itemPrice;
	private double totalPrice;
	private String buyerName;
	private String dateAndTime;
	public DefuserSalesEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DefuserSalesEntity(long id, long itemId, String itemName, String quantity, double itemPrice,
			double totalPrice, String buyerName, String dateAndTime) {
		super();
		this.id = id;
		this.itemId = itemId;
		this.itemName = itemName;
		this.quantity = quantity;
		this.itemPrice = itemPrice;
		this.totalPrice = totalPrice;
		this.buyerName = buyerName;
		this.dateAndTime = dateAndTime;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getItemId() {
		return itemId;
	}
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getDateAndTime() {
		return dateAndTime;
	}
	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}
	@Override
	public String toString() {
		return "DefuserSalesEntity [id=" + id + ", itemId=" + itemId + ", itemName=" + itemName + ", quantity="
				+ quantity + ", itemPrice=" + itemPrice + ", totalPrice=" + totalPrice + ", buyerName=" + buyerName
				+ ", dateAndTime=" + dateAndTime + "]";
	}

	

	
	
}
