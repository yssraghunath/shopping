package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dufuser_purchase")
public class DefuserPurchaseEntity {

	@Id
	@GeneratedValue
	private long id;
	private long itemId;
	private String itemName;
	private String quantity;
	private double itemPrice;
	private double totalPrice;
	private String vendorName;
	private String dateAndTime;
	
	public DefuserPurchaseEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DefuserPurchaseEntity(long id, long itemId, String itemName, String quantity, double itemPrice,
			double totalPrice, String vendorName, String dateAndTime) {
		super();
		this.id = id;
		this.itemId = itemId;
		this.itemName = itemName;
		this.quantity = quantity;
		this.itemPrice = itemPrice;
		this.totalPrice = totalPrice;
		this.vendorName = vendorName;
		this.dateAndTime = dateAndTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	@Override
	public String toString() {
		return "DefuserPurchaseEntity [id=" + id + ", itemId=" + itemId + ", itemName=" + itemName + ", quantity="
				+ quantity + ", itemPrice=" + itemPrice + ", totalPrice=" + totalPrice + ", vendorName=" + vendorName
				+ ", dateAndTime=" + dateAndTime + "]";
	}

	
		
}
