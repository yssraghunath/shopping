package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class ItemAndService {
	
	@Id
	@GeneratedValue
	private long id;
	private String itemName;
	private String hnsCode;
	private String taxAblePrice;
	private String cgst;
	private String sgst;
	private String igst;
	private String totalTaxAmount;
	private String totalprice;
	
	
	public ItemAndService() {
		super();
		// TODO Auto-generated constructor stub
	}


	public ItemAndService(long id, String itemName, String hnsCode, String taxAblePrice, String cgst, String sgst,
			String igst, String totalTaxAmount, String totalprice) {
		super();
		this.id = id;
		this.itemName = itemName;
		this.hnsCode = hnsCode;
		this.taxAblePrice = taxAblePrice;
		this.cgst = cgst;
		this.sgst = sgst;
		this.igst = igst;
		this.totalTaxAmount = totalTaxAmount;
		this.totalprice = totalprice;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getHnsCode() {
		return hnsCode;
	}


	public void setHnsCode(String hnsCode) {
		this.hnsCode = hnsCode;
	}


	public String getTaxAblePrice() {
		return taxAblePrice;
	}


	public void setTaxAblePrice(String taxAblePrice) {
		this.taxAblePrice = taxAblePrice;
	}


	public String getCgst() {
		return cgst;
	}


	public void setCgst(String cgst) {
		this.cgst = cgst;
	}


	public String getSgst() {
		return sgst;
	}


	public void setSgst(String sgst) {
		this.sgst = sgst;
	}


	public String getIgst() {
		return igst;
	}


	public void setIgst(String igst) {
		this.igst = igst;
	}


	public String getTotalTaxAmount() {
		return totalTaxAmount;
	}


	public void setTotalTaxAmount(String totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}


	public String getTotalprice() {
		return totalprice;
	}


	public void setTotalprice(String totalprice) {
		this.totalprice = totalprice;
	}


	@Override
	public String toString() {
		return "ItemAndService [id=" + id + ", itemName=" + itemName + ", hnsCode=" + hnsCode + ", taxAblePrice="
				+ taxAblePrice + ", cgst=" + cgst + ", sgst=" + sgst + ", igst=" + igst + ", totalTaxAmount="
				+ totalTaxAmount + ", totalprice=" + totalprice + "]";
	}
	
	
	

}
