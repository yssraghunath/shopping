package com.shopping.cart.entity;

public class CompanyDetails {

	private long id;
	private String companyName;
	private String gstNo;
	private String panNo;
	private String cinNO;
	private String invoiceNo;
	private String registerAddress;
	private String corporateAddress;
	private String BankName;
	private String AccountNo;
	private String ifscCode;
	private String bankAddress;
	
	
	
	
	public long getId() {
		return id;
	}




	public void setId(long id) {
		this.id = id;
	}




	public String getCompanyName() {
		return companyName;
	}




	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}




	public String getGstNo() {
		return gstNo;
	}




	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}




	public String getPanNo() {
		return panNo;
	}




	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}




	public String getCinNO() {
		return cinNO;
	}




	public void setCinNO(String cinNO) {
		this.cinNO = cinNO;
	}




	public String getInvoiceNo() {
		return invoiceNo;
	}




	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}




	public String getRegisterAddress() {
		return registerAddress;
	}




	public void setRegisterAddress(String registerAddress) {
		this.registerAddress = registerAddress;
	}




	public String getCorporateAddress() {
		return corporateAddress;
	}




	public void setCorporateAddress(String corporateAddress) {
		this.corporateAddress = corporateAddress;
	}




	public String getBankName() {
		return BankName;
	}




	public void setBankName(String bankName) {
		BankName = bankName;
	}




	public String getAccountNo() {
		return AccountNo;
	}




	public void setAccountNo(String accountNo) {
		AccountNo = accountNo;
	}




	public String getIfscCode() {
		return ifscCode;
	}




	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}




	public String getBankAddress() {
		return bankAddress;
	}




	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}




	@Override
	public String toString() {
		return "CompanyDetails [id=" + id + ", companyName=" + companyName + ", gstNo=" + gstNo + ", panNo=" + panNo
				+ ", cinNO=" + cinNO + ", invoiceNo=" + invoiceNo + ", registerAddress=" + registerAddress
				+ ", corporateAddress=" + corporateAddress + ", BankName=" + BankName + ", AccountNo=" + AccountNo
				+ ", ifscCode=" + ifscCode + ", bankAddress=" + bankAddress + "]";
	}
	
	
}
