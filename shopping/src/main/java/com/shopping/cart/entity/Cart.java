package com.shopping.cart.entity;

import javax.persistence.ManyToOne;

public class Cart {
	@ManyToOne
	private DefuserItemEntity itemCode;
	private double price;
	private int quantity;
	
	
	
	public Cart() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Cart(DefuserItemEntity itemCode, double price, int quantity) {
		super();
		this.itemCode = itemCode;
		this.price = price;
		this.quantity = quantity;
	}
	public DefuserItemEntity getItemCode() {
		return itemCode;
	}
	public void setItemCode(DefuserItemEntity itemCode) {
		this.itemCode = itemCode;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "Cart [itemCode=" + itemCode + ", price=" + price + ", quantity=" + quantity + "]";
	}

	
	
	

}
