package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class AddToCartEntity {
	
	@Id
	@GeneratedValue
	private long id;
	@ManyToOne
	private UserEntity userId;
	@ManyToOne
	private DefuserItemEntity itemCode;
	private double itemPrice;
	private int quantity;
	private double totalPrice;
	private String addToCardDate;
	
	public AddToCartEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AddToCartEntity(long id, UserEntity userId, DefuserItemEntity itemCode, double itemPrice, int quantity,
			double totalPrice, String addToCardDate) {
		super();
		this.id = id;
		this.userId = userId;
		this.itemCode = itemCode;
		this.itemPrice = itemPrice;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
		this.addToCardDate = addToCardDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserEntity getUserId() {
		return userId;
	}

	public void setUserId(UserEntity userId) {
		this.userId = userId;
	}

	public DefuserItemEntity getItemCode() {
		return itemCode;
	}

	public void setItemCode(DefuserItemEntity itemCode) {
		this.itemCode = itemCode;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getAddToCardDate() {
		return addToCardDate;
	}

	public void setAddToCardDate(String addToCardDate) {
		this.addToCardDate = addToCardDate;
	}

	@Override
	public String toString() {
		return "AddToCartEntity [id=" + id + ", userId=" + userId + ", itemCode=" + itemCode + ", itemPrice="
				+ itemPrice + ", quantity=" + quantity + ", totalPrice=" + totalPrice + ", addToCardDate="
				+ addToCardDate + "]";
	}

	
}
