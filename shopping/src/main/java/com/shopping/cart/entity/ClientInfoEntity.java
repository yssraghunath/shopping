package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="client_info")
public class ClientInfoEntity {

	@Id
	@GeneratedValue
	private long id;
	private String customerId;
	private String companyName;
	private String address;
	private String gstNo;
	private String contactNo;
	private String email;
	private String entryDate;
	
	 
	
	public ClientInfoEntity() {
		super();
		// TODO Auto-generated constructor stub
	}



	public ClientInfoEntity(long id, String customerId, String companyName, String address, String gstNo,
			String contactNo, String email, String entryDate) {
		super();
		this.id = id;
		this.customerId = customerId;
		this.companyName = companyName;
		this.address = address;
		this.gstNo = gstNo;
		this.contactNo = contactNo;
		this.email = email;
		this.entryDate = entryDate;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getCustomerId() {
		return customerId;
	}



	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}



	public String getCompanyName() {
		return companyName;
	}



	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getGstNo() {
		return gstNo;
	}



	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}



	public String getContactNo() {
		return contactNo;
	}



	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getEntryDate() {
		return entryDate;
	}



	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}



	@Override
	public String toString() {
		return "ClientInfoEntity [id=" + id + ", customerId=" + customerId + ", companyName=" + companyName
				+ ", address=" + address + ", gstNo=" + gstNo + ", contactNo=" + contactNo + ", email=" + email
				+ ", entryDate=" + entryDate + "]";
	}



	
	
}
