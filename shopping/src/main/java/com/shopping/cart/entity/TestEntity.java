package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="temp")
public class TestEntity {

	@Id
	private long id;
	private String name;
	public TestEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TestEntity(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "TestEntity [id=" + id + ", name=" + name + "]";
	}
	
	
}
