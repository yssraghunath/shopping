package com.shopping.cart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="defuser_item")
public class DefuserItemEntity {
	@Id
	@GeneratedValue
	private long id;
	@Column(unique=true)
	private String itemCode;
	@Column(unique=true)
	private String itemCodeV;
	private String itemName;
	private String itemDescription;
	private String itemCategory;
	private String itemSubCategory;
	private long quantity;
	private double itemMRP;
	private double itemOLP;
	private double itemTP;
	private double itemOSP;
	private String imageUrl;
	
	public DefuserItemEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DefuserItemEntity(long id, String itemCode, String itemCodeV, String itemName, String itemDescription,
			String itemCategory, String itemSubCategory, long quantity, double itemMRP, double itemOLP, double itemTP,
			double itemOSP, String imageUrl) {
		super();
		this.id = id;
		this.itemCode = itemCode;
		this.itemCodeV = itemCodeV;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.itemCategory = itemCategory;
		this.itemSubCategory = itemSubCategory;
		this.quantity = quantity;
		this.itemMRP = itemMRP;
		this.itemOLP = itemOLP;
		this.itemTP = itemTP;
		this.itemOSP = itemOSP;
		this.imageUrl = imageUrl;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemCodeV() {
		return itemCodeV;
	}

	public void setItemCodeV(String itemCodeV) {
		this.itemCodeV = itemCodeV;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(String itemCategory) {
		this.itemCategory = itemCategory;
	}

	public String getItemSubCategory() {
		return itemSubCategory;
	}

	public void setItemSubCategory(String itemSubCategory) {
		this.itemSubCategory = itemSubCategory;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public double getItemMRP() {
		return itemMRP;
	}

	public void setItemMRP(double itemMRP) {
		this.itemMRP = itemMRP;
	}

	public double getItemOLP() {
		return itemOLP;
	}

	public void setItemOLP(double itemOLP) {
		this.itemOLP = itemOLP;
	}

	public double getItemTP() {
		return itemTP;
	}

	public void setItemTP(double itemTP) {
		this.itemTP = itemTP;
	}

	public double getItemOSP() {
		return itemOSP;
	}

	public void setItemOSP(double itemOSP) {
		this.itemOSP = itemOSP;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "DefuserItemEntity [id=" + id + ", itemCode=" + itemCode + ", itemCodeV=" + itemCodeV + ", itemName="
				+ itemName + ", itemDescription=" + itemDescription + ", itemCategory=" + itemCategory
				+ ", itemSubCategory=" + itemSubCategory + ", quantity=" + quantity + ", itemMRP=" + itemMRP
				+ ", itemOLP=" + itemOLP + ", itemTP=" + itemTP + ", itemOSP=" + itemOSP + ", imageUrl=" + imageUrl
				+ "]";
	}

	
	
}
