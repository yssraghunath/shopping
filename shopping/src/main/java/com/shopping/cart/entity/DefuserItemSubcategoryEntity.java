package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="item_subcategory")
public class DefuserItemSubcategoryEntity {
	

	@Id
	@GeneratedValue
	private long id;
	private String subCategory;
	@ManyToOne
	private DefuserItemCategoryEntity category;
	
	public DefuserItemSubcategoryEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DefuserItemSubcategoryEntity(long id, String subCategory, DefuserItemCategoryEntity category) {
		super();
		this.id = id;
		this.subCategory = subCategory;
		this.category = category;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public DefuserItemCategoryEntity getCategory() {
		return category;
	}

	public void setCategory(DefuserItemCategoryEntity category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "DefuserItemSubcategoryEntity [id=" + id + ", subCategory=" + subCategory + ", category=" + category
				+ "]";
	}

	
	
	
}
