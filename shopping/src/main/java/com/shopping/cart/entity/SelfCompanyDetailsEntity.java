package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Company_info")
public class SelfCompanyDetailsEntity {

	@Id
	@GeneratedValue
	private long id;
	private String companyName;
	private String gstNo;
	private String panNo;
	private String cinNO;
	private String invoiceNo;
	private String registerAddress;
	private String corporateAddress;
	@OneToOne
	private SelfBankDetailsEntity bank;
	
	public SelfCompanyDetailsEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SelfCompanyDetailsEntity(long id, String companyName, String gstNo, String panNo, String cinNO,
			String invoiceNo, String registerAddress, String corporateAddress, SelfBankDetailsEntity bank) {
		super();
		this.id = id;
		this.companyName = companyName;
		this.gstNo = gstNo;
		this.panNo = panNo;
		this.cinNO = cinNO;
		this.invoiceNo = invoiceNo;
		this.registerAddress = registerAddress;
		this.corporateAddress = corporateAddress;
		this.bank = bank;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getCinNO() {
		return cinNO;
	}

	public void setCinNO(String cinNO) {
		this.cinNO = cinNO;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getRegisterAddress() {
		return registerAddress;
	}

	public void setRegisterAddress(String registerAddress) {
		this.registerAddress = registerAddress;
	}

	public String getCorporateAddress() {
		return corporateAddress;
	}

	public void setCorporateAddress(String corporateAddress) {
		this.corporateAddress = corporateAddress;
	}

	public SelfBankDetailsEntity getBank() {
		return bank;
	}

	public void setBank(SelfBankDetailsEntity bank) {
		this.bank = bank;
	}

	@Override
	public String toString() {
		return "SelfCompanyDetailsEntity [id=" + id + ", companyName=" + companyName + ", gstNo=" + gstNo + ", panNo="
				+ panNo + ", cinNO=" + cinNO + ", invoiceNo=" + invoiceNo + ", registerAddress=" + registerAddress
				+ ", corporateAddress=" + corporateAddress + ", bank=" + bank + "]";
	}
	
	
	
	
	
}
