package com.shopping.cart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class UserEntity {
	
	@Id
	@GeneratedValue
	private long id;
	
	@NotNull
	private String name;
	
	@NotNull
	@Column(unique=true)
	private String userName;

	@NotNull
	private String password;
	private String mobile;
	private String address;
	@Column(name="email")
	private String emailId;
	
	@NotNull
	@ManyToOne
	private UserRoleEntity role;
	@Column(name="Reg_Date")
	private String EnteryDate;
	
//	private boolean activate;
	
	public UserEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserEntity(long id, String name, String userName, String password, String mobile, String address,
			String emailId, UserRoleEntity role, String enteryDate) {
		super();
		this.id = id;
		this.name = name;
		this.userName = userName;
		this.password = password;
		this.mobile = mobile;
		this.address = address;
		this.emailId = emailId;
		this.role = role;
		EnteryDate = enteryDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public UserRoleEntity getRole() {
		return role;
	}

	public void setRole(UserRoleEntity role) {
		this.role = role;
	}

	public String getEnteryDate() {
		return EnteryDate;
	}

	public void setEnteryDate(String enteryDate) {
		EnteryDate = enteryDate;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", name=" + name + ", userName=" + userName + ", password=" + password
				+ ", mobile=" + mobile + ", address=" + address + ", emailId=" + emailId + ", role=" + role
				+ ", EnteryDate=" + EnteryDate + "]";
	}

	
	
}
