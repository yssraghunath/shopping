package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class ItemEntity {
	
	@Id
	@GeneratedValue
	private long id;
	private String itemName;
	private String hnsCode;
	private int cgst;
	private int sgst;
	private int igst;
	
	public ItemEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

		
	
	public ItemEntity(long id, String itemName, String hnsCode, int cgst, int sgst, int igst) {
		super();
		this.id = id;
		this.itemName = itemName;
		this.hnsCode = hnsCode;
		this.cgst = cgst;
		this.sgst = sgst;
		this.igst = igst;
	}




	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getHnsCode() {
		return hnsCode;
	}

	public void setHnsCode(String hnsCode) {
		this.hnsCode = hnsCode;
	}

	public int getCgst() {
		return cgst;
	}

	public void setCgst(int cgst) {
		this.cgst = cgst;
	}

	public int getSgst() {
		return sgst;
	}

	public void setSgst(int sgst) {
		this.sgst = sgst;
	}

	public int getIgst() {
		return igst;
	}

	public void setIgst(int igst) {
		this.igst = igst;
	}



	@Override
	public String toString() {
		return "ItemEntity [id=" + id + ", itemName=" + itemName + ", hnsCode=" + hnsCode + ", cgst=" + cgst + ", sgst="
				+ sgst + ", igst=" + igst + "]";
	}
	
	
	
	
	
	

}
