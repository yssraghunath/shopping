package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Bank_info")
public class SelfBankDetailsEntity {
	@Id
	@GeneratedValue
	private long id;
	private String BankName;
	private String AccountNo;
	private String ifscCode;
	private String bankAddress;
	
	public SelfBankDetailsEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SelfBankDetailsEntity(long id, String bankName, String accountNo, String ifscCode, String bankAddress) {
		super();
		this.id = id;
		BankName = bankName;
		AccountNo = accountNo;
		this.ifscCode = ifscCode;
		this.bankAddress = bankAddress;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBankName() {
		return BankName;
	}

	public void setBankName(String bankName) {
		BankName = bankName;
	}

	public String getAccountNo() {
		return AccountNo;
	}

	public void setAccountNo(String accountNo) {
		AccountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	@Override
	public String toString() {
		return "SelfBankDetailsEntity [id=" + id + ", BankName=" + BankName + ", AccountNo=" + AccountNo + ", ifscCode="
				+ ifscCode + ", bnkAddress=" + bankAddress + "]";
	}
	
	
	
	
}
