package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="item_category")
public class DefuserItemCategoryEntity {
	
	@Id
	@GeneratedValue
	private long id;
	private String category;
	
	public DefuserItemCategoryEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DefuserItemCategoryEntity(long id, String category) {
		super();
		this.id = id;
		this.category = category;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "DefuserItemCategoryEntity [id=" + id + ", category=" + category + "]";
	}

		
}
