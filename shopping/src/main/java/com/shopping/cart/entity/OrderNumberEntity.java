package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class OrderNumberEntity {

	@Id
	@GeneratedValue
	private long id;
	private String userName;
	private String orderDate;
	private String address;
	private String mobileNo;
	private String name;
	private String emailId;
	private String orderFormate;
	
	
	public OrderNumberEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public OrderNumberEntity(long id, String userName, String orderDate, String address, String mobileNo, String name,
			String emailId, String orderFormate) {
		super();
		this.id = id;
		this.userName = userName;
		this.orderDate = orderDate;
		this.address = address;
		this.mobileNo = mobileNo;
		this.name = name;
		this.emailId = emailId;
		this.orderFormate = orderFormate;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getOrderDate() {
		return orderDate;
	}


	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getOrderFormate() {
		return orderFormate;
	}


	public void setOrderFormate(String orderFormate) {
		this.orderFormate = orderFormate;
	}


	@Override
	public String toString() {
		return "OrderNumberEntity [id=" + id + ", userName=" + userName + ", orderDate=" + orderDate + ", address="
				+ address + ", mobileNo=" + mobileNo + ", name=" + name + ", emailId=" + emailId + ", orderFormate="
				+ orderFormate + "]";
	}


		
	
	
}
