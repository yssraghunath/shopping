package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name="ivoice_Details")
public class InvoiceDetailsEntity {

	@Id
	@GeneratedValue
	private long id;
	private String invoiceNo;
	private String buyerDetails;
	private String siteDetails;
	private String invoiceDate;
	private String clientPoNo;
	private String poDate;
	private String quoteRefNo;
	private String dueDate;
	private String amcDateFrom;
	private String amcDateTo;
	private String paymentMode;
	private String taxPayable;
	
	public InvoiceDetailsEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvoiceDetailsEntity(long id, String invoiceNo, String buyerDetails, String siteDetails, String invoiceDate,
			String clientPoNo, String poDate, String quoteRefNo, String dueDate, String amcDateFrom, String amcDateTo,
			String paymentMode, String taxPayable) {
		super();
		this.id = id;
		this.invoiceNo = invoiceNo;
		this.buyerDetails = buyerDetails;
		this.siteDetails = siteDetails;
		this.invoiceDate = invoiceDate;
		this.clientPoNo = clientPoNo;
		this.poDate = poDate;
		this.quoteRefNo = quoteRefNo;
		this.dueDate = dueDate;
		this.amcDateFrom = amcDateFrom;
		this.amcDateTo = amcDateTo;
		this.paymentMode = paymentMode;
		this.taxPayable = taxPayable;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getBuyerDetails() {
		return buyerDetails;
	}

	public void setBuyerDetails(String buyerDetails) {
		this.buyerDetails = buyerDetails;
	}

	public String getSiteDetails() {
		return siteDetails;
	}

	public void setSiteDetails(String siteDetails) {
		this.siteDetails = siteDetails;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getClientPoNo() {
		return clientPoNo;
	}

	public void setClientPoNo(String clientPoNo) {
		this.clientPoNo = clientPoNo;
	}

	public String getPoDate() {
		return poDate;
	}

	public void setPoDate(String poDate) {
		this.poDate = poDate;
	}

	public String getQuoteRefNo() {
		return quoteRefNo;
	}

	public void setQuoteRefNo(String quoteRefNo) {
		this.quoteRefNo = quoteRefNo;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getAmcDateFrom() {
		return amcDateFrom;
	}

	public void setAmcDateFrom(String amcDateFrom) {
		this.amcDateFrom = amcDateFrom;
	}

	public String getAmcDateTo() {
		return amcDateTo;
	}

	public void setAmcDateTo(String amcDateTo) {
		this.amcDateTo = amcDateTo;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getTaxPayable() {
		return taxPayable;
	}

	public void setTaxPayable(String taxPayable) {
		this.taxPayable = taxPayable;
	}

	@Override
	public String toString() {
		return "InvoiceDetailsEntity [id=" + id + ", invoiceNo=" + invoiceNo + ", buyerDetails=" + buyerDetails
				+ ", siteDetails=" + siteDetails + ", invoiceDate=" + invoiceDate + ", clientPoNo=" + clientPoNo
				+ ", poDate=" + poDate + ", quoteRefNo=" + quoteRefNo + ", dueDate=" + dueDate + ", amcDateFrom="
				+ amcDateFrom + ", amcDateTo=" + amcDateTo + ", paymentMode=" + paymentMode + ", taxPayable="
				+ taxPayable + "]";
	}

	

		
}
