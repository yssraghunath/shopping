package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="purchase_info")
public class PurchaseInfoEntity {

	@Id
	@GeneratedValue
	private long id;
	private String itemName;
	private String companyName;
	private String hnsCode;
	private int quantity;
	private double price;
	private int sgst;
	private int cgst;
	private double total;
	private String entryDate;
	
	public PurchaseInfoEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	


	public PurchaseInfoEntity(long id, String itemName, String companyName, String hnsCode, int quantity, double price,
			int sgst, int cgst, double total, String entryDate) {
		super();
		this.id = id;
		this.itemName = itemName;
		this.companyName = companyName;
		this.hnsCode = hnsCode;
		this.quantity = quantity;
		this.price = price;
		this.sgst = sgst;
		this.cgst = cgst;
		this.total = total;
		this.entryDate = entryDate;
	}











	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	
	
	
	public int getSgst() {
		return sgst;
	}



	public void setSgst(int sgst) {
		this.sgst = sgst;
	}



	public int getCgst() {
		return cgst;
	}



	public void setCgst(int cgst) {
		this.cgst = cgst;
	}


	
	

	public String getHnsCode() {
		return hnsCode;
	}




	public void setHnsCode(String hnsCode) {
		this.hnsCode = hnsCode;
	}




	@Override
	public String toString() {
		return "PurchaseInfoEntity [id=" + id + ", itemName=" + itemName + ", companyName=" + companyName + ", hnsCode="
				+ hnsCode + ", quantity=" + quantity + ", price=" + price + ", sgst=" + sgst + ", cgst=" + cgst
				+ ", total=" + total + ", entryDate=" + entryDate + "]";
	}




	

	
	
	
	
	
}
