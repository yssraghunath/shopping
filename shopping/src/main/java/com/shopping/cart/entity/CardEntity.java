package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_card")
public class CardEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String itemname;
	private String serialno;
	private String purchasedate;
	private String modeltype;
	private String warranty;
	private String issuedto;
	private String installedby;
	private String returned;
	private String price;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public String getSerialno() {
		return serialno;
	}
	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
	public String getPurchasedate() {
		return purchasedate;
	}
	public void setPurchasedate(String purchasedate) {
		this.purchasedate = purchasedate;
	}
	public String getModeltype() {
		return modeltype;
	}
	public void setModeltype(String modeltype) {
		this.modeltype = modeltype;
	}
	public String getWarranty() {
		return warranty;
	}
	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}
	public String getIssuedto() {
		return issuedto;
	}
	public void setIssuedto(String issuedto) {
		this.issuedto = issuedto;
	}
	public String getInstalledby() {
		return installedby;
	}
	public void setInstalledby(String installedby) {
		this.installedby = installedby;
	}
	public String getReturned() {
		return returned;
	}
	public void setReturned(String returned) {
		this.returned = returned;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	
	public CardEntity(Long id, String itemname, String serialno, String purchasedate, String modeltype, String warranty,
			String issuedto, String installedby, String returned, String price) {
		super();
		this.id = id;
		this.itemname = itemname;
		this.serialno = serialno;
		this.purchasedate = purchasedate;
		this.modeltype = modeltype;
		this.warranty = warranty;
		this.issuedto = issuedto;
		this.installedby = installedby;
		this.returned = returned;
		this.price = price;
	}
	public CardEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "ItemEntity [id=" + id + ", itemname=" + itemname + ", serialno=" + serialno + ", purchasedate="
				+ purchasedate + ", modeltype=" + modeltype + ", warranty=" + warranty + ", issuedto=" + issuedto
				+ ", installedby=" + installedby + ", returned=" + returned + ", price=" + price + "]";
	}
	

	
}
