package com.shopping.cart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class UserRoleEntity {

	@Id
	@GeneratedValue
	long id;
	
	@NotNull
	@Column(name="role")
	String roleType;

	
	public UserRoleEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public UserRoleEntity(long id, String roleType) {
		super();
		this.id = id;
		this.roleType = roleType;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getRoleType() {
		return roleType;
	}


	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}


	@Override
	public String toString() {
		return "UserRoleEntity [id=" + id + ", roleType=" + roleType + "]";
	}
	
	
	
	
	
}
