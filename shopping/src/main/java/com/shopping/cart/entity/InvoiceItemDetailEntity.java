package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="invoice_item_Details")
public class InvoiceItemDetailEntity {

	@Id
	@GeneratedValue
	private long id;
	private String itemName;
	private String hnsCode;
	private String taxAblePrice;
	private String price;
	private String cgst;
	private String sgst;
	private String igst;
	private String totalTaxAmount;
	private String totalprice;
	private long invoiceId;
	@ManyToOne
	private InvoiceDetailsEntity invoiceDetails;
	
	
	public InvoiceItemDetailEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	

	public InvoiceItemDetailEntity(long id, String itemName, String hnsCode, String taxAblePrice, String price,
			String cgst, String sgst, String igst, String totalTaxAmount, String totalprice, long invoiceId,
			InvoiceDetailsEntity invoiceDetails) {
		super();
		this.id = id;
		this.itemName = itemName;
		this.hnsCode = hnsCode;
		this.taxAblePrice = taxAblePrice;
		this.price = price;
		this.cgst = cgst;
		this.sgst = sgst;
		this.igst = igst;
		this.totalTaxAmount = totalTaxAmount;
		this.totalprice = totalprice;
		this.invoiceId = invoiceId;
		this.invoiceDetails = invoiceDetails;
	}




	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getHnsCode() {
		return hnsCode;
	}


	public void setHnsCode(String hnsCode) {
		this.hnsCode = hnsCode;
	}


	public String getTaxAblePrice() {
		return taxAblePrice;
	}


	public void setTaxAblePrice(String taxAblePrice) {
		this.taxAblePrice = taxAblePrice;
	}


	public String getCgst() {
		return cgst;
	}


	public void setCgst(String cgst) {
		this.cgst = cgst;
	}


	public String getSgst() {
		return sgst;
	}


	public void setSgst(String sgst) {
		this.sgst = sgst;
	}


	public String getIgst() {
		return igst;
	}


	public void setIgst(String igst) {
		this.igst = igst;
	}


	public String getTotalTaxAmount() {
		return totalTaxAmount;
	}


	public void setTotalTaxAmount(String totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}


	public String getTotalprice() {
		return totalprice;
	}


	public void setTotalprice(String totalprice) {
		this.totalprice = totalprice;
	}


	public InvoiceDetailsEntity getInvoiceDetails() {
		return invoiceDetails;
	}


	public void setInvoiceDetails(InvoiceDetailsEntity invoiceDetails) {
		this.invoiceDetails = invoiceDetails;
	}


	
	public long getInvoiceId() {
		return invoiceId;
	}


	public void setInvoiceId(long invoiceId) {
		this.invoiceId = invoiceId;
	}


	
	
	public String getPrice() {
		return price;
	}




	public void setPrice(String price) {
		this.price = price;
	}




	@Override
	public String toString() {
		return "InvoiceItemDetailEntity [id=" + id + ", itemName=" + itemName + ", hnsCode=" + hnsCode
				+ ", taxAblePrice=" + taxAblePrice + ", price=" + price + ", cgst=" + cgst + ", sgst=" + sgst
				+ ", igst=" + igst + ", totalTaxAmount=" + totalTaxAmount + ", totalprice=" + totalprice
				+ ", invoiceId=" + invoiceId + ", invoiceDetails=" + invoiceDetails + "]";
	}






	
	
	
	
}
