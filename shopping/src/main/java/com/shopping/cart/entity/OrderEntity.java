package com.shopping.cart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class OrderEntity {
	
	@Id
	@GeneratedValue
	private long id;
	@ManyToOne
	private UserEntity userId;
	@ManyToOne
	private DefuserItemEntity itemCode;
	@ManyToOne
	private OrderNumberEntity orderId;
	private double itemPrice;
	private int quantity;
	private double totalPrice;
	private String orderStatus;
	private String orderDate;
	private String deliveryStatus;
	private String deliveryDate;
	
	
	public OrderEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public OrderEntity(long id, UserEntity userId, DefuserItemEntity itemCode, OrderNumberEntity orderId,
			double itemPrice, int quantity, double totalPrice, String orderStatus, String orderDate,
			String deliveryStatus, String deliveryDate) {
		super();
		this.id = id;
		this.userId = userId;
		this.itemCode = itemCode;
		this.orderId = orderId;
		this.itemPrice = itemPrice;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
		this.orderStatus = orderStatus;
		this.orderDate = orderDate;
		this.deliveryStatus = deliveryStatus;
		this.deliveryDate = deliveryDate;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public UserEntity getUserId() {
		return userId;
	}


	public void setUserId(UserEntity userId) {
		this.userId = userId;
	}


	public DefuserItemEntity getItemCode() {
		return itemCode;
	}


	public void setItemCode(DefuserItemEntity itemCode) {
		this.itemCode = itemCode;
	}


	public OrderNumberEntity getOrderId() {
		return orderId;
	}


	public void setOrderId(OrderNumberEntity orderId) {
		this.orderId = orderId;
	}


	public double getItemPrice() {
		return itemPrice;
	}


	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public double getTotalPrice() {
		return totalPrice;
	}


	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}


	public String getOrderStatus() {
		return orderStatus;
	}


	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}


	public String getOrderDate() {
		return orderDate;
	}


	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}


	public String getDeliveryStatus() {
		return deliveryStatus;
	}


	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}


	public String getDeliveryDate() {
		return deliveryDate;
	}


	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	@Override
	public String toString() {
		return "OrderEntity [id=" + id + ", userId=" + userId + ", itemCode=" + itemCode + ", orderId=" + orderId
				+ ", itemPrice=" + itemPrice + ", quantity=" + quantity + ", totalPrice=" + totalPrice
				+ ", orderStatus=" + orderStatus + ", orderDate=" + orderDate + ", deliveryStatus=" + deliveryStatus
				+ ", deliveryDate=" + deliveryDate + "]";
	}


	
	
}
